﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class constructionMaterialHelper : MonoBehaviour
{
    
    public Material constructionMat;
    public float progress;
    public Vector2 limits;
    public Texture useTexture;
    private Renderer renderer;
    public float curProgress = 0;
   
    
    // Start is called before the first frame update
    void Start() {
        renderer = this.GetComponent<Renderer>();
        renderer.material = constructionMat;
        renderer.material.SetTexture("mainTexture", useTexture);
        renderer.material.SetVector("limits", new Vector4(limits.x, limits.y, 0 ,0));
        renderer.material.SetFloat("progress", progress);
    }

    // Update is called once per frame
    void Update() {
        curProgress = Mathf.Lerp(curProgress, progress, 0.02f);
        renderer.material.SetFloat("progress", curProgress);
    }
}
