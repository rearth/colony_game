﻿using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

public class cheatItems : MonoBehaviour {

	// Use this for initialization
	void Start () {
		this.GetComponent<inventory>().add(new ressourceStack(3000, ressources.Wood));
		this.GetComponent<inventory>().add(new ressourceStack(3000, ressources.Stone));
		this.GetComponent<inventory>().add(new ressourceStack(3000, ressources.OreIron));
		this.GetComponent<inventory>().add(new ressourceStack(3000, ressources.OreGold));
		this.GetComponent<inventory>().add(new ressourceStack(3000, ressources.Gold));
		this.GetComponent<inventory>().add(new ressourceStack(2000, ressources.Iron));
		this.GetComponent<inventory>().add(new ressourceStack(3000, ressources.Water));
		this.GetComponent<inventory>().add(new ressourceStack(2000, ressources.Uranium));
		this.GetComponent<inventory>().add(new ressourceStack(1000, ressources.Scrap));
		this.GetComponent<inventory>().add(new ressourceStack(500, ressources.Thorium));
		this.GetComponent<inventory>().add(new ressourceStack(500, ressources.Electrum));
		this.GetComponent<inventory>().add(new ressourceStack(500, ressources.DepletedUranium));
		this.GetComponent<inventory>().add(new ressourceStack(500, ressources.Warhead));
		this.GetComponent<inventory>().add(new ressourceStack(500, ressources.Coal));
		this.GetComponent<inventory>().add(new ressourceStack(500, ressources.Steel));
		this.GetComponent<inventory>().add(new ressourceStack(500, ressources.Electrum));
		this.GetComponent<inventory>().add(new ressourceStack(500, ressources.Iridium));
	}
}
