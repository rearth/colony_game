﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class SaveLoad : MonoBehaviour {
    public List<GameObject> prefabs;
    public GameObject loadingIcon;

    public static bool creatingNew = false;
    public static bool fromMenu = false;
    public UnityAction onSaveComplete = null;
    private static bool delegateAdded = false;

    private float lastSave = 0;
    private bool doYield = true;

    void Start() {
        InvokeRepeating("autoSave", 60, 60);
    }

    private void OnEnable() {
        if (!fromMenu) return;
        if (!delegateAdded) {
            delegateAdded = true;
            SceneManager.sceneLoaded += ExecuteLoad;
        }
    }

    void autoSave() {
        print("starting autosave");
        save(Scene_Controller.saveName);
    }

    public void save(string name) {
        lastSave = Time.time;

        StartCoroutine(saveASync(name));
    }

    private IEnumerator saveASync(string name) {
        print("starting async save, yield: " + doYield);

        loadingIcon.SetActive(true);
        float curTime = Time.realtimeSinceStartup;
        List<System.Object> toSave = new List<System.Object>();
        if (doYield)
            yield return new WaitForSecondsRealtime(0.1f);

        var rootObjects = SceneManager.GetActiveScene().GetRootGameObjects();
        var allObjects = new List<GameObject>();
        allObjects.AddRange(rootObjects);
        var editorTests = GameObject.Find("TestingObjects");
        if (editorTests != null) {
            foreach (Transform elem in editorTests.transform) {
                allObjects.Add(elem.gameObject);
            }
        }

        //iterate over all root objects (and TestingObjects)
        int i = 0;

        int foundObjects = 0;

        foreach (GameObject obj in allObjects) {
            if (obj == null) {
                continue;
            }

            var usedObj = obj;
            if (obj.name.StartsWith("Conveyors")) {
                try {
                    usedObj = obj.transform.GetChild(2).gameObject;
                }
                catch (Exception e) {
                    continue;
                }
            }

            try {
                if (usedObj.layer == 10) {
                    continue;
                }
            }
            catch (Exception ex) {
                print("object probably has been destroyed");
                continue;
            }

            if (i++ % 40 == 0 && doYield) {
                yield return new WaitForSecondsRealtime(0.0001f);
            }

            try {
                string prefabName = usedObj.name;
                prefabName = prefabName.Replace("Clone", "");
                prefabName = prefabName.Replace(" ", "");
                prefabName = prefabName.Replace("(", "");
                prefabName = prefabName.Replace(")", "");
                prefabName = Regex.Replace(prefabName, @"[\d-]", string.Empty);
                print("searching for prefab: " + prefabName);

                GameObject prefabFound = null;
                foreach (GameObject prefab in prefabs) {
                    if (prefab.name.Equals(prefabName)) {
                        prefabFound = prefab;
                        break;
                    }
                }

                if (prefabFound == null) {
                    //print("found no valid prefab, going to next item, searched for: " + prefabName);
                    continue;
                }

                GameObjectInfo info = new GameObjectInfo(usedObj);
                toSave.Add(info);
                foundObjects++;
            }
            catch (Exception ex) {
                print("Error while saving thing: " + ex);
            }
        }

//        foreach (System.Object obj in toSave) {
//            print(obj);
//        }

        //prevent game from saving invalid saves with too few objects
        if (foundObjects < 5) {
            yield break;
        }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + name);
        bf.Serialize(file, toSave);
        file.Close();
        print("saving done..., saved To: " + Application.persistentDataPath);

        float timeNeeded = Time.realtimeSinceStartup - curTime;
        print("Time needed: " + timeNeeded);
        loadingIcon.SetActive(false);

        if (onSaveComplete != null) {
            onSaveComplete.Invoke();
            onSaveComplete = null;
        }
    }

    private void OnApplicationPause(bool pauseStatus) {
        if (pauseStatus && Time.time - lastSave > 5f) {
            doYield = false;
            save(Scene_Controller.saveName);
        }
    }

    private void OnApplicationQuit() {
        print("exiting application!");
        if (Time.time - lastSave > 5f) {
            print("saving on exit");
            doYield = false;
            save(Scene_Controller.saveName);
        }
    }

    [Serializable]
    private class GameObjectInfo {
        public SerializableVector3 position;
        public SerializableQuaternion rotation;
        public SerializableVector3 scale;
        public string prefabName;
        public string originalName;
        public List<SerializationInfo> info = new List<SerializationInfo>();

        public GameObjectInfo(GameObject obj) {
            this.position = obj.transform.position;
            this.rotation = obj.transform.rotation;
            this.scale = obj.transform.localScale;
            this.prefabName = obj.name;
            this.originalName = obj.name;

            prefabName = prefabName.Replace("Clone", "");
            prefabName = prefabName.Replace(" ", "");
            prefabName = prefabName.Replace("(", "");
            prefabName = prefabName.Replace(")", "");
            prefabName = Regex.Replace(prefabName, @"[\d-]", string.Empty);
            SerializableInfo[] data = obj.GetComponents<SerializableInfo>();

            foreach (SerializableInfo elem in data) {
                info.Add(elem.getSerialize());
            }

            print("found ser. Info on obj: " + info.Count);
        }

        public override string ToString() {
            return prefabName + " scale=" + scale + " rotation=" + rotation + " position=" + position;
        }
    }

    [System.Serializable]
    public struct SerializableVector3 {
        /// <summary>
        /// x component
        /// </summary>
        public float x;

        /// <summary>
        /// y component
        /// </summary>
        public float y;

        /// <summary>
        /// z component
        /// </summary>
        public float z;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rX"></param>
        /// <param name="rY"></param>
        /// <param name="rZ"></param>
        public SerializableVector3(float rX, float rY, float rZ) {
            x = rX;
            y = rY;
            z = rZ;
        }

        /// <summary>
        /// Returns a string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return String.Format("[{0}, {1}, {2}]", x, y, z);
        }

        /// <summary>
        /// Automatic conversion from SerializableVector3 to Vector3
        /// </summary>
        /// <param name="rValue"></param>
        /// <returns></returns>
        public static implicit operator Vector3(SerializableVector3 rValue) {
            return new Vector3(rValue.x, rValue.y, rValue.z);
        }

        /// <summary>
        /// Automatic conversion from Vector3 to SerializableVector3
        /// </summary>
        /// <param name="rValue"></param>
        /// <returns></returns>
        public static implicit operator SerializableVector3(Vector3 rValue) {
            return new SerializableVector3(rValue.x, rValue.y, rValue.z);
        }
    }

    [System.Serializable]
    public struct SerializableQuaternion {
        /// <summary>
        /// x component
        /// </summary>
        public float x;

        /// <summary>
        /// y component
        /// </summary>
        public float y;

        /// <summary>
        /// z component
        /// </summary>
        public float z;

        /// <summary>
        /// w component
        /// </summary>
        public float w;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="rX"></param>
        /// <param name="rY"></param>
        /// <param name="rZ"></param>
        /// <param name="rW"></param>
        public SerializableQuaternion(float rX, float rY, float rZ, float rW) {
            x = rX;
            y = rY;
            z = rZ;
            w = rW;
        }

        /// <summary>
        /// Returns a string representation of the object
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return String.Format("[{0}, {1}, {2}, {3}]", x, y, z, w);
        }

        /// <summary>
        /// Automatic conversion from SerializableQuaternion to Quaternion
        /// </summary>
        /// <param name="rValue"></param>
        /// <returns></returns>
        public static implicit operator Quaternion(SerializableQuaternion rValue) {
            return new Quaternion(rValue.x, rValue.y, rValue.z, rValue.w);
        }

        /// <summary>
        /// Automatic conversion from Quaternion to SerializableQuaternion
        /// </summary>
        /// <param name="rValue"></param>
        /// <returns></returns>
        public static implicit operator SerializableQuaternion(Quaternion rValue) {
            return new SerializableQuaternion(rValue.x, rValue.y, rValue.z, rValue.w);
        }
    }

    void ExecuteLoad(Scene scene, LoadSceneMode mode) {
        print("scene loaded: " + scene.name);

        if (scene.name != "MainGame") {
            return;
        }

        SceneManager.SetActiveScene(scene);

        print("maingame saveload started");

        //Destroy testing objects
        GameObject.Destroy(GameObject.Find("TestingObjects"));

        Load();
        //StartCoroutine(this.Load());
    }

    private void Load() {
        print("loading last save");
        List<System.Object> list = null;

        LoadingProgressBar.getInstance()?.setInitLoadProgress(0f);
        Time.timeScale = 0.01f;
        //Debug.Break();

        //load "newGame" save
        if (creatingNew) {
            creatingNew = false;
            print("found createNew flag, loading different save! Accessing: " + Application.streamingAssetsPath +
                  "/newGameSave");

            var filePath = Application.streamingAssetsPath + "/newGameSave";
            var www = new WWW(filePath);
            //TODO wait for www to be done
            while (www.MoveNext());

            if (!string.IsNullOrEmpty(www.error)) {
                Debug.LogError("Can't read newGameSave");
            }

            BinaryFormatter bf = new BinaryFormatter();
            var ms = new MemoryStream(www.bytes);
            print("opened file");
            list = (List<System.Object>) bf.Deserialize(ms);

            print("loading done!");

            //load last save
        }
        else if (File.Exists(Application.persistentDataPath + "/default")) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/default", FileMode.Open);
            list = (List<System.Object>) bf.Deserialize(file);
            file.Close();

            print("file loading done!");
        }


        if (list == null) {
            print("Error, no save game found");
            return;
            //yield break;
        }

        print("deserializing...");

        List<GameObject> spawns = new List<GameObject>();
        float prog = 0;

        foreach (System.Object obj in list) {
            try {
                GameObjectInfo info = (GameObjectInfo) obj;

                GameObject prefab = null;

                foreach (GameObject elem in prefabs) {
                    if (elem.name.Equals(info.prefabName)) {
                        prefab = elem;
                        break;
                    }
                }

                print("creating " + info.prefabName);
                GameObject spawned = GameObject.Instantiate(prefab, info.position, info.rotation);
                spawned.name = info.originalName;
                spawned.transform.localScale = info.scale;
                /*for (int i = 0; i < info.info.Count; i++) {
                    //spawned.GetComponents<SerializableInfo>()[i].handleDeserialization(info.info[i]);
                    ((SerializableInfo) spawned.GetComponent(info.info[i].scriptTarget)).handleDeserialization(info.info[i]);
                }*/
                spawns.Add(spawned);

                LoadingProgressBar.getInstance()?.setInitLoadProgress(0.5f * (prog++ / list.Count));
            }
            catch (Exception e) {
                Console.WriteLine(e);
            }
        }

        print("all objects spawned, initing scripts...");

        for (int j = 0; j < spawns.Count; j++) {
            GameObjectInfo info = (GameObjectInfo) list[j];

            for (int i = 0; i < info.info.Count; i++) {
                try {
                    //spawned.GetComponents<SerializableInfo>()[i].handleDeserialization(info.info[i]);
                    ((SerializableInfo) spawns[j].GetComponent(info.info[i].scriptTarget)).handleDeserialization(
                        info.info[i]);
                }
                catch (Exception ex) {
                    print("unable to load data: " + ex);
                }
            }

            LoadingProgressBar.getInstance()?.setInitLoadProgress(0.5f + 0.5f * ((float) j / spawns.Count));
        }

        LoadingProgressBar.getInstance()?.setInitLoadProgress(1f);

        print("gameobjects added");

        Time.timeScale = 1f;
    }

    public interface SerializableInfo {
        SerializationInfo getSerialize();
        void handleDeserialization(SerializationInfo info);
    }

    [System.Serializable]
    public abstract class SerializationInfo {
        public abstract string scriptTarget { get; }
    }
}