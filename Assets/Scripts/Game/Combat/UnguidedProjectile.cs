﻿using System;
using Game.Combat;
using JetBrains.Annotations;
using UnityEngine;

public class UnguidedProjectile : MonoBehaviour, IProjectile {

	private GameObject target;
	private Vector3 startPos;
	private Vector3 impactPos;
	private float progress = 0;
	private float flightTime;
	private float maxHeight;
	private float damage;
	private float damageRadius;
	private bool done = false;
	private Quaternion randomRot;
	
	public float speed;
	public bool randomRotation = true;
	public GameObject explosionPrefab;
	
	public void init([NotNull] GameObject target, Vector3 impactPos, float damage, float damageRadius) {
		if (target == null) throw new ArgumentNullException("target");
		this.target = target;
		this.impactPos = impactPos;
		this.startPos = this.transform.position;
		this.flightTime = (impactPos - startPos).magnitude / speed;
		maxHeight = 4f * flightTime;
		this.damage = damage;
		this.damageRadius = damageRadius;
		randomRot = UnityEngine.Random.rotation;
	}

	private void FixedUpdate() {

		if (progress > 10f) this.GetComponent<HPHandler>().HP = 0;
		
		//formula: 4*x - 4*x^2
		var prog = progress / flightTime;
		var curPos = startPos + (impactPos - startPos) * prog;
		var heightAdd = (float) (4 * prog - 4 * Math.Pow(prog, 2)) * maxHeight * Vector3.up;
		curPos += heightAdd;

		if (!float.IsNaN(curPos.x)) {
			this.transform.position = curPos;
		}
		
		progress += Time.deltaTime;

		if (randomRotation) {
			this.transform.Rotate(randomRot.eulerAngles * Time.deltaTime);
		}

	}

	//returns the estimated position in now+delta time, used to make turrets more accurate
	public Vector3 futurePosition(float delta) {
		var fTime = progress + delta;
		fTime /= flightTime;
		var fPos = startPos + (impactPos - startPos) * fTime;
		var heightAdd = Vector3.up * maxHeight * (float) (4 * fTime - 4 * Math.Pow(fTime, 2));
		fPos += heightAdd;
		
		//print("future position in " + delta + " is: " + fPos);
		return fPos;
	}

	private void OnCollisionEnter(Collision other) {
		if (done) return;
		print("projectile hit: " + other.gameObject.name);
		done = true;
		
		var explosion = GameObject.Instantiate(explosionPrefab, this.transform.position, Quaternion.identity);
		explosion.GetComponent<SphereExplosion>().range = damageRadius;
		explosion.GetComponent<SphereExplosion>().Damage = damage;
		
		this.GetComponent<HPHandler>().HP = -1;
	}

	public HPHandler.Faction getFaction() {
		return this.GetComponent<HPHandler>().faction;
	}
}
