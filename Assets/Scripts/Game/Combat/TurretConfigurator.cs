﻿using UnityEngine;

namespace Content.Helpers.Combat {
    public abstract class TurretConfigurator : DefaultStructure, TurretConfigurator.ConfigurableTurret {
        public bool active = true;
        public float idleEnergyUsage = 0.1f;
        public float energyPerShot = 1f;
        public interface ConfigurableTurret {
            void setActive(bool val);
            bool getActive();
        }

        public override int getMaxEnergy() {
            return 250;
        }

        public override int getMaxOutput() {
            return 0;
        }

        public override int getMaxInput() {
            return 150;
        }

        public override int getPriority() {
            return 2;
        }

        public override void handleClick() {
            base.handleClick();
            if (salvaging || Salvaging.isActive()) return;
        
            Debug.Log("clicked structure");

            displayState(getActive());
        }
        private void deactivate() {
            this.GetComponent<ConfigurableTurret>().setActive(false);
            displayState(false);
        }

        private void activate() {
            this.GetComponent<ConfigurableTurret>().setActive(true);
            displayState(true);
        }

        private void displayState(bool state) {
            if (state) {
                Notification.createNotification(this.gameObject, Notification.sprites.Working, "Working....", Color.cyan,
                    true);
            }
            else {
                Notification.createNotification(this.gameObject, Notification.sprites.Stopping, "Idle", Color.yellow,
                    false);
            }
        }

        public void setActive(bool val) {
            this.active = val;
        }

        public bool getActive() {
            return active;
        }

        public override PopUpCanvas.popUpOption[] getOptions() {
            var activate = new PopUpCanvas.popUpOption( UIPrefabCache.ActivateBut, !getActive(), "enable", this.activate, null, (x) => !getActive());
            var deactivate = new PopUpCanvas.popUpOption(UIPrefabCache.DeactivateBut, getActive(), "disable", this.deactivate, null, (x) => getActive());

            return new[] {activate, deactivate};
        }
    }
}