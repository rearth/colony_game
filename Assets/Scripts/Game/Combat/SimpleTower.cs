using System;
using System.Collections.Generic;
using Content.Helpers.Combat;
using Game.Combat;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Content.Helpers {
    public abstract class SimpleTower : TurretConfigurator, IWeapon {
        public float Damage;

        public float attacksPerSecond;
        public float maxRange;

        public float minRange;

        //necessary to calculate projectile hit points
        public float bulletSpeed;

        internal GameObject enemy;
        internal Collider enemyCollider;
        internal float timeElapsed = 0f;
        protected EnergyContainer energyContainer;
        [SerializeField] private bool targetAir1;
        [SerializeField] private bool targetProjectiles1;


        public abstract string getName();

        public bool targetAir => targetAir1;

        public bool targetProjectiles => targetProjectiles1;

        public bool targetOnlyProjectile => targetOnlyMissile1;

        private IProjectile activeProjectile;
        [SerializeField] private bool targetOnlyMissile1;

        private void OnDrawGizmosSelected() {
            Gizmos.DrawWireSphere(this.transform.position, this.getRange());
        }

        public override void Start() {
            base.Start();
            energyContainer = this.GetComponent<EnergyContainer>();
            timeElapsed = 1 / attacksPerSecond;
        }

        public new virtual void FixedUpdate() {
            counter++;
            if (this.salvaging && this.getHP().HP < 3) {
                print("structure salvaged!");
                var pickup = Instantiate(GameObject.Find("Terrain").GetComponent<Scene_Controller>().pickupBox,
                    this.transform.position, Quaternion.identity);
                pickup.GetComponent<inventory>().add(new ressourceStack(salvageStartHP, ressources.Scrap));
                getHP().destruct();
            }

            if (salvaging) {
                this.getHP().HP -= 2.5f;
                return;
            }

            if (!active || energyContainer.getCurEnergy() < 1) return;

            energyContainer.addEnergy(-idleEnergyUsage * Time.deltaTime, energyContainer);

            updateEnemy();

            if (enemy == null) {
                return;
            }

            var impactPos = getImpactPos();
            timeElapsed += Time.deltaTime;
            if (timeElapsed >= 1 / attacksPerSecond) {
                timeElapsed -= 1 / attacksPerSecond;
                shoot(enemy, impactPos);
                energyContainer.addEnergy(-energyPerShot, energyContainer);
            }
        }

        protected void updateEnemy() {
            if (enemy == null && counter % 30 == 0) {
                enemy = findEnemy(useRandom());
                if (enemy != null) {
                    enemyCollider = enemy.GetComponent<Collider>();
                    activeProjectile = enemy.GetComponent<IProjectile>();
                }
            }
        }

        protected virtual Vector3 getImpactPos() {
            if (activeProjectile != null) {
                var estimatedTime = Vector3.Distance(enemy.transform.position, this.transform.position) / bulletSpeed;
                estimatedTime -= Time.deltaTime * 4;
                var pos = activeProjectile.futurePosition(estimatedTime);
                Debug.DrawRay(pos, this.transform.position - pos);
                return pos;
            }

            return enemyCollider.ClosestPoint(this.transform.position + Vector3.up * 10);
        }

        public abstract void shoot(GameObject target, Vector3 impactPos);
        public abstract bool useRandom();

        private bool targetValid(GameObject target) {
            var hp = target.GetComponent<HPHandler>();
            if (hp == null) return false;

            //check for projectile targets
            if (!targetProjectiles && (hp.foundFlags & HPHandler.props.Projectile) != 0) return false;
            if (!targetAir && (hp.foundFlags & HPHandler.props.Air) != 0) return false;
            if (targetOnlyProjectile && (hp.foundFlags & HPHandler.props.Projectile) == 0) return false;
            return (hp.foundFlags & HPHandler.props.HasCollider) != 0;
        }

        internal GameObject findEnemy(bool random) {
            if (!random) {
                return findEnemy();
            }

            //gather potential targets
            HPHandler.Faction self = this.GetComponent<HPHandler>().faction;
            var potentialEnemies = new List<GameObject>();

            foreach (HPHandler.Faction item in Enum.GetValues(typeof(HPHandler.Faction))) {
                //skip non-enemy factions
                if (item == self || item == HPHandler.Faction.Neutral) {
                    continue;
                }

                //loop through faction members
                var factionMembers = HPHandler.factionMembers;
                foreach (var enemy in factionMembers[item]) {
                    //calc dist
                    var dist = Vector3.Distance(this.gameObject.transform.position,
                        enemy.gameObject.transform.position);

                    //check dist
                    if (dist < maxRange && dist > minRange && targetValid(enemy)) {
                        potentialEnemies.Add(enemy);
                    }
                }
            }

            return potentialEnemies.Count == 0 ? null : potentialEnemies[Random.Range(0, potentialEnemies.Count)];
        }

        internal GameObject findEnemy() {
            //gather potential targets
            HPHandler.Faction self = this.GetComponent<HPHandler>().faction;
            GameObject target = null;
            float closestDistance = maxRange;

            foreach (HPHandler.Faction item in Enum.GetValues(typeof(HPHandler.Faction))) {
                //skip non-enemy factions
                if (item == self || item == HPHandler.Faction.Neutral) {
                    continue;
                }

                //loop through faction members
                var factionMembers = HPHandler.factionMembers;
                foreach (var enemy in factionMembers[item]) {
                    var dist = Vector3.Distance(this.gameObject.transform.position,
                        enemy.gameObject.transform.position);
                    if (dist < closestDistance && dist > minRange && targetValid(enemy)) {
                        target = enemy.gameObject;
                        closestDistance = dist;
                    }
                }
            }

            return target;
        }

        public float getDamage() {
            return Damage;
        }

        public float getRange() {
            return maxRange;
        }

        public void setRange(float range) {
            this.maxRange = range;
        }
    }
}