﻿using System.Collections;
using System.Collections.Generic;
using Game.Combat;
using UnityEngine;

public class BeamTurret : RotatingTurret {

    public LineRenderer beamLine;
    public Transform beamStartPivot;
    public Animator beamAnimator;
    public int maxEnergy;
    public override int getMaxEnergy() {
        return maxEnergy;
    }

    public override void Start() {
        base.Start();
        beamLine.useWorldSpace = true;

    }

    // Start is called before the first frame update
    public override string getName() {
        return "Beam Turret";
    }
    
    
    // Start is called before the first frame update
    public override void FixedUpdate() {
        base.FixedUpdate();
        if (enemyCollider == null) return;
        beamLine.SetPosition(0, beamStartPivot.position);
        beamLine.SetPosition(1, enemyCollider.bounds.center);
        
    }

    public override void shoot(GameObject target, Vector3 impactPos) {
        print("shooting beam turret at: " + target);
        beamLine.SetPosition(0, beamStartPivot.position);
        beamLine.SetPosition(1, impactPos);
        beamAnimator.SetTrigger("Fire");

        target.GetComponent<HPHandler>().inflictDamage(Damage, null);

    }

    public override bool useRandom() {
        return false;
    }

    protected override Vector3 getImpactPos() {
        return enemyCollider.bounds.center;
    }
}
