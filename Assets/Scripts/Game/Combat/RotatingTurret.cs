﻿using Content;
using Content.Helpers;
using UnityEngine;

namespace Game.Combat {
    public abstract class RotatingTurret : SimpleTower {
        public Transform rotationPart;
        public float turnSpeed;
        public float angleMustMatch;

        private bool isRotated;

        public override void FixedUpdate() {
            counter++;
            if (this.salvaging && this.getHP().HP < 3) {
                print("structure salvaged!");
                var pickup = Instantiate(GameObject.Find("Terrain").GetComponent<Scene_Controller>().pickupBox,
                    this.transform.position, Quaternion.identity);
                pickup.GetComponent<inventory>().add(new ressourceStack(salvageStartHP, ressources.Scrap));
                getHP().destruct();
            }

            if (salvaging) {
                this.getHP().HP -= 2.5f;
                return;
            }

            if (!active || energyContainer.getCurEnergy() < 1) return;

            energyContainer.addEnergy(-idleEnergyUsage * Time.deltaTime, energyContainer);
            updateEnemy();

            if (enemy == null) {
                return;
            }

            var impactPos = getImpactPos();
            rotateToTarget(impactPos);

            if (isRotated) {
                timeElapsed += Time.deltaTime;
                if (timeElapsed >= 1 / attacksPerSecond) {
                    timeElapsed -= 1 / attacksPerSecond;
                    shoot(enemy, impactPos);
                    energyContainer.addEnergy(-energyPerShot, energyContainer);
                }
            }
        }


        private void rotateToTarget(Vector3 target) {
            var ownPos = rotationPart.position;
            var toTarget = target - ownPos;

            Quaternion targetRotation = Quaternion.LookRotation(toTarget);

            var rotation = rotationPart.transform.rotation;
            var tar = Quaternion.Lerp(rotation, targetRotation, turnSpeed);

            isRotated = Mathf.Abs(Quaternion.Angle(targetRotation, rotation)) < angleMustMatch;

            rotation = tar;
            rotationPart.transform.rotation = rotation;
        }
    }
}