﻿using System;
using System.Collections.Generic;
using DigitalRuby.LightningBolt;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace Content.Helpers.Combat {
	public class UnitSelection : MonoBehaviour {
		
		public List<CombatUnit> activeSelected = new List<CombatUnit>();
		public Animator Anim;

		private Camera cam;
		private RectTransform rectAnim;
		private RadialMenuController controller;

		private void Awake() {
			cam = GameObject.Find("Main Camera").GetComponent<Camera>();
			rectAnim = GameObject.Find("UnitSelectorAnim").GetComponent<RectTransform>();
		}

		public void startSelection(RadialMenuController radialMenuController) {
			controller = radialMenuController;
			Debug.Log("starting unit selection");
			
			GameObject.Find("Main Camera").GetComponent<clickDetector>().setNextRayClickAction(handleRay);
			Anim.SetTrigger("play");
			selectUnitsOnScreen();
			TimeScaleHandler.setScale(0.1f);
		}

		private void selectUnitsOnScreen() {
			activeSelected.Clear();
			
			var potTargets = HPHandler.factionMembers[HPHandler.Faction.Terran];
			foreach (var unit in potTargets) {
				if (unit.GetComponent<CombatUnit>() == null) {
					continue;
				}
				var sPos = cam.WorldToScreenPoint(unit.transform.position);
				
				var success = RectTransformUtility.RectangleContainsScreenPoint(rectAnim, sPos);
				if (!success) continue;
				activeSelected.Add(unit.GetComponent<CombatUnit>());
					
				SkinnedMeshOutline effect = unit.AddComponent<SkinnedMeshOutline>();
				effect.OutlineColor = Color.blue;
				effect.OutlineWidth = 8.0f;
				var effect2 = unit.AddComponent<Outline>();
				effect2.OutlineColor = Color.blue;
				effect2.OutlineWidth = 8.0f;
//				var scaledPosX = sPos.x / Screen.width * referenceRes.x;
//				var scaledPosY = sPos.y / Screen.height * referenceRes.y;
			}

			if (activeSelected.Count < 1) {
				controller.cancelClicked();
			}
		}

		public void restoreNormal() {
			TimeScaleHandler.normalizeScale();

			foreach (var unit in activeSelected) {
				try {
					GameObject.Destroy(unit.GetComponent<SkinnedMeshOutline>());
					GameObject.Destroy(unit.GetComponent<Outline>());
				} catch (Exception e) {
					Console.WriteLine(e);
				}
			}
		}
		
		private void handleRay(Ray ray) {
			print("handling ray for wall creation: " + ray);
			RaycastHit raycastHit;

			if (!Physics.Raycast(ray, out raycastHit, 200.0f)) {
				print("raycast returned invalid!");
				GameObject.Find("Main Camera").GetComponent<clickDetector>().setNextRayClickAction(handleRay);
				return;
			}

			var hitPos = raycastHit.point;
			selectTarget(hitPos);
		}

		public void selectTarget(Vector3 target) {
			print("selected target: " + target);
			//order units
			NavMeshHit hit;
			var success = NavMesh.SamplePosition(target, out hit, 20f, NavMesh.AllAreas);
			print("found target: " + success);
			if (success) {
				foreach (var elem in activeSelected) {
					elem.moveTo(hit.position);
				}
				Notification.createIngamePointer(target, 1f);
			}
			
			controller.cancelClicked();
		}

		public void cancelClicked(RadialMenuController controller) {
			restoreNormal();
			GameObject.Find("Main Camera").GetComponent<clickDetector>().resetNextClick();
		}
	}
}
