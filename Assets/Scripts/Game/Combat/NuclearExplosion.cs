﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NuclearExplosion : MonoBehaviour {

    public float Damage;
    public float range;

    private void OnDrawGizmosSelected() {
        Gizmos.DrawWireSphere(this.transform.position, range);
    }

    void FixedUpdate() {

        //destroy stuff
        var hitColliders = Physics.OverlapSphere(this.transform.position, range);
        var i = 0;
        while (i < hitColliders.Length) {
            var hp = hitColliders[i].gameObject.GetComponent<HPHandler>();
            //var dist = Vector3.Distance(this.transform.position)
            if (hp != null) {
                hp.HP -= Damage;
            }
            i++;
        }
        GameObject.Destroy(this.gameObject, 12f);
    }
}
