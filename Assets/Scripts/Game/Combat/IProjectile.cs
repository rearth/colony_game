﻿using System;
using UnityEngine;

namespace Game.Combat {
    public interface IProjectile {
        HPHandler.Faction getFaction();
        
        Vector3 futurePosition(float delta);
    }
}