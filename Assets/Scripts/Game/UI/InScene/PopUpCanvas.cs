﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

public class PopUpCanvas : MonoBehaviour {

    public static Transform parentObj;
    
    public class popUpOption {
        public readonly Sprite sourceImage;
        public bool enabled;
        public string text;
        public UnityAction onClick;
        public Object param;
        public Func<object, bool> enabledCheck;
        public bool restoreUIOnClick;

        public bool getEnabled() {
            return enabledCheck == null || enabledCheck.Invoke(param);
        }

        public popUpOption(Sprite sourceImage, bool enabled, string text, UnityAction onClick, Object param = null, Func<object, bool> enabledCheck = null, bool restoreUiOnClick = true) {
            this.sourceImage = sourceImage;
            this.enabled = enabled;
            this.text = text;
            this.onClick = onClick;
            this.restoreUIOnClick = restoreUiOnClick;
            this.param = param;
            this.enabledCheck = enabledCheck;
        }

        public void setEnabled(bool b) {
            enabled = b;
        }
    }

    public void setParent(Transform parent) {
        Debug.Log("set gameobjecting for canvas");
        parentObj = parent;
    }
}
