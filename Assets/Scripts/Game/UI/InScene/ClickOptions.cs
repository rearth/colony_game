﻿using System.Collections.Generic;
using Content;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ClickOptions : MonoBehaviour {
    
    public GameObject Canvas;
    public GameObject template;
    public static GameObject inventoryBar;
    public static GameObject energyBar;
    public static GameObject HPBar;
    public GameObject assignEnergyBar;
    public GameObject assignInventoryBar;
    public GameObject assignHPBar;

    private GameObject curBarInv;
    private GameObject curBarEnergy;
    private GameObject curBarHP;
    private GameObject curTempDisplay;
    public GameObject tempDisplay;

    public static List<GameObject> PopUps = new List<GameObject>();
    public static bool UIOpen;

    private static float creationTime;

    void FixedUpdate() {
        if (curBarInv != null) {
            var inv = GetComponent<inventory>();
            curBarInv.transform.GetChild(0).GetComponent<Image>().fillAmount = inv.getFillPercent();
        }

        if (curBarEnergy != null) {
            var energy = GetComponent<EnergyContainer>();
            curBarEnergy.transform.GetChild(0).GetComponent<Image>().fillAmount = (float) energy.getCurEnergy() / energy.getMaxEnergy();

        }
        
        if (curBarHP != null) {
            var hp = GetComponent<HPHandler>();
            curBarHP.transform.GetChild(0).GetComponent<Image>().fillAmount = hp.HP / hp.getInitialHP();

        }

        if (curTempDisplay != null) {
            var textA = curTempDisplay.GetComponent<Text>();
            var textB = curTempDisplay.transform.GetChild(0).GetComponent<Text>();
            var temp = gameObject.GetComponent<reactorPart.IHeatableElement>();
            textA.text = temp.getTemp() + "°";
            textB.text = temp.getTemp() + "°";
            var col = new Color(temp.getTemp() / 1000, 1f, 0.02f);
            if (temp.getTemp() > 1000) {
                col = new Color(1f, 1f - ((temp.getTemp() - 1000) / 1000f), 0.02f);
            }
            //textA.color = col;
            textB.color = col;
        }
    }

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Start() {
        if (assignInventoryBar != null) {
            inventoryBar = assignInventoryBar;
        }
        
        if (assignEnergyBar != null) {
            energyBar = assignEnergyBar;
        }

        if (assignHPBar != null) {
            HPBar = assignHPBar;
        }

        if (Canvas == null) {
            Canvas = UIPrefabCache.Canvas;
        }

        if (template == null) {
            template = UIPrefabCache.Template;
        }
    }
    
    public void Create() {
        Debug.Log("Creating Click options for: " + name);
        UIOpen = true;

        creationTime = Time.fixedTime;

        GameObject cam = Camera.main.gameObject;

        Vector3 createAt = transform.position;
        Vector3 above = createAt;
        above.y += 30;

        RaycastHit hit;
        Physics.Raycast(above, createAt - above, out hit, 30.0f, Scene_Controller.getInstance().normalRayCheck);
        Debug.Log("Top of element is at: " + hit.point);

        Vector3 camUp = cam.transform.up;
        camUp.Normalize();
        camUp *= 2.0f;

        createAt = hit.point + camUp;

        GameObject parent = Instantiate(Canvas, createAt, cam.transform.rotation);
        PopUps.Add(parent);
        parent.GetComponent<PopUpCanvas>().setParent(transform);
        
        PopUpCanvas.popUpOption[] options = ((clickable) GetComponent(typeof(clickable))).getOptions();

        int count = 0;
        float elements = options.Length - 1;
        foreach (PopUpCanvas.popUpOption button in options) {
            GameObject obj = Instantiate(template, parent.transform);
            obj.GetComponent<Image>().sprite = button.sourceImage;

            Vector3 pos = new Vector3(3.0f * (count - elements / 2), 0, 0);
            obj.transform.localPosition = pos;
            obj.name = button.text;
            obj.GetComponent<Button>().onClick.AddListener(() => onButtonClicked(button.onClick));

            if (!button.getEnabled()) {
                obj.GetComponent<Button>().interactable = false;
            }

            count++;
        }
        
        var hp = GetComponent<HPHandler>();
        if (hp != null && !(tempDisplay != null && GetComponent<reactorPart.IHeatableElement>() != null && hp.HP / hp.getInitialHP() > 0.99)) {
            var obj =  Instantiate(HPBar, parent.transform);
            obj.transform.GetChild(0).GetComponent<Image>().fillAmount = hp.HP / hp.getInitialHP();
            curBarHP = obj;
        }

        var energy = GetComponent<EnergyContainer>();
        if (energy != null) {
            var obj =  Instantiate(energyBar, parent.transform);
            obj.transform.GetChild(0).GetComponent<Image>().fillAmount = (float) energy.getCurEnergy() / energy.getMaxEnergy();
            curBarEnergy = obj;
        }
        
        var inv = GetComponent<inventory>();
        if (inv != null) {
            var obj =  Instantiate(inventoryBar, parent.transform);
            obj.transform.GetChild(0).GetComponent<Image>().fillAmount = inv.getFillPercent();
            if (energy == null) {
                obj.transform.localPosition = energyBar.transform.localPosition;
            }
            curBarInv = obj;
        }

        if (tempDisplay != null && GetComponent<reactorPart.IHeatableElement>() != null) {
            //reactor part found
            var obj =  Instantiate(tempDisplay, parent.transform);
            var pos = obj.transform.localPosition;
            pos.y -= 1f;
            if (options.Length > 0) {
                pos.x -= 7.5f;
                pos.y -= 0.5f;
            }
            obj.transform.localPosition = pos;
            curTempDisplay = obj;
        }

    }
    
    private void onButtonClicked(UnityAction action) {
        clear();
        clickDetector.overlayClicked = true;
        action.Invoke();
    }

    public static void clear() {

        if (Time.fixedTime - creationTime < 0.3f) {
            return;
        }

        foreach (GameObject obj in PopUps) {
            obj.GetComponent<PopUpDestroyer>().destroy();
        }
        PopUps.Clear();
    }
}
