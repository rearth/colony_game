﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using Object = UnityEngine.Object;

public interface clickable {

    void handleClick();
    void handleLongClick();
    PopUpCanvas.popUpOption[] getOptions();

}
