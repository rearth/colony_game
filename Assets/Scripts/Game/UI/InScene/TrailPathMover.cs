﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TrailPathMover : MonoBehaviour {
    Vector3 target;
    Vector3 start;

    void Start() {
        start = this.transform.position;
    }

    void Update() {
        float totalDistance = Vector3.Distance(start, target);
        float done = Vector3.Distance(transform.position, start) / totalDistance;

        if (done >= 0.95f) {
            return;
        }

        Vector3 moveBy = target - start;
        //moveBy.Normalize();
        moveBy *= Time.unscaledDeltaTime * 2;

        moveBy.y += getY(done) * 0.5f;
        //moveBy = transform.rotation * moveBy;

        this.transform.Translate(moveBy, Space.World);
    }

    private float getY(float x) {
        //return (4f * x) - (4.0f * (x * x));
        return -1.9f * x + 1;
    }

    public void setPath(Vector3 target) {
        this.target = target;
    }

    public void setColor(Color mode) {
        var renderer = this.GetComponent<TrailRenderer>();

        Debug.Log("setting delivery color!");
        var grad = renderer.colorGradient;
        var keys = grad.colorKeys;
        Debug.Log("found keys: " + keys.Length);
        keys[0].color = mode;
        keys[1].color = mode;
        grad.SetKeys(keys, grad.alphaKeys);
        renderer.colorGradient = grad;
    }
}