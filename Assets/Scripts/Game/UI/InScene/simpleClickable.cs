﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class simpleClickable : SimpleStructure, clickable {

	public String description;
	
	public void handleClick() {
		if (Salvaging.isActive()) {
			Salvaging.createNotification(this.gameObject);
			return;
		}
	}

	public void handleLongClick() {
		this.gameObject.GetComponent<ClickOptions>().Create();
	}

	public void displayInfo() {
		var view = DetailViewManager.getInstance()
			.createView(description, this.gameObject.name, "Info", "", UIPrefabCache.InfoBut);

//		InfoClicked controller = InfoClicked.getInstance();
//		controller.show();
//        
//		controller.setTitle(this.gameObject.name);
//		controller.setDesc(description);
	}

	public PopUpCanvas.popUpOption[] getOptions() {
		PopUpCanvas.popUpOption info = new  PopUpCanvas.popUpOption(UIPrefabCache.InfoBut, true, "display info", displayInfo);
        
		var options = new[]{info};
		return options;
	}
}
