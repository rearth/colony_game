﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class OutlineTemp : MonoBehaviour {

    private List<Material> applied = new List<Material>();
    public float width;
    public Color color;
    public bool playOnStart = false;
    
    private static readonly int Width = Shader.PropertyToID("_Width");
    private static readonly int OutlineColor = Shader.PropertyToID("_OutlineColor");

    // Start is called before the first frame update
    void Start() {
        var mat = Scene_Controller.getOutlineMat();
        var renderers = GetComponentsInChildren<Renderer>();
        
        foreach (var renderer in renderers) {
            var newMat = new Material(Scene_Controller.getOutlineMat());
            
            var mats = renderer.materials.ToList();
            mats.Add(newMat);
            
            newMat.SetColor(OutlineColor, color);
            newMat.SetFloat(Width, width);
            newMat.name = "outline_temp_" + playOnStart;
            
            if (playOnStart) newMat.SetFloat(Width, 0.001f);
            
            applied.Add(newMat);
            renderer.materials = mats.ToArray();
        }

        if (playOnStart) {
            startClickAnim();
        }
    }

    private void startClickAnim() {
        LeanTween.value(this.gameObject, onUpdate, 0.001f, width, 0.6f).setEase(Scene_Controller.getClickAnimCurve()).setOnComplete(onComplete);
    }

    private void onComplete() {
        var renderers = GetComponentsInChildren<Renderer>();
        
        foreach (var renderer in renderers) {
            var mats = renderer.materials.ToList();
            mats.Remove(mats.Last());
            renderer.materials = mats.ToArray();
        }
        GameObject.Destroy(this);
    }

    private void onUpdate(float obj) {
        foreach (var elem in applied) {
            elem.SetFloat(Width, obj);
        }
    }

    public void clear() {
        onComplete();
    }
}