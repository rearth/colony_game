﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Object = System.Object;

public class ObjectDetailView : MonoBehaviour {
    public GameObject buttonPrefab;
    public Text Title;
    public Text desc;
    public Text statsTitle;
    public Text statsDesc;
    public Image icon;
    private int buttonOffset = 45;

    private readonly List<(Button but, Func<Object, bool> enabledCheck, Object param)> buttons =
        new List<(Button but, Func<object, bool> enabledCheck, object param)>();

    private int buttonCount;

    public void addAction(string text, bool enabled, UnityAction onClick, Object param = null,
        Func<Object, bool> enabledCheck = null, bool restoreUI = true) {
        var pos = buttonPrefab.transform.position;
        pos.y -= buttonOffset * buttonCount;
        var obj = GameObject.Instantiate(buttonPrefab, pos, buttonPrefab.transform.rotation, this.transform);
        obj.GetComponent<Button>().interactable = enabled;
        obj.transform.GetChild(0).GetComponent<Text>().text = text;
        obj.GetComponent<Button>().onClick.AddListener(() => buttonClicked(onClick, restoreUI));
        obj.GetComponent<RectTransform>().anchoredPosition = pos;

        buttonCount++;

        if (enabledCheck != null) {
            buttons.Add((obj.GetComponent<Button>(), enabledCheck, param));
        }
    }

    private void buttonClicked(UnityAction onClick, bool restoreUI) {
        clickDetector.overlayClicked = true;
        remove();
        if (restoreUI)
            RadialMenuController.getInstance().restoreUI();
        onClick.Invoke();
    }

    private void FixedUpdate() {
        var toRemove = new List<(Button but, Func<object, bool> enabledCheck, object param)>();
        foreach (var (but, enabledCheck, param) in buttons) {
            if (but == null) {
                toRemove.Add((but, enabledCheck, param));
                continue;
            }

            but.interactable = enabledCheck.Invoke(param);
        }

        foreach (var elem in toRemove) {
            buttons.Remove(elem);
        }
    }

    public void remove() {
        GameObject.Destroy(this.gameObject);
    }

    public void setTitle(string title) {
        Title.text = title;
    }

    public void setDesc(string desc) {
        this.desc.text = desc;
    }

    public void setStats(string title, string content) {
        statsTitle.text = title;
        statsDesc.text = content;
    }

    public void setImage(Sprite image) {
        icon.sprite = image;
    }
}