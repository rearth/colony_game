﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game.Structures.Placement;
using UnityEngine;
using UnityEngine.UI;
using Object = System.Object;

public class BuildMenuController : MonoBehaviour {
    public Transform kategoryParent;
    public Transform kategorySelected;
    public GameObject buildingFramePrefab;
    public GameObject arrow_right;
    public GameObject arrow_left;
    private RadialMenuController controller;

    private List<GameObject> structureDisplays = new List<GameObject>();

    private int maxDisplayCount;
    private int curPage = 0;

    public enum buildKategories {
        Processing,
        Power,
        Combat,
        Special
    }

    public void openKategoryOverview(RadialMenuController controller) {
        kategoryParent.gameObject.SetActive(true);
        this.controller = controller;
    }

    public void onCategoryClicked(int categoryIndex) {
        clickDetector.overlayClicked = true;
        var category = (buildKategories) categoryIndex;
        print("selected category: " + category);
        kategoryParent.gameObject.SetActive(false);
        showCategory(category);
    }

    public void onCancelClicked(RadialMenuController radialMenuController) {
        reset();
    }

    private void reset() {
        print("closing build menu");
        kategoryParent.gameObject.SetActive(false);
        kategorySelected.gameObject.SetActive(false);
        arrow_left.SetActive(false);
        arrow_right.SetActive(false);
        curPage = 0;
    }

    private void showCategory(buildKategories category) {
        clearDisplays();
        kategorySelected.gameObject.SetActive(true);

        var buildings = GameObject.Find("Terrain").GetComponent<BuildingManager>().getBuildings().Values.ToList();
        var filteredBuildings = buildings.Where(x => x.category.Equals(category));
        var i = 0;
        const int spacing = 15;

        var availWidth = kategorySelected.GetComponent<RectTransform>().rect.width + 60;
        var individualWidth = buildingFramePrefab.GetComponent<RectTransform>().rect.width + spacing;
        maxDisplayCount = (int) (availWidth / individualWidth - 0.9f);
        maxDisplayCount = maxDisplayCount > filteredBuildings.Count() ? filteredBuildings.Count() : maxDisplayCount;

        var usedWidth = maxDisplayCount * individualWidth;
        var xOffset = individualWidth / 2;

        print("displayCount: " + maxDisplayCount + " x: " + xOffset);

        foreach (var data in filteredBuildings) {
            var useI = i % (maxDisplayCount * 2);
            var iX = useI;
            var posY = 60;
            if (useI >= maxDisplayCount) {
                posY *= -1;
                iX = useI - maxDisplayCount;
            }

            var posX = -usedWidth / 2 + individualWidth * iX + xOffset;

            var display = GameObject.Instantiate(buildingFramePrefab, new Vector3(posX, posY + 195, 0),
                Quaternion.identity, kategorySelected);
            display.GetComponent<RectTransform>().anchoredPosition = new Vector2(posX, posY);
            display.transform.GetChild(0).GetComponent<Text>().text = data.name;
            display.transform.GetChild(1).GetComponent<Image>().sprite = data.icon;
            display.GetComponent<Button>().onClick.AddListener(() => buildingClicked(data));
            structureDisplays.Add(display);
            if (i >= maxDisplayCount * 2) display.SetActive(false);
            i++;
        }

        arrow_right.SetActive((curPage + 1) * maxDisplayCount * 2 < structureDisplays.Count);
    }

    public void conveyorButClicked() {
        clickDetector.overlayClicked = true;
        reset();
        ConveyorCreator.getInstance().onConveyorButtonClicked();
        controller.clearUI((x) => ConveyorCreator.getInstance().cancel());
        controller.quickStructureConveyor();
    }

    public void foundationButClicked() {
        clickDetector.overlayClicked = true;
        reset();
        var wallCreator = this.GetComponent<WallCreator>();
        wallCreator.surfaceSelected();
        controller.clearUI((x) => wallCreator.cancelPressed(), true, (x) => wallCreator.finishWallbuilding());
    }

    public void salvageButClicked() {
        clickDetector.overlayClicked = true;
        reset();
        GameObject.Find("Terrain").GetComponent<Salvaging>().startSalvageMode();
    }

    public void NavRight() {
        curPage++;
        updatePageDisplay();
    }

    public void NavLeft() {
        curPage--;
        updatePageDisplay();
    }

    private void updatePageDisplay() {
        var minIndex = maxDisplayCount * 2 * curPage;
        var maxIndex = maxDisplayCount * 2 * (curPage + 1);

        for (int i = 0; i < structureDisplays.Count; i++) {
            var display = structureDisplays[i];
            display.SetActive(i >= minIndex && i < maxIndex);
        }
        
        arrow_left.SetActive(curPage > 0);
        arrow_right.SetActive((curPage + 1) * maxDisplayCount * 2 < structureDisplays.Count);
    }

    private void buildingClicked(BuildingManager.structureData data) {
        clickDetector.overlayClicked = true;
        
        print("clicked building icon ! " + data.name);
        reset();
        var cost = data.cost.Aggregate("", (current, stack) => current + (stack.getRessource() + ": <color=gray>" + stack.getAmount() + "</color>\n"));
        var view = DetailViewManager.getInstance().createView(data.description, data.name, "Cost:", cost, data.icon);
        view.addAction("Build", true, () => onBuildClicked(data), data, isPlacableDelegate, false);
        controller.onCancel = menuController => DetailViewManager.getInstance().clear();
    }

    private bool isPlacableDelegate(Object param) {
        var data = (BuildingManager.structureData) param;

        return data.cost.All(elem => !(elem.getAmount() > ResourceHandler.getAmoumt(elem.getRessource())));
    }

    private void onBuildClicked(BuildingManager.structureData data) {
        clickDetector.overlayClicked = true;
        print("build clicked with data: " + data);
        //controller.cancelClicked();
        reset();
        GameObject.Find("Terrain").GetComponent<Building>().buildClicked(data);
        controller.updateQuickStrucutre(data);
    }

    private void clearDisplays() {
        foreach (var display in structureDisplays) {
            GameObject.Destroy(display);
        }

        structureDisplays.Clear();
    }
}