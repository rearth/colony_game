﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DetailViewManager : MonoBehaviour {
    public GameObject detailViewPrefab;

    private ObjectDetailView curActive;
    private static DetailViewManager instance;

    private void Start() {
        instance = this;
    }

    public static DetailViewManager getInstance() {
        return instance;
    }

    public ObjectDetailView createView(string desc, string title, string statsTitle, string statsDesc, Sprite image) {
        if (curActive != null) curActive.remove();

        curActive = GameObject.Instantiate(detailViewPrefab, detailViewPrefab.transform.position,
            detailViewPrefab.transform.rotation, this.transform).GetComponent<ObjectDetailView>();
        curActive.gameObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        curActive.setDesc(desc);
        curActive.setTitle(title);
        curActive.setStats(statsTitle, statsDesc);
        curActive.setImage(image);

        return curActive;
    }

    public void clear() {
        if (curActive != null)
            curActive.remove();
    }
}