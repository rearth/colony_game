﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeScaleHandler : MonoBehaviour {

	private static float targetScale = 1f;

	public GameObject display;

	public static void setScale(float scale) {
		Time.timeScale = scale;
		Time.fixedDeltaTime = 0.04f * Time.timeScale;
	}

	public static void normalizeScale() {
		Time.timeScale = targetScale;
		Time.fixedDeltaTime = 0.04f * Time.timeScale;
	}

	public void increaseTimeScale() {
		clickDetector.overlayClicked = true;
		targetScale += 0.5f;
		if (targetScale > 5f) {
			targetScale = 5f;
		}
		Time.timeScale = targetScale;
		Time.fixedDeltaTime = 0.04f * Time.timeScale;
		Debug.Log("increasing time scale to: " + targetScale);
		drawScale();
	}

	public void decreaseTimeScale() {
		clickDetector.overlayClicked = true;
		Debug.Log("decreasing time scale");
		targetScale -= 0.1f;
		if (targetScale < 0.1f) {
			targetScale = 0.1f;
		}
		Time.timeScale = targetScale;
		Time.fixedDeltaTime = 0.04f * Time.timeScale;
		drawScale();
	}

	public void resetTimeScale() {
		clickDetector.overlayClicked = true;
		Debug.Log("resetting time scale");
		targetScale = 1f;
		Time.timeScale = targetScale;
		Time.fixedDeltaTime = 0.04f * Time.timeScale;
		display.GetComponent<Text>().text = Math.Round(targetScale, 1).ToString();
		display.GetComponent<UnityEngine.UI.Outline>().enabled = false;
	}

	private void drawScale() {
		display.GetComponent<Text>().text = Math.Round(targetScale, 1).ToString();
		display.GetComponent<UnityEngine.UI.Outline>().enabled = true;
	}
}
