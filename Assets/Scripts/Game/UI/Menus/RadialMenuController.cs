﻿using System;
using System.Linq;
using System.Timers;
using Content.Helpers.Combat;
using Game.Structures.Placement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RadialMenuController : MonoBehaviour {

    public Animator animator;
    public Transform homeCameraPivot;
    public UnitSelection unitSelection;
    public BuildMenuController buildMenuController;
    public Image structurePreview;
    public Text structureTitle;
    public GameObject confirmBut;
    public Sprite conveyorIcon;
    
    private static readonly int CloseRadialMenu = Animator.StringToHash("closeRadialMenu");
    private static readonly int OpenRadialMenu = Animator.StringToHash("openRadialMenu");
    private static readonly int ClearUi = Animator.StringToHash("clearUI");
    private static readonly int RestoreUi = Animator.StringToHash("restoreUI");
    private static RadialMenuController instance;
    private BuildingManager.structureData lastPlacementData;
    private bool lastBuildingWasConveyor = false;

    public Action<RadialMenuController> onCancel;
    public Action<RadialMenuController> onConfirm;

    private void Start() {
        lastPlacementData = GameObject.Find("Terrain").GetComponent<BuildingManager>().getBuildings().First().Value;
        updatePlacementPreview();
        instance = this;
    }

    public void quickStructureConveyor() {
        lastBuildingWasConveyor = true;
        updatePlacementPreview();
    }

    public void updateQuickStrucutre(BuildingManager.structureData data) {
        lastPlacementData = data;
        lastBuildingWasConveyor = false;
        updatePlacementPreview();
    }

    private void updatePlacementPreview() {
        if (lastBuildingWasConveyor) {
            structurePreview.sprite = conveyorIcon;
            structureTitle.text = "build conveyor";
            return;
        }
        structurePreview.sprite = lastPlacementData.icon;
        structureTitle.text = "build " + lastPlacementData.name.ToLower();
    }
    
    public void openMenuClicked() {
        clickDetector.overlayClicked = true;
        animator.SetTrigger(OpenRadialMenu);
    }

    public void closeMenuClicked() {
        clickDetector.overlayClicked = true;
        animator.SetTrigger(CloseRadialMenu);
    }

    public void camHomeClicked() {
        clickDetector.overlayClicked = true;
        LeanTween.move(Camera.main.gameObject, homeCameraPivot, 0.5f);
        closeMenuClicked();
    }

    public void selectUnitsClicked() {
        clickDetector.overlayClicked = true;
        animator.SetTrigger(ClearUi);
        unitSelection.startSelection(this);
        onCancel = unitSelection.cancelClicked;
    }

    public void cancelClicked() {
        print("radial menu cancel clicked");
        clickDetector.overlayClicked = true;
        animator.SetTrigger(RestoreUi);
        onCancel?.Invoke(this);
        onCancel = null;
        onConfirm = null;
    }

    public void confirmClicked() {
        print("radial menu confirm clicked");
        clickDetector.overlayClicked = true;
        animator.SetTrigger(RestoreUi);
        onConfirm?.Invoke(this);
        onCancel = null;
        onConfirm = null;
    }
    
    public void openBuildMenuClicked() {
        animator.SetTrigger(ClearUi);
        clearUI(buildMenuController.onCancelClicked);
        buildMenuController.openKategoryOverview(this);
    }

    public void leaveGameClicked() {
        Time.timeScale = 0;
        GameObject.Find("Terrain").GetComponent<SaveLoad>().onSaveComplete = savingDone;
        GameObject.Find("Terrain").GetComponent<SaveLoad>().save(Scene_Controller.saveName);
        EnergyHandler.allContainers.Clear();
        HPHandler.factionMembers.Clear();
    }

    private void savingDone() {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void clearUI(Action<RadialMenuController> cancelAction, bool useConfirmBut = false, Action<RadialMenuController> confirmAction = null) {
        animator.SetTrigger(ClearUi);
        onCancel = cancelAction;
        confirmBut.SetActive(useConfirmBut);
        onConfirm = confirmAction;
    }

    public void restoreUI() {
        animator.SetTrigger(RestoreUi);
        onCancel = null;
        onConfirm = null;
        confirmBut.SetActive(false);
    }

    public void lastBuildingClicked() {
        clickDetector.overlayClicked = true;
        print("build clicked with data: " + lastPlacementData);
        animator.SetTrigger(ClearUi);
        if (lastBuildingWasConveyor) {
            ConveyorCreator.getInstance().onConveyorButtonClicked();
            clearUI((x) => ConveyorCreator.getInstance().cancel());
        }
        else {
            GameObject.Find("Terrain").GetComponent<Building>().buildClicked(lastPlacementData);
        }
    }

    public static RadialMenuController getInstance() {
        return instance;
    }

}
