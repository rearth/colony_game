﻿using System.Collections;
using System.Collections.Generic;
using System;
using Content;
using Game.Structures.Placement;
using UnityEngine;

public class ReactorController : MonoBehaviour, clickable, Structure {

    public Sprite infoBut;
    public Sprite activateBut;
    public Sprite deactivateBut;
    public Sprite buildCoreBut;
    public GameObject corePrefab;
    public GameObject corePlacement;
    public Sprite buildBoilerBut;
    public GameObject boilerPrefab;
    public GameObject boilerPlacement;
    public Sprite buildWallBut;
    public GameObject wallPrefab;
    public GameObject wallPlacement;
    
    bool salvaging = false;

    public void displayInfo() {
        var data = BuildingManager.getDataByName(this.gameObject.name);

        var view = DetailViewManager.getInstance()
            .createView(data.description, data.name, "Info", getStats(), data.icon);
        RadialMenuController.getInstance().clearUI((x) => view.remove());

        foreach (var option in getOptions()) {
            if (option.text.ToLower().StartsWith("info")) continue;
            view.addAction(option.text, option.enabled, option.onClick, option.param, option.enabledCheck, option.restoreUIOnClick);
        }
    }

    private string getStats() {
        return "Input uranium, produces depleted uranium\nStored Uranium: " + getInv().getAmount(ressources.Uranium);
    }

    public static ressourceStack[] getPrice() {
        ressourceStack[] cost = new ressourceStack[4];

        cost[0] = new ressourceStack(200, ressources.Wood);
        cost[1] = new ressourceStack(200, ressources.Stone);
        cost[2] = new ressourceStack(500, ressources.Iron);
        cost[3] = new ressourceStack(100, ressources.Gold);
        return cost;
    }

    public PopUpCanvas.popUpOption[] getOptions() {

        var info = new PopUpCanvas.popUpOption(infoBut, true, "info", displayInfo);
        var buildReactor = new PopUpCanvas.popUpOption(buildCoreBut, true, "build reactor core", buildReactorCore, null, null, false);
        var buildBoiler = new PopUpCanvas.popUpOption(buildBoilerBut, true, "build boiler", buildReactorBoiler, null, null, false);
        var buildWall = new PopUpCanvas.popUpOption(buildWallBut, true, "build reactor wall", buildReactorWall, null, null, false);
        var activate = new PopUpCanvas.popUpOption(activateBut, true, "start reactor", this.activate, null, canStart);
        var deactivate = new PopUpCanvas.popUpOption(deactivateBut, true, "stop reactor", this.deactivate, null, canStop);

        return new[] { info, activate, deactivate, buildReactor, buildBoiler, buildWall};
    }

    public void handleClick() {
        Debug.Log("clicked dome");

        if (Salvaging.isActive()) {
            Salvaging.createNotification(this.gameObject);
            return;
        }

        if (salvaging) {
            return;
        }
    }
    public void salvage() {
        Debug.Log("Got salvage request!");
        this.salvaging = true;
        Salvaging.displayIndicator(this.gameObject);
    }

    public void handleLongClick() {
        this.GetComponent<ClickOptions>().Create();
        return;
    }

    private bool canStart(object o) {
        return !this.gameObject.GetComponent<ReactorLogic>().isActive();
    }

    private bool canStop(object o) {
        return this.gameObject.GetComponent<ReactorLogic>().isActive();
    }

    private void deactivate() {
        this.gameObject.GetComponent<ReactorLogic>().deactivate();
    }

    private void activate() {
        this.gameObject.GetComponent<ReactorLogic>().activate();
    }

    private void buildReactorWall() {
        BuildingManager.structureData data;
        data = new BuildingManager.structureData(wallPrefab, wallPlacement, null, "reactorwall", "reactorwall",
            new List<ressourceStack> {
                new ressourceStack(100, ressources.Stone),
                new ressourceStack(50, ressources.Iron)
            }, BuildMenuController.buildKategories.Power,
            isNearReactor);
        GameObject.Find("Terrain").GetComponent<Building>().buildClicked(data);
        GameObject.Find("Terrain").GetComponent<Building>().setOverrideCost(data.cost.ToArray());
    }

    private void buildReactorBoiler() {
        BuildingManager.structureData data;
        data = new BuildingManager.structureData(boilerPrefab, boilerPlacement, null, "reactorboiler", "reactorboiler",
            new List<ressourceStack> {
                new ressourceStack(200, ressources.Stone),
                new ressourceStack(200, ressources.Wood),
                new ressourceStack(500, ressources.Iron)
            }, BuildMenuController.buildKategories.Power,
            isNearReactor);
        GameObject.Find("Terrain").GetComponent<Building>().buildClicked(data);
        GameObject.Find("Terrain").GetComponent<Building>().setOverrideCost(data.cost.ToArray());
    }

    private void buildReactorCore() {
        BuildingManager.structureData data;
        data = new BuildingManager.structureData(corePrefab, corePlacement, null, "reactorcore", "reactorcore",
            new List<ressourceStack> {
                new ressourceStack(250, ressources.Stone),
                new ressourceStack(50, ressources.Wood),
                new ressourceStack(20, ressources.Gold),
                new ressourceStack(200, ressources.Iron)
            }, BuildMenuController.buildKategories.Power,
            isNearReactor);
        GameObject.Find("Terrain").GetComponent<Building>().buildClicked(data);
        GameObject.Find("Terrain").GetComponent<Building>().setOverrideCost(data.cost.ToArray());
    }

    private bool isNearReactor(GameObject holoPlacement, bool terrainCheck) {

        if (!terrainCheck) {
            return false;
        }

        var parts = GameObject.FindGameObjectsWithTag("reactorPart");
        foreach (var item in parts) {
            //check if an object with the "reactorPart" tag is within x meters of the placement structure
            if (Vector3.Distance(item.transform.position, holoPlacement.transform.position) < 8) {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate() {
        if (this.salvaging && this.getHP().HP < 3) {
            print("structure salvaged!");
            var pickup = Instantiate(GameObject.Find("Terrain").GetComponent<Scene_Controller>().pickupBox, this.transform.position, Quaternion.identity);
            pickup.GetComponent<inventory>().add(new ressourceStack(this.getHP().getInitialHP(), ressources.Scrap));
            GameObject.Destroy(this.gameObject);
        }

        if (salvaging) {
            this.getHP().HP -= 2.5f;
            return;
        }
    }

    public bool isWorking() {
        return !salvaging;
    }

    public GameObject getGameobject() {
        return this.gameObject;
    }

    public ressourceStack[] getCost() {
        return getPrice();
    }

    public ressourceStack[] getResources() {
        return new ressourceStack[] { new ressourceStack(1000, ressources.Scrap) };
    }

    public HPHandler getHP() {
        return this.GetComponent<HPHandler>();
    }

    public inventory getInv() {
        return this.GetComponent<inventory>();
    }

    public bool isSalvaging() {
        return salvaging;
    }
}
