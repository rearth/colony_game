﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Structures.Placement {
    public class BuildingManager : MonoBehaviour {
        
        private readonly Dictionary<string, structureData> availBuildings = new Dictionary<string, structureData>();

        public List<placementData> autoPrefabs = new List<placementData>();

        // Use this for initialization
        void Awake() {
            initBuildings();
        }

        private void initBuildings() {
        
            //add auto-generated  buildings (new system)
            foreach (var elem in autoPrefabs) {
                structureData.requirementcheck check = null;
                if (elem.dispName.Contains("Water Pump")) check = waterpump.isNearWater;
                availBuildings.Add(elem.prefab.name,
                    new structureData(elem.prefab, elem.placement, elem.icon, elem.desc, elem.dispName, elem.cost, elem.category, check));
            }
        }

        public Dictionary<string, structureData> getBuildings() {
            return availBuildings;
        }

        public static string getNiceString(List<ressourceStack> cost) {
            string text = "";

            foreach (var elem in cost) {
                text += elem.getRessource().ToString() + ": " + elem.getAmount() + " ";
            }

            return text;
        }

        public static structureData getDataByName(string name) {
            var instance = Scene_Controller.getInstance().buildingManager;
            return (from data in instance.availBuildings where name.ToLower().StartsWith(data.Key.ToLower()) select data.Value).FirstOrDefault();
        }

        [System.Serializable]
        public class placementData {
            public GameObject prefab;
            public GameObject placement;
            public Sprite icon;
            public String dispName;
            public String desc;
            public List<ressourceStack> cost;
            public BuildMenuController.buildKategories category;
        }

        [System.Serializable]
        public class structureData {
            public delegate bool requirementcheck(GameObject holoPlacement, bool terrainCheck);

            public readonly GameObject prefab;
            public readonly GameObject placement;
            public readonly Sprite icon;
            internal readonly string description;
            public readonly string name;
            public List<ressourceStack> cost;
            public readonly requirementcheck buildCheck;
            public readonly BuildMenuController.buildKategories category;

            public structureData(GameObject prefab, GameObject placement, Sprite icon, string description, string name, List<ressourceStack> cost, BuildMenuController.buildKategories category, requirementcheck buildCheck = null) {
                this.prefab = prefab;
                this.placement = placement;
                this.icon = icon;
                this.description = description;
                this.name = name;
                this.cost = cost;
                this.buildCheck = buildCheck;
                this.category = category;
            }
        }
    }
}