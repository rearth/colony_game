﻿using System.Collections;
using System.Collections.Generic;
using Content;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public class ScrapBurner : TogglableStructure {
    private GameObject workLight = null;

    public static ressourceStack[] getPrice() {
        ressourceStack[] cost = new ressourceStack[2];

        cost[0] = new ressourceStack(100, ressources.Wood);
        cost[1] = new ressourceStack(250, ressources.Stone);
        //TODO
        //cost[2] = new HPHandler.ressourceStack(20, HPHandler.ressources.Scrap);
        return cost;
    }

    public override ressourceStack[] getCost() {
        return getPrice();
    }

    public override int getMaxEnergy() {
        return 3000;
    }

    public override int getMaxInput() {
        return getMaxOutput();
    }

    public override int getMaxOutput() {
        return 200;
    }

    public override int getPriority() {
        return 10;
    }

    protected override void doStop() {
        base.doStop();
        this.GetComponent<Animator>().SetBool("working", false);
        workLight.GetComponent<Light>().enabled = false;
        this.transform.Find("DrillSparks").GetComponent<ParticleSystem>().Stop();
    }

    private float energyPerSecond = 30f;
    private float scrapPerSecond = 5f;

    new void FixedUpdate() {

        base.FixedUpdate();

        if (workLight == null) {
            workLight = this.transform.Find("DrillSparks").transform.Find("Point Light").gameObject;
        }

        if (this.isBusy()) {
            //check if it has scrap, if true then recycle INFO: 1 Scrap = 3 Energy
            if (this.GetComponent<inventory>().getAmount(ressources.Scrap) >= 1) {
                this.addEnergy(energyPerSecond * Time.deltaTime, this);
                this.GetComponent<inventory>().remove(new ressourceStack(scrapPerSecond * Time.deltaTime, ressources.Scrap));
            }

            if (this.GetComponent<inventory>().getAmount(ressources.Scrap) >= 1) {
                this.GetComponent<Animator>().SetBool("working", true);
                workLight.GetComponent<Light>().enabled = true;
                this.transform.Find("DrillSparks").GetComponent<ParticleSystem>().Play();
            } else {
                this.GetComponent<Animator>().SetBool("working", false);
                workLight.GetComponent<Light>().enabled = false;
                this.transform.Find("DrillSparks").GetComponent<ParticleSystem>().Stop();
            }
            //send for moar scrap!!
            if (counter % 60 == 0 && !this.GetComponent<inventory>().isFull()) {
                RessourceHelper.deliverTo(this.gameObject, false, ressources.Scrap);
            }
        }

    }
}
