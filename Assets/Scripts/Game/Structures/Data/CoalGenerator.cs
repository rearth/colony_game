﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoalGenerator : ProductionStructure {
    public override int getMaxOutput() {
        return 100;
    }

    public override int getMaxInput() {
        return 0;
    }

    public override int getPriority() {
        return 10;
    }

    public override List<ProduceData> getProduceData() {
        var list = new List<ProduceData> {
            new ProduceData() {
                consume = new List<ressourceStack>() {new ressourceStack(2, ressources.Coal)},
                outcome = new List<ressourceStack>() {},
                energyProduce = 50f
            }
        };
        return list;
    }
}