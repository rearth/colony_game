﻿using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

[RequireComponent(typeof(inventory), typeof(HPHandler), typeof(ClickOptions))]
public class WarheadAssembler : ProductionStructure {
	public override List<ProduceData> getProduceData() {
		var list = new List<ProduceData> {
			new ProduceData() {
				consume = new List<ressourceStack>() {new ressourceStack(3, ressources.DepletedUranium), new ressourceStack(5, ressources.Gold), new ressourceStack(15, ressources.Iron)},
				outcome = new List<ressourceStack>() {
					new ressourceStack(1f, ressources.Warhead),
				},
				energyCost = 10f
			}
		};
		return list;
	}
	
	
	
}
