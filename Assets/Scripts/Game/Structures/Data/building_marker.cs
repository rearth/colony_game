﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Content;
using Game.Structures.Placement;
using Game.Systems;
using Game.Units.Logic;
using UnityEngine;

public class building_marker : MonoBehaviour, SaveLoad.SerializableInfo, clickable {
    public GameObject buildTo = null;
    internal Structure Info = null;
    public ressourceStack[] overrideCost = null;
    private bool inited = false;
    private List<ressourceStack> missing = new List<ressourceStack>();
    private List<ressourceStack> ordered = new List<ressourceStack>();
    long count = 0;
    private inventory own;
    public constructionMaterialHelper materialHelper;
    private float initialRequiredAmount;
    private Dictionary<IWorker, ressourceStack> orders = new Dictionary<IWorker, ressourceStack>();


    // Use this for initialization
    private void Start() {
        own = this.gameObject.GetComponent<inventory>();
    }

    void Init() {
        Debug.Log("initing building marker that will be built to: " + buildTo);

        if (Info == null)
            Info = buildTo.GetComponent<Structure>();
        inited = true;

        if (overrideCost != null) {
            print("using override cost!");
            foreach (ressourceStack stack in overrideCost) {
                missing.Add(stack.clone());
            }
        }
        else {
            foreach (ressourceStack stack in Info.getCost()) {
                missing.Add(stack);
            }
        }

        initialRequiredAmount = missing.Sum(stack => stack.getAmount());
        var particlePos = this.GetComponent<Collider>().bounds.center;
        this.transform.GetChild(0).transform.position = particlePos;
        own.maxSize = initialRequiredAmount;
        
        GameObject.Find("Terrain").GetComponent<DeliveryTargetManager>().SetWantData(this.own, missing, 1);
        GameObject.Find("Terrain").GetComponent<DeliveryTargetManager>().triggerRefresh();

        //orderIdle();
    }

    void FixedUpdate() {

        count++;
        if (buildTo == null) {
            return;
        }

        if (!inited) {
            Init();
        }

        if (count % 60 == 0) {
            //orderIdle();
        }

        if (count % 30 == 0 && checkDone()) {
            hasAll();
        }

        checkProgress();
    }

    private bool checkDone() {
        var iterateList = Info.getCost();
        if (overrideCost != null) {
            iterateList = overrideCost;
        }

        return iterateList.All(elem => own.canTake(elem)) && materialHelper.curProgress > 0.94f;
    }

    private void hasAll() {
        Debug.Log("got all required ressources, building structure now!");

        var done = GameObject.Instantiate(buildTo, this.transform.position, this.transform.rotation);
        GameObject.Destroy(this.gameObject);
    }

    private void checkProgress() {
        var invFill = own.getAmount();
        materialHelper.progress = invFill / initialRequiredAmount;
    }


    private void reloadMissing() {
        missing.Clear();
        var iterateList = Info.getCost();
        if (overrideCost != null) {
            iterateList = overrideCost;
        }

        foreach (ressourceStack stack in iterateList) {
            inventory inv = this.GetComponent<inventory>();
            ressourceStack stillMissing = stack.clone();
            stillMissing.addAmount(-inv.getAmount(stack.getRessource()));
            missing.Add(stillMissing);
        }
    }

    private void reloadOrdered() {
        reloadMissing();
        ordered.Clear();

        foreach (var order in orders) {
            var controller = order.Key;
            var target = controller.getCurTarget();
            if (target != null && target is TransformTarget transformTarget &&
                transform == this.transform && transformTarget.getTarget() == this.transform && orders.ContainsKey(controller)) {
                ordered.Add(orders[controller]);
            }
        }

        foreach (var order in ordered) {
            foreach (var miss in missing) {
                if (miss.getRessource().Equals(order.getRessource())) {
                    miss.addAmount(-order.getAmount());
                }
            }
        }
    }

    private void orderIdle() {
        //Debug.Log("finding idle unit to deliver things");

        //reloadOrdered();
        ordered.Clear();
        
        removeEmpty(missing);
        Debug.Log("searching idles, already ordered: " + printList(ordered) + " missing: " + printList(missing));
        if (missing.Count <= 0) {
            return;
        }

        var closestDropbase = RessourceHelper.getClosestDropbase(this.transform.position);
        
        foreach (ressourceStack stack in missing) {
            if (stack.getAmount() <= 0) {
                continue;
            }

            if (closestDropbase.GetComponent<inventory>().getAmount(stack.getRessource()) > .01f) {
                Transform mover = GetClosestMover(GameObject.FindGameObjectsWithTag("mover"));

                if (mover == null) {
                    return;
                }

                Debug.Log("found valid idle mover");
                ressourceStack take = stack.clone();

                float takeable = closestDropbase.GetComponent<inventory>().getAmount(stack.getRessource());
                if (takeable > mover.gameObject.GetComponent<inventory>().maxSize) {
                    takeable = mover.gameObject.GetComponent<inventory>().maxSize;
                }

                if (stack.getAmount() <= takeable) {
                    stack.setAmount(0);
                    //ordered.Add(stack);
                }
                else {
                    stack.addAmount(-takeable);
                    take.setAmount(takeable);
                }

                Debug.Log("delivering to marker: " + take);
                var worker = mover.GetComponent<IWorker>();
                worker.moveTo(closestDropbase.transform,
                    () => collectFromReached(closestDropbase.transform, worker, take, own),
                    Color.red);
                ordered.Add(take);
                orders[worker] = take;
                //mover.gameObject.GetComponent<IWorker>().deliverTo(closestDropbase, this.gameObject, take);
            }
        }

        foreach (var order in ordered) {
            foreach (var miss in missing) {
                if (miss.getRessource().Equals(order.getRessource())) {
                    miss.addAmount(-order.getAmount());
                }
            }
        }
        
    }

    private void collectFromReached(Transform collectorFrom, IWorker mover, ressourceStack toBring, inventory bringTo) {
        var collectInv = collectorFrom.GetComponent<inventory>();
        var moverInv = mover.getInventory();
        moverInv.transferAllSafe(collectInv);
        collectInv.transferSafeCounted(moverInv, toBring.getRessource(), toBring.getAmount());

        mover.moveTo(bringTo.transform, () => mover.getInventory().transferAllSafe(bringTo),
            Color.yellow);
    }

    private void removeEmpty(List<ressourceStack> list) {
        List<ressourceStack> remove = new List<ressourceStack>();

        foreach (ressourceStack stack in list) {
            if (stack.getAmount() <= 0) {
                remove.Add(stack);
            }
        }

        foreach (ressourceStack stack in remove) {
            list.Remove(stack);
        }
    }

    private Transform GetClosestMover(GameObject[] movers) {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;

        foreach (GameObject potentialTarget in movers) {
            if (!potentialTarget.GetComponent<IWorker>().isIdle()) {
                continue;
            }

            Vector3 directionToTarget = potentialTarget.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr) {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget.transform;
            }
        }

        return bestTarget;
    }

    public void deliveryArrived(ressourceStack stack) {
        Debug.Log("Delivery arrived: " + stack);

        if (checkDone()) {
            hasAll();
            return;
        }

        Debug.Log("Total ressources that arrived: " + this.GetComponent<inventory>() + " ordered: " +
                  printList(ordered) + " missing" + printList(missing));

        orderIdle();
    }

    private string printList(List<ressourceStack> list) {
        string toReturn = "";
        foreach (ressourceStack stack in list) {
            toReturn += stack;
        }

        return toReturn;
    }


    public SaveLoad.SerializationInfo getSerialize() {
        return new serializationData(buildTo, inited, missing, ordered, overrideCost);
    }

    public void handleDeserialization(SaveLoad.SerializationInfo info) {
        print("got deserialization for: " + info.scriptTarget);

        serializationData data = (serializationData) info;
        print("deserilazing building_marker...");

        List<GameObject> prefabs = GameObject.Find("Terrain").GetComponent<SaveLoad>().prefabs;
        GameObject prefabFound = null;
        foreach (GameObject prefab in prefabs) {
            if (prefab.name.Equals(data.buildTo)) {
                prefabFound = prefab;
                break;
            }
        }

        this.buildTo = prefabFound;
        this.inited = data.inited;
        this.Info = buildTo.GetComponent<Structure>();
        this.overrideCost = data.overrideCost;
        this.missing = data.missing;
        this.ordered = data.ordered;
        this.GetComponent<MeshFilter>().sharedMesh = buildTo.GetComponent<MeshFilter>().sharedMesh;
        this.GetComponent<MeshCollider>().sharedMesh = buildTo.GetComponent<MeshFilter>().sharedMesh;
    }

    public void handleClick() {
        if (Salvaging.isActive()) {
            Salvaging.createNotification(this.gameObject);
            return;
        }
    }

    public void handleLongClick() {
        Debug.Log("long clicked structure");

        this.GetComponent<ClickOptions>().Create();
    }

    public void displayInfo() {

        var view = DetailViewManager.getInstance()
            .createView("Building marker, builds to: " + buildTo.name, "Construction Site", "Info", getStats(), UIPrefabCache.InfoBut);
        RadialMenuController.getInstance().clearUI((x) => view.remove());

        foreach (var option in getOptions()) {
            if (option.text.ToLower().StartsWith("info")) continue;
            view.addAction(option.text, option.enabled, option.onClick, option.param, option.enabledCheck, option.restoreUIOnClick);
        }
    }

    private string getStats() {
        var res = "Progress: ";
        return res;
    }

    public PopUpCanvas.popUpOption[] getOptions() {
        PopUpCanvas.popUpOption info = new PopUpCanvas.popUpOption(UIPrefabCache.InfoBut, true, "info", displayInfo);
        return new[] {info};
    }

    [System.Serializable]
    class serializationData : SaveLoad.SerializationInfo {
        public string buildTo;
        public bool inited;
        public List<ressourceStack> missing;
        public List<ressourceStack> ordered;
        public ressourceStack[] overrideCost;

        public serializationData(GameObject buildTo, bool inited, List<ressourceStack> missing,
            List<ressourceStack> ordered, ressourceStack[] overrideCost) {
            string prefabName = buildTo.name;
            prefabName = prefabName.Replace("Clone", "");
            prefabName = prefabName.Replace(" ", "");
            prefabName = prefabName.Replace("(", "");
            prefabName = prefabName.Replace(")", "");
            prefabName = System.Text.RegularExpressions.Regex.Replace(prefabName, @"[\d-]", string.Empty);
            this.buildTo = prefabName;
            this.inited = inited;
            this.missing = missing;
            this.ordered = ordered;
            this.overrideCost = overrideCost;
        }

        public override string scriptTarget {
            get { return "building_marker"; }
        }
    }
}