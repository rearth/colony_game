﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyStorageCluster : DefaultStructure {

    public MeshRenderer statusLight;
    private static readonly int EmissionColor = Shader.PropertyToID("_EmissionColor");

    public override int getMaxEnergy() {
        return 15000;
    }

    public override int getMaxOutput() {
        return 1000;
    }

    public override int getMaxInput() {
        return 1500;
    }

    public override int getPriority() {
        return 8;
    }

    public override void FixedUpdate() {
        base.FixedUpdate();
        var energyFill = (float) this.getCurEnergy() / this.getMaxEnergy();
        updateEmissiveColor(energyFill);
    }

    private void updateEmissiveColor(float status) {
        var hdrIntensity = 2f;
        var mat = statusLight.material;
        var red = Mathf.Clamp((float) (255 - 255 * (status - 0.5) * 2), 0, 255);
        var green = Mathf.Clamp(255 * status * 2, 0f, 255f);
        var color = new Color(red / 255 * hdrIntensity, green / 255 * hdrIntensity, 0f, 1f);
        mat.SetColor(EmissionColor, color);
    }
}