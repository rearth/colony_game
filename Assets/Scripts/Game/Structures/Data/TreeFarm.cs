﻿using System;
using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

public class TreeFarm : ProductionStructure {
    public override List<ProduceData> getProduceData() {
        var list = new List<ProduceData> {
            new ProduceData() {
                consume = new List<ressourceStack>() {},
                outcome = new List<ressourceStack>() {new ressourceStack(5, ressources.Trees)},
                energyCost = 20f
            }
        };
        return list;
    }
}
