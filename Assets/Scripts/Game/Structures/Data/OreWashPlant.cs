﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OreWashPlant : ProductionStructure {
    public override List<ProduceData> getProduceData() {
        var list = new List<ProduceData>();
        list.Add(new ProduceData() {
            consume = new List<ressourceStack>() {
                new ressourceStack(2, ressources.Stone)
            }, outcome = new List<ressourceStack>() {
            new ressourceStack(0.05f, ressources.Uranium), 
            new ressourceStack(0.3f, ressources.OreIron),
            new ressourceStack(0.1f, ressources.OreGold),
            new ressourceStack(0.01f, ressources.OreIridium),
            }, energyCost = 5f
        });
        return list;
    }
}