﻿using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;
using UnityEngine.AI;

public class WarpGateConfigurator : TogglableStructure {
    public float energyCost = 50f;

    public Sprite selectBut;
    public WarpGateConfigurator linkedTo;
    public OffMeshLink ownLink;
    public Transform linkPos;
    public ParticleSystem onActive;

    private int loadedInvID = 0;

    public override PopUpCanvas.popUpOption[] getOptions() {
        PopUpCanvas.popUpOption[] options;

        var info = new PopUpCanvas.popUpOption(infoBut, true, "info", displayInfo);
        var stop = new PopUpCanvas.popUpOption(stopBut, true, "stop", doStop, null, stopEnabled);
        var start = new PopUpCanvas.popUpOption(startBut, true, "start", doStart, null, startEnabled);
        var selectTarget = new PopUpCanvas.popUpOption(selectBut, true, "select target", selectOther);

        options = new[] {info, stop, start, selectTarget};
        return options;
    }

    private bool startEnabled(object o) {
        return getCurEnergy() > minEnergyRequired && !busy && linkedTo != null;
    }

    private bool stopEnabled(object o) {
        return busy;
    }

    protected override void doStart() {
        base.doStart();
        if (linkedTo != null)
            linkedTo.busy = true;
        ownLink.activated = true;
        ownLink.UpdatePositions();
    }

    protected override void doStop() {
        base.doStop();
        if (linkedTo != null)
            linkedTo.busy = false;
        ownLink.activated = false;
        ownLink.UpdatePositions();
    }

    public override void handleClick() {
        Debug.Log("clicked Thing");

        if (salvaging)
            return;

        if (Salvaging.isActive()) {
            Salvaging.createNotification(gameObject);
            return;
        }

        if (busy)
            Notification.createNotification(gameObject, Notification.sprites.Working, "Working...", Color.green,
                true);
        if (!busy && getCurEnergy() >= minEnergyRequired) {
            if (linkedTo != null) {
                doStart();
            }
            else {
                
                Notification.createNotification(gameObject, Notification.sprites.Energy_Low, "Link to a warpgate first",
                    Color.red, false);
            }
        }
        else if (!busy && getCurEnergy() < minEnergyRequired)
            Notification.createNotification(gameObject, Notification.sprites.Energy_Low, "Not enough Energy",
                Color.red, false);
    }

    public override void FixedUpdate() {

        if (loadedInvID != 0) {
            var other = inventory.inventories[loadedInvID].gameObject;
            onTargetSelected(other);
            loadedInvID = 0;
        }
        
        base.FixedUpdate();
        if (isSalvaging()) return;
        if (busy && linkedTo != null) {
            if (this.getCurEnergy() < 10) {
                doStop();
                return;
            }
            var onActiveEmission = onActive.emission;
            onActiveEmission.enabled = true;
            this.addEnergy(-energyCost * Time.deltaTime, this);
            exchangeEnergy(linkedTo);
        }
        else {
            var onActiveEmission = onActive.emission;
            onActiveEmission.enabled = false;
        }
    }

    private void selectOther() {
        Camera.main.gameObject.GetComponent<clickDetector>().setNextClickAction(onTargetSelected);
        RadialMenuController.getInstance().clearUI(cancelSelection);
    }

    private void cancelSelection(RadialMenuController radialMenuController) {
        linkedTo = null;
        Camera.main.gameObject.GetComponent<clickDetector>().resetNextClick();
        ownLink.enabled = false;
        this.busy = false;
    }

    private void onTargetSelected(GameObject obj) {
        var other = obj.GetComponent<WarpGateConfigurator>();
        if (other == null || other == this) {
            Notification.createNotification(obj, Notification.sprites.Stopping, "Must select Warpgate");
            selectOther();
            return;
        }

        Scene_Controller.getInstance().restoreDefaultUI();
        RadialMenuController.getInstance().restoreUI();

        linkedTo = other;
        other.ownLink.enabled = false;

        ownLink.enabled = true;
        ownLink.endTransform = other.linkPos;
        ownLink.startTransform = linkPos;

        other.busy = true;
        other.linkedTo = this;
        doStart();
        ownLink.UpdatePositions();
    }

    private void exchangeEnergy(WarpGateConfigurator other) {
        var ownEnergy = this.getCurEnergy();
        var otherEnergy = other.getCurEnergy();
        var diff = otherEnergy - ownEnergy;
        var exchange = (diff / 10f) * Time.deltaTime;
        other.addEnergy(-exchange, this);
        this.addEnergy(exchange, other);
    }
    
    
    public override SaveLoad.SerializationInfo getSerialize() {
        var otherID = 0;
        if (linkedTo != null) otherID = linkedTo.GetComponent<inventory>().id;
        return new serializationData(storedEnergy, busy, salvaging, ownResource, otherID);
    }

    public override void handleDeserialization(SaveLoad.SerializationInfo info) {
        serializationData data = (serializationData) info;
        this.storedEnergy = data.storedEnergy;
        this.busy = data.busy;
        this.ownResource = data.ownResource;
        this.salvaging = data.salvaging;
        this.loadedInvID = data.linkedToInvID;

        if (salvaging) {
            Debug.Log("got salvaging info, creating particles....");
        }
    }
    
    [System.Serializable]
    public new class serializationData : SaveLoad.SerializationInfo {
        public float storedEnergy;
        public bool busy;
        public bool salvaging;
        public ressourceStack[] ownResource;
        public int linkedToInvID;

        public serializationData(float storedEnergy, bool busy, bool salvaging, ressourceStack[] ownResource, int linkedToInvId) {
            this.storedEnergy = storedEnergy;
            this.busy = busy;
            this.salvaging = salvaging;
            this.ownResource = ownResource;
            linkedToInvID = linkedToInvId;
        }

        public override string scriptTarget {
            get { return "WarpGateConfigurator"; }
        }
    }
}