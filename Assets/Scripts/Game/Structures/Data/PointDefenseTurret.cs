﻿using System.Collections;
using System.Collections.Generic;
using Content.Helpers;
using Game.Combat;
using UnityEngine;

public class PointDefenseTurret : RotatingTurret {
    
    public ParticleSystem emitter;
    public ParticleSystem shotEmitter;
    public Transform barrel;
    public List<Transform> emitPos;
    
    private Vector3 barrelInitialLocalPosition;


    public override void Start() {
        base.Start();
        barrelInitialLocalPosition = barrel.localPosition;
    }

    public override string getName() {
        return "PDD";
    }

    public override void shoot(GameObject target, Vector3 impactPos) {
        emitter.Play();
        barrel.localPosition = barrelInitialLocalPosition + new Vector3(0, 0, Random.Range(-0.05f, 0.05f));

        foreach (var elem in emitPos) {
            var emitParams = new ParticleSystem.EmitParams {
                position = elem.position, 
                applyShapeToPosition = true
            };
            shotEmitter.Emit(emitParams, 1);
        }
    }

    public void onBulletCollision(Collider colliderHit) {
        var otherHP = colliderHit.GetComponent<HPHandler>();
        if (otherHP != null && otherHP.faction != this.getHP().faction) {
            otherHP.inflictDamage(Damage, null);
        }
    }

    public override bool useRandom() {
        return false;
    }
}