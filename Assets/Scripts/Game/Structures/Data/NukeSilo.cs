﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NukeSilo : DefaultStructure {

	public Sprite launchBut;
	public GameObject missilePrefab;
	public LayerMask checkLayersForClicks;
	
	private Vector3 target;

	public override int getMaxEnergy() {
		return 1000;
	}

	public override int getMaxInput() {
		return 50;
	}

	public override int getMaxOutput() {
		return 0;
	}

	public override PopUpCanvas.popUpOption[] getOptions() {
		PopUpCanvas.popUpOption info = new PopUpCanvas.popUpOption(infoBut, true, "info", displayInfo);
		PopUpCanvas.popUpOption launch = new PopUpCanvas.popUpOption(launchBut, true, "launch nuke", selectPoint, null, launchEnabled, false);

		var options = new[] {info, launch};
		return options;
	}

	private bool launchEnabled(object o) {
		return this.getCurEnergy() >= 500 && this.getInv().getAmount(ressources.Warhead) >= 1;
	}

	public override int getPriority() {
		return 3;
	}

	private void selectPoint() {
		Debug.Log("starting point selection");
		
		RadialMenuController.getInstance().clearUI(cancel);
			
		GameObject.Find("Main Camera").GetComponent<clickDetector>().setNextRayClickAction(handleRay);
		TimeScaleHandler.setScale(0.1f);
	}
		
	private void handleRay(Ray ray) {
		print("handling ray for nuke creation: " + ray);
		RaycastHit raycastHit;

		if (!Physics.Raycast(ray, out raycastHit, 200.0f, checkLayersForClicks)) {
			print("raycast returned invalid!");
			return;
		}

		var hitPos = raycastHit.point;
		selectTarget(hitPos);
	}

	private void selectTarget(Vector3 target) {
		print("selected target: " + target);
		restoreNormal();
		Notification.createNotification(target, Notification.sprites.Targeting);
		this.target = target;
		this.GetComponent<Animator>().SetTrigger("work");
		this.getInv().remove(new ressourceStack(1, ressources.Warhead));
		this.addEnergy(-550, this);
	}

	private void launchMissile() {
		print("launching missile");

		var missile = GameObject.Instantiate(missilePrefab, this.transform.position + new Vector3(0, 0, 0),
			Quaternion.identity);
		missile.GetComponent<ParabolaMissile>().setTarget(target);
	}

	private void restoreNormal() {
		clickDetector.overlayClicked = true;
		GameObject.Find("Main Camera").GetComponent<clickDetector>().resetNextClick();
		RadialMenuController.getInstance().restoreUI();
		
		TimeScaleHandler.normalizeScale();
	}

	public void cancel(RadialMenuController radialMenuController) {
		restoreNormal();
	}

	public override void FixedUpdate() {
		base.FixedUpdate();
		if (counter % 600 == 1 &&  this.getInv().getAmount(ressources.Warhead) < 1) {
			RessourceHelper.deliverFromAnywhere(this.gameObject, false, ressources.Warhead);
		}
	}
}
