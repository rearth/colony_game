﻿using System;
using System.Collections.Generic;
using Content;
using Game.Structures.Placement;
using UnityEngine;

public class ressourceStorage : MonoBehaviour, clickable, Structure {

    public Sprite infoBut;
    public Sprite moveToBut;

    public void displayInfo() {
        var view = DetailViewManager.getInstance()
            .createView(getDesc(), gameObject.name, "Info", getHP().niceText(), UIPrefabCache.InfoBut);
        RadialMenuController.getInstance().clearUI(x => view.remove());

        foreach (var option in getOptions()) {
            if (option.text.ToLower().StartsWith("info")) continue;
            view.addAction(option.text, option.enabled, option.onClick, option.param, option.enabledCheck);
        }
    }

    private string getDesc() {
        return "A Structure that can be used as ressource gathering point. Clicking it will force all nearby units to deliver everything they're carrying"
            +  Environment.NewLine
            + GetComponent<inventory>();

    }

    public PopUpCanvas.popUpOption[] getOptions() {
        PopUpCanvas.popUpOption info = new  PopUpCanvas.popUpOption(infoBut, true, "info", displayInfo);
         PopUpCanvas.popUpOption goTo = new  PopUpCanvas.popUpOption(moveToBut, true, "recall workers", deliverIdles);
        
        return new[]{info, goTo};
    }

    public void handleClick() {
        Debug.Log("clicked ressource");
        deliverIdles();
    }

    public void handleLongClick() {
        Debug.Log("long clicked ressource");
        GetComponent<ClickOptions>().Create();
    }

    private void deliverIdles() {
        Debug.Log("setting units to deliver now");

        GameObject[] carriers = GameObject.FindGameObjectsWithTag("mover");

        foreach (GameObject carrier in carriers) {
            if (carrier.GetComponent<inventory>().getAmount() > 0) {
                carrier.GetComponent<movementController>().setTarget(transform);
                carrier.GetComponent<ActionController>().setState(ActionController.State.Walking);
            }
        }

    }

    public bool isWorking() {
        return true;
    }

    public GameObject getGameobject() {
        return gameObject;
    }

    public ressourceStack[] getResources() {
        return new[] { new ressourceStack(1000, ressources.Stone) };
    }

    public HPHandler getHP() {
        return GetComponent<HPHandler>();
    }

    public inventory getInv() {
        return GetComponent<inventory>();
    }

    public bool isSalvaging() {
        return false;
    }

    public void salvage() {
    }

    public virtual ressourceStack[] getCost() {
        return findDefinedPrice().ToArray();
    }

    private List<ressourceStack> findDefinedPrice() {
        var prefabs = GameObject.Find("Terrain").GetComponent<BuildingManager>().autoPrefabs;
        var res = new List<ressourceStack>();

        foreach (var elem in prefabs) {
            if (gameObject.name.Equals(elem.prefab.name)) {
                foreach (var stack in elem.cost) {
                    res.Add(stack.clone());
                }
            }
        }

        return res;
    }
}
