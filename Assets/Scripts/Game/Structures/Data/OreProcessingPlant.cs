﻿using System.Collections.Generic;
using UnityEngine;

namespace Content.Structures {
    public class OreProcessingPlant : ProductionStructure {
        public override List<ProduceData> getProduceData() {
            var list = new List<ProduceData> {
                new ProduceData() {
                    consume = new List<ressourceStack>() {new ressourceStack(2, ressources.OreGold)},
                    outcome = new List<ressourceStack>() {new ressourceStack(1, ressources.Gold)},
                    energyCost = 10f
                },
                new ProduceData() {
                    consume = new List<ressourceStack>() {new ressourceStack(2, ressources.OreIron)},
                    outcome = new List<ressourceStack>() {new ressourceStack(1, ressources.Iron)},
                    energyCost = 10f
                }
            };
            return list;
        }
    }
}
