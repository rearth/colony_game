﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deepRessource : harvestableRessource {

    public Sprite drillBut;
    public GameObject markerPrefab;
    public GameObject drillPrefab;
    public GameObject activeDrill = null;
    override public PopUpCanvas.popUpOption[] getOptions() {
        PopUpCanvas.popUpOption[] options;

        PopUpCanvas.popUpOption info = new PopUpCanvas.popUpOption(infoBut, true, "info", displayInfo);
        PopUpCanvas.popUpOption goTo = new PopUpCanvas.popUpOption( moveToBut, true, "send worker", moveHere);
        PopUpCanvas.popUpOption makeDrill = new PopUpCanvas.popUpOption(drillBut, true, "build drill", createDrill);

        for (int i = 0; i < DeepDrillPlatform.getPrice().Length; i++) {
            var price = DeepDrillPlatform.getPrice();
            if (ResourceHandler.getAmoumt(price[i].getRessource()) < price[i].getAmount()) {
                //ressources missing
                makeDrill.enabled = false;
            }
        }

        if (activeDrill != null) {
            makeDrill.enabled = false;
        }

        options = new PopUpCanvas.popUpOption[] { info, goTo, makeDrill };
        return options;
    }

    private void createDrill() {
        print("creating drill on mineral patches!");
        var elem = markerPrefab;
        var beacon = GameObject.Instantiate(elem, this.transform.position, Quaternion.identity);

        beacon.GetComponent<MeshCollider>().sharedMesh = this.GetComponent<MeshFilter>().sharedMesh;
        beacon.GetComponent<MeshFilter>().sharedMesh = this.GetComponent<MeshFilter>().sharedMesh;
        //beacon.transform.localScale = new Vector3(3f, 3f, 3f);

        beacon.GetComponent<building_marker>().buildTo = drillPrefab;
        beacon.GetComponent<building_marker>().overrideCost = null;
        //beacon.transform.Rotate(new Vector3(-90f, 0 , 0), Space.Self);
        var pos = beacon.transform.position;
        pos.y += 1f;
        beacon.transform.position = pos;
    }

}
