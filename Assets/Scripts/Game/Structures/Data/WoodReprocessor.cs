﻿using System;
using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

public class WoodReprocessor : ProductionStructure {
    
    public override List<ProduceData> getProduceData() {
        var list = new List<ProduceData> {
            new ProduceData() {
                consume = new List<ressourceStack>() {new ressourceStack(10, ressources.Trees)},
                outcome = new List<ressourceStack>() {new ressourceStack(5, ressources.Wood)},
                energyCost = 10f
            }
        };
        return list;
    }
    
}
