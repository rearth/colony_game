﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waterpump : TogglableStructure {
    private float animDuration = 0f;
    private Vector3 animEndPoint;
    public TrailRenderer trail;
    public int waterLayer;

    private static readonly float animMaxDuration = 3f;

    public override ressourceStack[] getCost() {
        return getPrice();
    }

    public override void Start() {
        base.Start();
        var endPos = findWaterPos();
        animEndPoint = endPos;
    }

    public void Update() {
        if (animDuration >= animMaxDuration) return;
        animDuration += Time.deltaTime;
        var progress = animDuration / animMaxDuration;
        var position = trail.transform.position;

        var addHeight = 2.3f * progress - 2.58 * Mathf.Pow(progress, 2);
        addHeight *= 5f;

        var pos = position + (animEndPoint - position) * progress;
        pos.y += (float) addHeight;
        trail.transform.position = pos;
    }

    private Vector3 findWaterPos() {
        var origin = this.transform.position;
        var down = Vector3.down;
        for (int j = 10; j < 50; j += 10) {
            for (int i = 0; i < 360; i += 45) {
                var dir = Quaternion.AngleAxis(i, Vector3.up) * Vector3.forward;
                var targetPoint = origin + dir * j;
                var ray = new Ray(targetPoint - down * 10, down);
                Debug.DrawRay(targetPoint - down * 10, down, Color.red, 10f);

                RaycastHit result;
                var isHit = Physics.Raycast(ray, out result, 20, Physics.AllLayers);

                if (isHit && result.collider != null && waterLayer == result.collider.gameObject.layer) {
                    print("found ray for water position");
                    return result.point;
                }
            }
        }

        var parts = GameObject.FindGameObjectsWithTag("water");
        foreach (var water in parts) {
            //check if distance between colliders is low enough

            var waterCollider = water.gameObject.GetComponent<Collider>();

            var sampleTar = waterCollider.ClosestPoint(this.transform.position);

            //check if close enough
            if (Vector3.Distance(this.transform.position, sampleTar) < 12) {
                //Debug.DrawLine(holoPlacement.gameObject.transform.position, sampleTar);

                var center = waterCollider.bounds.center;
                var usePoint = sampleTar + (center - sampleTar) * 0.2f;
                usePoint.y = sampleTar.y - 0.1f;
                return usePoint;
            }
        }

        return Vector3.zero;
    }

    public static ressourceStack[] getPrice() {
        ressourceStack[] cost = new ressourceStack[3];

        cost[0] = new ressourceStack(50, ressources.Wood);
        cost[1] = new ressourceStack(20, ressources.Stone);
        cost[2] = new ressourceStack(20, ressources.Iron);
        return cost;
    }

    public override string getDesc() {
        return "The Water pump pumps out water from a nearby lake. Requires energy to work"
               + base.getDesc();
    }

    public override int getMaxEnergy() {
        return 1000;
    }

    public override int getMaxInput() {
        if (this.getMaxEnergy() - this.getCurEnergy() < 50) {
            return this.getMaxEnergy() - this.getCurEnergy();
        }

        return 50;
    }

    public override int getMaxOutput() {
        return 0;
    }

    public static bool isNearWater(GameObject holoPlacement, bool terrainCheck) {
        if (!terrainCheck) {
            return false;
        }

        var parts = GameObject.FindGameObjectsWithTag("water");
        foreach (var water in parts) {
            //check if distance between colliders is low enough
            //TODO make animation of pipe expand towards water

            var waterCollider = water.gameObject.GetComponent<Collider>();

            var sampleTar = waterCollider.ClosestPoint(holoPlacement.gameObject.transform.position);

            //check if close enough
            if (Vector3.Distance(holoPlacement.gameObject.transform.position, sampleTar) < 10) {
                //Debug.DrawLine(holoPlacement.gameObject.transform.position, sampleTar);
                return true;
            }
        }

        return false;
    }

    new void FixedUpdate() {
        base.FixedUpdate();

        //stop updating if not working
        if (this.getCurEnergy() <= 5 || !this.busy) return;

        //working, get water!
        this.addEnergy(-5f * Time.deltaTime, this);
        this.getInv().add(ressources.Water, 10 * Time.deltaTime);
    }
}