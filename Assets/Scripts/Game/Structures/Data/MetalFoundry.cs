﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalFoundry : ProductionStructure {
    
    //coal + iron -> steel
    //iron + gold -> electrum
    public override List<ProduceData> getProduceData() {
        var list = new List<ProduceData> {
            new ProduceData() {
                consume = new List<ressourceStack>() {new ressourceStack(0.1f, ressources.Coal), new ressourceStack(1, ressources.Iron)},
                outcome = new List<ressourceStack>() {new ressourceStack(1, ressources.Steel)},
                energyCost = 10f
            },
            new ProduceData() {
                consume = new List<ressourceStack>() {new ressourceStack(1, ressources.Iron), new ressourceStack(1, ressources.Gold)},
                outcome = new List<ressourceStack>() {new ressourceStack(1, ressources.Electrum)},
                energyCost = 20f
            }
        };
        return list;
    }
}