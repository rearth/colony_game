﻿using System;
using System.Collections;
using System.Collections.Generic;
using Content;
using Content.Helpers;
using DigitalRuby.LightningBolt;
using UnityEngine;

public class TeslaTower : SimpleTower {
    public GameObject whipPrefab;
    public GameObject startPos;
    public GameObject impactPrefab;

    private bool ready = false;
    private bool morphing = false;
    private static readonly int Activate = Animator.StringToHash("activate");
    private static readonly int Deactivate = Animator.StringToHash("deactivate");
    private int idleReadyTimer = 0;

    public override void FixedUpdate() {
        
        
        if (this.salvaging && this.getHP().HP < 3) {
            print("structure salvaged!");
            var pickup = Instantiate(GameObject.Find("Terrain").GetComponent<Scene_Controller>().pickupBox,
                this.transform.position, Quaternion.identity);
            pickup.GetComponent<inventory>().add(new ressourceStack(salvageStartHP, ressources.Scrap));
            getHP().destruct();
        }

        if (salvaging) {
            this.getHP().HP -= 2.5f;
            return;
        }

        if (morphing || (!ready && !base.active)) {
            return;
        }

        if (ready && !base.active) {
            morphing = true;
            print("deactivating tesla tower");
            Invoke("deactivate", 5f);
            this.GetComponent<Animator>().SetTrigger(Deactivate);
            setParticleState(false);
            ready = false;
            return;
        }

        if ((counter++ % 50 == 0 || ready && counter % 6 == 0)) {
            enemy = findEnemy(useRandom());
            if (enemy != null) {
                enemyCollider = enemy.GetComponent<Collider>();
            }
        }

        if (!ready && enemyIsClose()) {
            morphing = true;
            Invoke("activate", 5f);
            print("activating tesla tower...");
            this.GetComponent<Animator>().SetTrigger(Activate);
            timeElapsed += 1 / attacksPerSecond;
        } else if (ready && !enemyIsClose() && idleReadyTimer++ > 60) {
            morphing = true;
            idleReadyTimer = 0;
            print("deactivating tesla tower");
            Invoke("deactivate", 5f);
            this.GetComponent<Animator>().SetTrigger(Deactivate);
            setParticleState(false);
            ready = false;
        }

        if (!ready || enemy == null) {
            return;
        }

        idleReadyTimer = 0;
        
        timeElapsed += Time.deltaTime;
        if (timeElapsed >= 1 / attacksPerSecond) {
            var impactPos = getImpactPos();
            timeElapsed -= 1 / attacksPerSecond;
            shoot(enemy, impactPos);
        }
    }

    public override ressourceStack[] getCost() {
        
        ressourceStack[] cost = new ressourceStack[2];

        cost[0] = new ressourceStack(100, ressources.Wood);
        cost[1] = new ressourceStack(100, ressources.Stone);
        return cost;
    }

    private void activate() {
        this.ready = true;
        setParticleState(true);
        morphing = false;
    }
    private void deactivate() {
        morphing = false;
        startPos.SetActive(false);
    }

    private void setParticleState(bool state) {
        if (state) {
            startPos.GetComponent<ParticleSystem>().Play();
        }
        else {
            startPos.GetComponent<ParticleSystem>().Stop();
        }
    }
    
    private bool enemyIsClose() {
        return enemy != null;
    }

    public override void shoot(GameObject target, Vector3 impactPos) {
        var position = startPos.transform.position;
        var bullet = GameObject.Instantiate(whipPrefab, position,
            Quaternion.LookRotation(impactPos - position));
        var controller = bullet.GetComponent<LightningBoltScript>();
        controller.StartPosition = startPos.transform.position;
        controller.EndPosition = impactPos;

        if (target != null) {
            target.GetComponent<HPHandler>().HP -= Damage;
        }


        GameObject.Instantiate(impactPrefab, impactPos, Quaternion.AngleAxis(-90f, new Vector3(1, 0, 0)));
    }

    public override bool useRandom() {
        return true;
    }

    public override string getName() {
        return "Electric overcharge";
    }
}