﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Solar_Panel : DefaultStructure {
    
    public bool generating = true;
    public float generateAmount = 20;   //per second

    public static ressourceStack[] getPrice() {
        ressourceStack[] cost = new ressourceStack[2];

        cost[0] = new ressourceStack(20, ressources.Wood);
        cost[1] = new ressourceStack(200, ressources.Stone);

        return cost;
    }

    public override ressourceStack[] getCost() {
        return Solar_Panel.getPrice();
    }

    public override string getDesc() {
        return "A basic Solar Panel. Generates 20 Energy per second"
            +  Environment.NewLine
            + "Kind: " + this.GetComponent<HPHandler>().niceText();

    }

    public override int getMaxEnergy() {
        return 500;
    }

    public override int getMaxInput() {
        return 50;
    }

    public override int getMaxOutput() {
        return 50;
    }

    public override int getPriority() {
        return 10;
    }

    public new void FixedUpdate() {
        base.FixedUpdate();

        if (generating && this.getCurEnergy() < this.getMaxEnergy()) {
            this.storedEnergy += Time.deltaTime * generateAmount * SunLightRotation.getIntensity();
            if (this.storedEnergy > this.getMaxEnergy()) {
                this.storedEnergy = this.getMaxEnergy();
            }
        }
    }

    public override PopUpCanvas.popUpOption[] getOptions() {
        PopUpCanvas.popUpOption info = new  PopUpCanvas.popUpOption(infoBut, true, "info", displayInfo);
        var options = new[]{info};
        return options;
    }
}
