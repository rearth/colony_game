﻿using System;
using System.Collections;
using System.Collections.Generic;
using Content.Helpers;
using Game.Combat;
using UnityEngine;

public class BombardTurret : RotatingTurret {
    public GameObject projectilePrefab;
    public Transform shootPosition;
    public ParticleSystem playOnShoot;

    public override void shoot(GameObject target, Vector3 impactPos) {
        var bullet = GameObject.Instantiate(projectilePrefab, shootPosition.position,
            Quaternion.LookRotation(Vector3.up));

        bullet.GetComponent<UnguidedProjectile>().init(target, impactPos, Damage, 4f);
        bullet.GetComponent<HPHandler>().faction = this.GetComponent<HPHandler>().faction;
        playOnShoot.Play();
    }

    public override bool useRandom() {
        return true;
    }

    public override string getName() {
        return "Unguided Rockets";
    }

    protected override Vector3 getImpactPos() {
        var position = enemy.transform.position;
        var toTar = position - this.transform.position;
        var upperPos = this.transform.position + toTar * 0.66f + Vector3.up * toTar.magnitude * 0.33f;
        return enemyCollider.ClosestPoint(upperPos);
    }
}