﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarpGateAnimStarter : MonoBehaviour {

    public ParticleSystem system;
    private ParticleSystem.EmissionModule emissionModule;

    private void Start() {
        emissionModule = system.emission;
    }

    private void OnTriggerEnter(Collider other) {
        
        system.Play();
    }
}