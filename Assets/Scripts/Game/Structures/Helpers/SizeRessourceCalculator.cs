﻿using System;
using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

[RequireComponent(typeof(inventory), typeof(harvestableRessource), typeof(Collider))]
public class SizeRessourceCalculator : MonoBehaviour {
	private void OnEnable() {

		var HP = this.GetComponent<HPHandler>();
		if (Math.Abs(HP.HP - HP.getInitialHP()) < 0.1f) {
			return;
		}
		
		Collider col = this.GetComponent<Collider>();
		var size = col.bounds.size;
		var vol = size.x * size.y * size.z / 2f;
		vol = (int) vol;
		if (vol < 20) {
			vol = 20;
		}
		//print("found size on ressource: " + vol);
		HP.HP = vol;
		HP.overrideInitialHP(vol);
	}
}
