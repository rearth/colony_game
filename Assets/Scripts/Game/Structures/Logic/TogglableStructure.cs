﻿using UnityEngine;

public abstract class TogglableStructure : DefaultStructure {

    public Sprite stopBut;
    public Sprite startBut;
    public float minEnergyRequired = 100f;

    public override int getMaxEnergy() {
        return 1000;
    }

    public override int getMaxInput() {
        return 150;
    }

    public override int getMaxOutput() {
        return 0;
    }

    public override PopUpCanvas.popUpOption[] getOptions() {

        var info = new PopUpCanvas.popUpOption(infoBut, true, "info", displayInfo);
        var stop = new PopUpCanvas.popUpOption(stopBut, true, "stop", doStop, null, stopEnabled);
        var start = new PopUpCanvas.popUpOption(startBut, true, "start", doStart, null, startEnabled);

        return new[] {info, stop, start};
    }

    private bool startEnabled(object o) {
        return getCurEnergy() >= minEnergyRequired && !busy;
    }

    private bool stopEnabled(object o) {
        return busy;
    }

    public override int getPriority() {
        return 4;
    }

    public override void handleClick() {
        Debug.Log("clicked Thing");

        if (salvaging)
            return;

        if (Salvaging.isActive()) {
            Salvaging.createNotification(gameObject);
            return;
        }

        if (busy)
            Notification.createNotification(gameObject, Notification.sprites.Working, "Working...", Color.green,
                true);
        if (!busy && getCurEnergy() >= minEnergyRequired) {
            doStart();
        }
        else if (!busy && getCurEnergy() < minEnergyRequired)
            Notification.createNotification(gameObject, Notification.sprites.Energy_Low, "Not enough Energy",
                Color.red, false);
    }

    protected virtual void doStop() {
        //this.GetComponent<Animator>().SetBool("working", false);
        Notification.createNotification(gameObject, Notification.sprites.Stopping, "Stopped", Color.red);
        busy = false;
    }

    protected virtual void doStart() {
        //this.GetComponent<Animator>().SetBool("working", true);
        Notification.createNotification(gameObject, Notification.sprites.Working, "Starting", Color.green, true);
        busy = true;
        //DeliveryRoutes.addRoute(this.gameObject, DeliveryRoutes.getClosest("dropBase", this.gameObject).gameObject, HPHandler.ressources.Trees);
    }
}