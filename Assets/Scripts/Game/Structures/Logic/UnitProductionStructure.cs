﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Content;
using Content.Helpers.Combat;
using UnityEngine;

[RequireComponent(typeof(inventory))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(HPHandler))]
[RequireComponent(typeof(NavMeshSourceTag))]
[RequireComponent(typeof(ClickOptions))]
public class UnitProductionStructure : DefaultStructure {
    private Animator animator;

    //used animations: work
    public bool hasAnimation;
    public bool startFirstOnClick;
    private static readonly int Work = Animator.StringToHash("work");

    public List<unitProductionData> data;

    private unitProductionData currentlyProducing = null;
    private float progress = 0f;
    private int requestCD = 0;

    public override void Start() {
        base.Start();
        if (hasAnimation) {
            animator = this.GetComponent<Animator>();
            if (animator == null) {
                hasAnimation = false;
            }
        }
    }

    public override int getMaxEnergy() {
        return 2000;
    }

    public override int getMaxOutput() {
        return 0;
    }

    public override int getMaxInput() {
        return 250;
    }

    public override int getPriority() {
        return 3;
    }

    public override PopUpCanvas.popUpOption[] getOptions() {
        var optionsList = new List<PopUpCanvas.popUpOption>();
        var info = new PopUpCanvas.popUpOption(infoBut, true, "info", displayInfo);
        optionsList.Add(info);

        optionsList.AddRange(data.Select(elem => new PopUpCanvas.popUpOption(elem.icon, true, elem.name,
            () => unitCreateSelected(elem), null, (x) => canBuild(elem))));

        return optionsList.ToArray();
    }

    private bool canBuild(unitProductionData elem) {
        return this.getCurEnergy() >= elem.energyCost && !this.busy;
    }

    private void unitCreateSelected(unitProductionData chosen) {
        if (this.getCurEnergy() < chosen.energyCost) {
            Notification.createNotification(this.gameObject, Notification.sprites.Energy_Low, "missing energy");
            return;
        }

        this.addEnergy(-chosen.energyCost, this);
        this.getInv().remove(chosen.cost);

        this.busy = true;
        if (hasAnimation)
            this.animator.SetBool(Work, true);
        this.currentlyProducing = chosen;
        Notification.createNotification(this.gameObject, Notification.sprites.Starting, "");
    }

    public override void FixedUpdate() {
        base.FixedUpdate();
        if (this.busy) {
            if (this.getInv().canTake(currentlyProducing.cost)) {
                this.progress += Time.deltaTime;
                if (progress > currentlyProducing.productionTime) {
                    //done producing
                    busy = false;
                    if (hasAnimation)
                        animator.SetBool(Work, false);
                    var created = GameObject.Instantiate(this.currentlyProducing.prefab,
                        this.transform.position + currentlyProducing.prefab.transform.position,
                        Quaternion.identity);

                    try {
                        created.GetComponent<CombatUnit>().moveRand();
                    }
                    catch (NullReferenceException ex) {
                    }

                    try {
                        created.GetComponent<movementController>().moveRand();
                    }
                    catch (NullReferenceException ex) {
                    }

                    Notification.createNotification(this.gameObject, Notification.sprites.Starting, "", Color.green,
                        true);
                    currentlyProducing = null;
                    progress = 0f;
                }
            }
            else {
                //request additional resources
                if (requestCD-- <= 0) {
                    RessourceHelper.deliverTo(this.gameObject, false, currentlyProducing.cost);
                    requestCD = 300;
                }
            }
        }
    }

    public override void handleClick() {
        base.handleClick();
        
        if (salvaging || Salvaging.isActive())
            return;
        
        if (busy) {
            Notification.createNotification(this.gameObject, Notification.sprites.Working, "", Color.blue, true);
            return;
        }

        if (startFirstOnClick) {
            var chosen = data[0];
            if (this.getCurEnergy() < chosen.energyCost) {
                Notification.createNotification(this.gameObject, Notification.sprites.Energy_Low, "missing energy");
                return;
            }

            this.addEnergy(-chosen.energyCost, this);
            this.getInv().remove(chosen.cost);

            this.busy = true;
            if (hasAnimation)
                this.animator.SetBool(Work, true);
            this.currentlyProducing = chosen;
            Notification.createNotification(this.gameObject, Notification.sprites.Starting, "");
        }
    }

    public override SaveLoad.SerializationInfo getSerialize() {
        return new serializationData(storedEnergy, busy, salvaging, ownResource, currentlyProducing, progress);
    }

    public override void handleDeserialization(SaveLoad.SerializationInfo info) {
        serializationData sData = (serializationData) info;
        base.handleDeserialization(info);

        this.progress = sData.progress;
        foreach (var elem in this.data) {
            if (elem.name.Equals(sData.dataName)) {
                this.currentlyProducing = elem;
                break;
            }
        }
    }

    [Serializable]
    public class unitProductionData {
        [SerializeField] internal Sprite icon;
        [SerializeField] internal GameObject prefab;
        [SerializeField] internal float energyCost;
        [SerializeField] internal float productionTime;
        [SerializeField] internal List<ressourceStack> cost;
        [SerializeField] internal String name;
    }

    [System.Serializable]
    new class serializationData : DefaultStructure.serializationData {
        internal String dataName;
        internal float progress;

        public serializationData(float storedEnergy, bool busy, bool salvaging, ressourceStack[] ownResource,
            unitProductionData data, float progress) : base(storedEnergy, busy, salvaging, ownResource) {
            if (data != null)
                this.dataName = data.name;
            this.progress = progress;
        }

        public override string scriptTarget {
            get { return "UnitProductionStructure"; }
        }
    }
}