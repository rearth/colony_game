﻿using System.Collections;
using System.Collections.Generic;
using Content;
using Game.Structures.Placement;
using Game.Systems;
using UnityEngine;

[RequireComponent(typeof(inventory))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(HPHandler))]
[RequireComponent(typeof(NavMeshSourceTag))]
[RequireComponent(typeof(ClickOptions))]
public abstract class ProductionStructure : TogglableStructure {
	private inventory inventory;
	private Animator animator;
	//used animations: work
	public bool hasAnimation;
	public bool useMovers = false;
	public float fillTarget = 0.6f;
	public ParticleSystem emitOnWork;
	public Transform rotateOnWork;
	public Vector3 rotationAngles;
	
	private static readonly int Work = Animator.StringToHash("work");
	private ParticleSystem.EmissionModule emissionModule;

	private new void Start() {
		base.Start();
		inventory = this.GetComponent<inventory>();
		if (hasAnimation) {
			animator = this.GetComponent<Animator>();
			if (animator == null) {
				hasAnimation = false;
			}
		}

		if (emitOnWork) {
			emissionModule = emitOnWork.emission;
			emissionModule.enabled = false;
		}
		
	}

	protected override void doStart() {
		base.doStart();
		
		if (useMovers) {
			var wantListP = this.getProduceData();
			var wantList = new List<ressourceStack>();
			foreach (var elem in wantListP) {
				wantList.AddRange(elem.consume);
			}
			
			GameObject.Find("Terrain").GetComponent<DeliveryTargetManager>().SetWantData(this.getInv(), wantList, fillTarget);
		}
		
	}

	protected override void doStop() {
		base.doStop();
		emissionModule.enabled = false;
	}

	public class ProduceData {
		//amount are in per second
		public List<ressourceStack> consume;
		public float energyCost = 0;
		public List<ressourceStack> outcome;
		public float energyProduce = 0;
		
	}

	public abstract List<ProduceData> getProduceData();
	
	
	private new void FixedUpdate() {
		base.FixedUpdate();

		if (!busy) return;
		
		//request resources
//		if (useMovers) {
//			foreach (var data in getProduceData()) {
//				foreach (var kind in data.consume) {
//					if (counter % 240 == 0 && (this.inventory.getFillPercent() < 0.5f || this.inventory.getAmount(kind.getRessource()) < kind.getAmount())) {
//						RessourceHelper.deliverTo(this.gameObject, false, kind.getRessource());
//					}
//				}
//			}
//		}
		
		//consume goods
		var dataList = this.getProduceData();
		var workDone = false;
		foreach (var data in dataList) {
			if (handleProduceData(data)) {
				workDone = true;
			}
		}

		if (hasAnimation) {
			animator.SetBool(Work, workDone);
		}

		if (emitOnWork) {
			emissionModule.enabled = workDone;
		}

		if (rotateOnWork && workDone) {
			rotateOnWork.Rotate(rotationAngles * Time.deltaTime, Space.Self);
		}
	}

	public override string getStats(BuildingManager.structureData data) {
		var res = "";
		foreach (var produceData in this.getProduceData()) {
			res += toNiceString(produceData.consume) + getNiceEnergy(produceData.energyCost) + " => " +
			       toNiceString(produceData.outcome) + getNiceEnergy(produceData.energyProduce) + " \n";
		}

		return res;
	}

	private string getNiceEnergy(float amount) {
		if (amount > 0.01) {
			return " Energy: " + amount.ToString("N");
		}

		return "";
	}

	private string toNiceString(List<ressourceStack> list) {
		var res = "";
		for (var i = 0; i < list.Count; i++) {
			var stack = list[i];
			if (i == list.Count - 1) {
				res += stack.getNice();
			}
			else {
				res += stack.getNice() + " + ";
			}
		}

		return res;
	}

	private bool handleProduceData(ProduceData data) {

		if (this.storedEnergy < data.energyCost * Time.deltaTime) return false;
		
		foreach (var elem in data.consume) {
			var scaled = elem.clone();
			scaled.setAmount(scaled.getAmount() * Time.deltaTime);
			if (!inventory.canTake(scaled)) return false;
		}
		
		//has enough energy and ressources!
		this.storedEnergy -= data.energyCost * Time.deltaTime;

		foreach (var elem in data.consume) {
			var scaled = elem;
			scaled.setAmount(scaled.getAmount() * Time.deltaTime);
			inventory.remove(scaled);
		}
		
		//create outcome/energy
		this.storedEnergy += data.energyProduce * Time.deltaTime;
		
		foreach (var elem in data.outcome) {
			var scaled = elem;
			scaled.setAmount(scaled.getAmount() * Time.deltaTime);
			inventory.add(scaled);
		}

		return true;
	}
}
