﻿using Content.Helpers.Combat;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Combat {
    public class NavMeshCombatUnit : CombatUnit {
        internal NavMeshAgent agent;
        private bool enteredLink;

        public override void Start() {
            base.Start();
            agent = this.GetComponent<NavMeshAgent>();
        }

        internal override void FixedUpdate() {
            base.FixedUpdate();

            if (agent.isOnOffMeshLink && !enteredLink && Vector3.Distance(agent.currentOffMeshLinkData.startPos,
                    agent.currentOffMeshLinkData.endPos) > 10f) {
                print("entered link");
                var oldPos = agent.destination;
                agent.Warp(agent.currentOffMeshLinkData.endPos);
                agent.destination = oldPos;
            }

            enteredLink = agent.isOnOffMeshLink;


            if (activeMode == MovementMode.walking && agent.remainingDistance < 10f && agent.remainingDistance > 8f) {
                blockMoves = 120;
            }

            if (activeMode == MovementMode.walking && agent.remainingDistance < 5f && blockMoves-- <= 0) {
                agent.destination = this.transform.position;
                this.activeMode = MovementMode.idle;
                clearCache();
                moveRand();
            }
        }

        public override void cacheMoveTarget() {
            if (movePosCache.Equals(Vector3.zero) && hasMoveTarget) {
                hasMoveTarget = false;
                movePosCache = agent.destination;
            }
        }

        public override void updatePos(GameObject enemy, MovementMode mode, Vector3 enemyPos) {
            this.transform.LookAt(enemy.transform.position);
            if (mode == MovementMode.inRange_staying || mode == MovementMode.walking) {
                return;
            }

            var dir = this.transform.position - enemyPos;
            var optimalDist = maxRange * optimalRange;
            positionTarget = enemyPos + dir.normalized * optimalDist;

            agent.destination = positionTarget;
            agent.updateRotation = false;
        }

        public override void moveTo(Vector3 target) {
            agent.destination = target;
            activeEnemy = null;
            activeMode = MovementMode.walking;
            blockMoves = 180;
            agent.updateRotation = true;
            hasMoveTarget = true;
        }

        public override void moveRand() {
            Vector3 goTo = this.transform.forward * UnityEngine.Random.Range(1, 8) +
                           this.transform.right * UnityEngine.Random.Range(-5f, 5f);
            if (agent != null)
                agent.destination = goTo + this.transform.position;
        }
    }
}