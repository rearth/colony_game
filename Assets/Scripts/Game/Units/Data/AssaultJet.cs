﻿using System;
using System.Collections;
using System.Collections.Generic;
using Content.Helpers.Combat;
using Game.Units.Logic;
using UnityEngine;
using Random = UnityEngine.Random;

public class AssaultJet : CombatUnit, HPHandler.IDestroyAction {
    private AirMover airController;
    private bool flyingOff;
    private bool crashing = false;

    public GameObject missilePrefab;
    public List<Transform> missilePivots;
    public float missileDelay = 0.5f;

    private List<GameObject> currentMissiles = new List<GameObject>();

    public override void Start() {
        base.Start();
        this.airController = this.GetComponent<AirMover>();
        airController.setTargetPosition(this.transform.position);
        reloadWeapons();
    }

    internal override void FixedUpdate() {
        base.FixedUpdate();
        if (flyingOff && activeEnemy == null) flyingOff = false;
    }

    public override void updatePos(GameObject enemy, MovementMode mode, Vector3 enemyPos) {
        var offset = enemy.transform.position - this.transform.position;
        offset.y = 0;
        var dist = offset.magnitude;

        if (dist < maxRange && !flyingOff) {
            fireWeapons();
            flyingOff = true;
            airController.speedBoost = 5f;
        }

        if (dist > maxRange + 1 && flyingOff) {
            flyingOff = false;
            reloadWeapons();
            airController.speedBoost = 0f;
        }

        if (flyingOff) return;

        var targetPos = enemyPos + offset.normalized * 120f;

        airController.setTargetPosition(targetPos);
    }

    private void reloadWeapons() {
        currentMissiles.Clear();
        foreach (var pivot in missilePivots) {
            var missile = GameObject.Instantiate(missilePrefab, pivot);
            currentMissiles.Add(missile);
        }
    }

    private void fireWeapons() {
        var delay = 0f;
        foreach (var missile in currentMissiles) {
            //set target
            StartCoroutine(FireMissileWithDelay(missile, delay += missileDelay));
        }
    }
    
    private void OnDrawGizmosSelected() {
        Gizmos.DrawSphere(movePosCache, 2f);
    }

    private IEnumerator FireMissileWithDelay(GameObject missile, float delay) {
        yield return new WaitForSeconds(delay);
        missile.transform.SetParent(null);
        var guide = missile.GetComponent<SimpleMissile>();
        missile.GetComponent<HPHandler>().faction = this.GetComponent<HPHandler>().faction;
        guide.targetPosition = enemyCollider;
        guide.currentSpeed = airController.getCurrentSpeed();
        guide.Fire();
    }


    public override void cacheMoveTarget() {
        if (movePosCache.Equals(Vector3.zero) && hasMoveTarget) {
            hasMoveTarget = false;
            movePosCache = airController.getTargetPosition();
        }
    }

    public override void moveTo(Vector3 target) {
        airController.setTargetPosition(target);
        airController.speedBoost = 0f;
        activeMode = MovementMode.walking;
        hasMoveTarget = true;
    }

    public override void moveRand() {
        var goTo = this.transform.forward * UnityEngine.Random.Range(1, 8) +
                   this.transform.right * UnityEngine.Random.Range(-5f, 5f);
        airController.setTargetPosition(goTo);
    }

    private void OnCollisionEnter(Collision other) {
        if (crashing) {
            GameObject.Destroy(this.gameObject, 0.2f);
            float size = this.gameObject.GetComponent<Collider>().bounds.size.magnitude * 2;
            Debug.Log("destroying object: " + this.transform.gameObject.name + " size:" + size);
            var effect =
                GameObject.Instantiate(GameObject.Find("Terrain").GetComponent<Scene_Controller>().destroyParticle,
                    this.transform.position, this.transform.rotation);
            size = 0.2f + size * 0.1f;
            effect.transform.localScale = new Vector3(size, size, size);
        }
    }

    public float beforeDestroy() {
        crashing = true;
        var rigidbody = this.gameObject.AddComponent<Rigidbody>();
        rigidbody.isKinematic = false;
        rigidbody.detectCollisions = true;
        rigidbody.velocity = airController.getCurrentSpeed() * 1.3f * this.transform.forward;
        rigidbody.angularVelocity = Random.rotation.eulerAngles * 0.02f;
        rigidbody.AddRelativeForce(10, 0, 0);

        foreach (var elem in currentMissiles) {
            GameObject.Destroy(elem);
        }
        currentMissiles.Clear();
        
        this.enabled = false;
        GameObject.Destroy(airController, 0.3f);
        return 5f;
    }
}