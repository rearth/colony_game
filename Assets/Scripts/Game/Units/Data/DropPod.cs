﻿using System;
using System.Collections;
 using System.Collections.Generic;
 using UnityEngine;
 
 public class DropPod : MonoBehaviour {

     public GameObject createOnDropped;
     public Action<GameObject> callbackOnSpawned;

     private bool done = false;

     private void OnCollisionEnter(Collision other) {
         if (done) return;
         done = true;
         
         var spawned = GameObject.Instantiate(createOnDropped, this.transform.position, Quaternion.identity);
         callbackOnSpawned.Invoke(spawned);
         this.GetComponent<HPHandler>().HP = 0;
         GameObject.Destroy(this);
     }
 }