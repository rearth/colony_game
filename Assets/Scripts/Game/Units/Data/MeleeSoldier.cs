﻿using System.Collections;
using System.Collections.Generic;
using Content.Helpers.Combat;
using Game.Combat;
using UnityEngine;
using UnityEngine.AI;

public class MeleeSoldier : NavMeshCombatUnit {
    public float Damage;
    public bool hasJumpAnim;

    private MovementMode lastState = MovementMode.idle;
    private Animator UnitAnimator;
    private static readonly int Walking = Animator.StringToHash("walk");
    private static readonly int Attack = Animator.StringToHash("attack");
    private static readonly int Idle = Animator.StringToHash("idle");
    private static readonly int Jump = Animator.StringToHash("jump");
    private float navSpeed;
    private float navAccel;
    private bool jumpStarted;

    private static readonly float jumptime = 1.2f;

    private void Awake() {
        UnitAnimator = this.GetComponent<Animator>();
        navSpeed = this.transform.GetComponent<NavMeshAgent>().speed;
        navAccel = this.transform.GetComponent<NavMeshAgent>().acceleration;
    }

    private new void FixedUpdate() {
        base.FixedUpdate();

        float dist;
        if (hasJumpAnim && !jumpStarted && base.activeMode == MovementMode.approaching && (dist = Vector3.Distance(this.transform.position, activeEnemy.transform.position)) > navSpeed * jumptime) {
            var jumpdist = (navSpeed + 3.5f) * jumptime;
            var enemySpeed = 4f;

            var enemyAgent = activeEnemy.GetComponent<NavMeshAgent>();
            if (enemyAgent != null) enemySpeed = enemyAgent.speed;
            
            var enemyMove = -enemySpeed * jumptime;
            if (activeEnemy.GetComponent<MeleeSoldier>() != null) enemyMove = enemySpeed * jumptime;
            Debug.DrawLine(this.transform.position, this.transform.position + this.transform.forward * jumpdist, Color.cyan);
            Debug.DrawLine(activeEnemy.transform.position, activeEnemy.transform.position + activeEnemy.transform.forward * enemyMove, Color.green);
            if (jumpdist > dist - enemyMove) {
                UnitAnimator.SetTrigger(Jump);
                base.agent.speed += 2;
                base.agent.acceleration += 2;
                this.optimalRange = -0.5f;
                jumpStarted = true;
                lockState = true;
            }
        }

        if (base.activeMode != lastState) {
            lastState = base.activeMode;
            updateState();
        }

        if (activeEnemy == null) return;
    }

    private void updateState() {
        //set animator state
        switch (base.activeMode) {
            case MovementMode.approaching:
            case MovementMode.walking:
            case MovementMode.tooClose:
                UnitAnimator.SetTrigger(Walking);
                break;
            case MovementMode.inRange_opening:
            case MovementMode.inRange_closing:
            case MovementMode.inRange_staying:
                base.lockState = true;
                UnitAnimator.SetTrigger(Attack);
                break;
            case MovementMode.idle:
                UnitAnimator.SetTrigger(Idle);
                this.GetComponent<NavMeshAgent>().destination = this.gameObject.transform.position;
                break;
        }
    }

    void OnDrawGizmosSelected() {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, this.scanRange);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, this.maxRange);
    }

    public void Hit(float bonusMultiplierAdd = 0f) {
        enemyHP.HP -= Damage;
    }

    public void HitEnd() {
        base.lockState = false;
        base.agent.speed = navSpeed;
        base.agent.acceleration = navAccel;
        jumpStarted = false;
        this.optimalRange = 0.2f;
    }
}