﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Combat;
using UnityEngine;

public class ParabolaMissile : MonoBehaviour, IProjectile {
    private Vector3 targetPos = Vector3.zero;
    public GameObject explosionPrefab;
    public float armTimer = 1f;
    public GameObject impactMarkerPrefab;

    private Vector3 startPos = Vector3.zero;
    private float totalDistance;
    private float totalTime;
    private float timePassed = 0;
    private Vector3 lastPos;
    private bool exploded = false;

    private void FixedUpdate() {
        if (targetPos == Vector3.zero) {
            return;
        }

        timePassed += Time.deltaTime;
        var progress = timePassed / totalTime;
        //progress = (float) (0.04761905 * progress + 0.952381 * Mathf.Pow(progress, 2));
        var height = 0.7507003 * progress + 5.747899 * Mathf.Pow(progress, 2) - 6.498599 * Mathf.Pow(progress, 3);
        height *= (totalDistance / 10f) * 3;
        var dir = targetPos - startPos;
        dir *= progress;
        dir.y += (float) height;
        
        Transform transform1 = this.transform;
        transform1.rotation = Quaternion.LookRotation(transform1.position - lastPos);
        lastPos = transform1.position;
        transform1.Rotate(new Vector3(-90f, 0, 180));
        transform1.position = startPos + dir;
    }

    public void setTarget(Vector3 target) {
        targetPos = target;
        
        startPos = this.transform.position;
        totalDistance = Vector3.Distance(startPos, targetPos);
        totalTime = totalDistance / 20;
        armTimer = totalTime / 2f;

        if (impactMarkerPrefab != null) {
            var markerAt = target;
            markerAt.y += 1.5f;
            var marker = GameObject.Instantiate(impactMarkerPrefab, markerAt, impactMarkerPrefab.transform.rotation);
            var animatedPart = marker.transform.GetChild(0);
            LeanTween.scale(animatedPart.gameObject, new Vector3(0.1f, 0.1f, 1f), totalTime);
            LeanTween.rotateAround(animatedPart.gameObject, new Vector3(0, 1, 0),360, totalTime);
            GameObject.Destroy(marker, totalTime);
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (timePassed < armTimer || exploded) return;
        Debug.Log("Guided missile exploding!");
        exploded = true;
        
        GameObject.Instantiate(explosionPrefab, this.transform.position, Quaternion.identity);
        GameObject.Destroy(this.gameObject);
    }

    public HPHandler.Faction getFaction() {
        return this.GetComponent<HPHandler>().faction;
    }

    public Vector3 futurePosition(float delta) {
        var ownPos = this.transform.position;
        var dir = ownPos - lastPos;
        dir /= Time.deltaTime;
        Debug.DrawRay(ownPos, dir, Color.green);

        return ownPos + delta * dir;
    }
}