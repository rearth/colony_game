using System;
using Game.Combat;
using UnityEngine;
using UnityEngine.AI;

namespace Content.Helpers.Combat {
    public abstract class CombatUnit : MonoBehaviour, SaveLoad.SerializableInfo {
        //enemy has hphandler, is enemy faction, and has a collider, and was the closest enemy at assignment time
        private int counter = 0;

        public GameObject activeEnemy = null;
        public MovementMode activeMode = MovementMode.idle;

        public float maxRange;
        public float minRange;
        public float scanRange;
        public float optimalRange = 0.7f;
        public bool targetProjectiles = false;
        public bool targetAir;

        public Vector3 positionTarget = Vector3.zero;
        internal HPHandler enemyHP;
        internal Collider enemyCollider;
        protected int blockMoves = 0;
        protected Vector3 movePosCache = Vector3.zero;
        protected bool hasMoveTarget = false;
        public bool lockState = false;
        private Collider owncollider;

        [System.Serializable]
        public enum MovementMode {
            approaching,
            inRange_closing,
            inRange_staying,
            inRange_opening,
            tooClose,
            idle,
            walking
        }

        public virtual void Start() {
            owncollider = this.GetComponent<Collider>();
        }

        internal virtual void FixedUpdate() {
            counter++;

            //search for new enemy
            if (counter % 10 == 0 && activeEnemy == null) {
                activeEnemy = findClosestEnemy();
                if (activeEnemy != null) {
                    enemyHP = activeEnemy.GetComponent<HPHandler>();
                    enemyCollider = activeEnemy.GetComponent<Collider>();
                }
            }

            if (activeEnemy == null) {
                if (activeEnemy == null && activeMode == MovementMode.idle) {
                    useTargetCache();
                }

                if (!lockState && activeMode != MovementMode.walking && activeMode != MovementMode.idle) {
                    activeMode = MovementMode.idle;
                }

                return;
            }

            cacheMoveTarget();

            //update state
            if (lockState) return;
            var enemyPoint = enemyCollider.ClosestPoint(this.transform.position);
            var ownPoint = owncollider.ClosestPoint(enemyPoint);
            var dist = Vector3.Distance(ownPoint, enemyPoint);

            if (dist > scanRange * 1.1f || activeEnemy == null) {
                //too far away
                activeEnemy = null;
                if (activeMode != MovementMode.walking) {
                    activeMode = MovementMode.idle;
                }

                return;
            }
            else if (dist > maxRange) {
                activeMode = MovementMode.approaching;
            }
            else if (dist > minRange) {
                //in range, betweent min and max range
                var optimalDist = maxRange * optimalRange;
                if (dist > optimalDist + 1) {
                    activeMode = MovementMode.inRange_closing;
                }
                else if (dist > optimalDist - 1) {
                    activeMode = MovementMode.inRange_staying;
                }
                else {
                    activeMode = MovementMode.inRange_opening;
                }
            }
            else if (dist <= minRange) {
                activeMode = MovementMode.tooClose;
            }

            updatePos(activeEnemy, activeMode, enemyPoint);
        }

        private void useTargetCache() {
            if (movePosCache.Equals(Vector3.zero)) {
                return;
            }

            moveTo(movePosCache);
            movePosCache = Vector3.zero;
        }

        protected void clearCache() {
            movePosCache = Vector3.zero;
            hasMoveTarget = false;
        }

        public abstract void updatePos(GameObject enemy, MovementMode mode, Vector3 enemyPos);

        public abstract void cacheMoveTarget();

        public abstract void moveTo(Vector3 target);

        public abstract void moveRand();

        //checks if target is actually attackable
        private bool targetValid(GameObject target) {
            var hp = target.GetComponent<HPHandler>();
            if (hp == null) return false;
            
            //check if requirements are met
            if (!targetProjectiles && (hp.foundFlags & HPHandler.props.Projectile) != 0) return false;
            if (!targetAir && (hp.foundFlags & HPHandler.props.Air) != 0) return false;
            return (hp.foundFlags & HPHandler.props.HasCollider) != 0;
            
        }

        internal GameObject findClosestEnemy() {
            //gather potential targets
            HPHandler.Faction self = this.GetComponent<HPHandler>().faction;

            float closest = float.MaxValue;
            GameObject result = null;

            foreach (HPHandler.Faction item in Enum.GetValues(typeof(HPHandler.Faction))) {
                //skip non-enemy factions
                if (item == self || item == HPHandler.Faction.Neutral) {
                    continue;
                }

                //loop through faction members
                var factionMembers = HPHandler.factionMembers;
                foreach (var enemy in factionMembers[item]) {
                    if (enemy == null) {
                        factionMembers[item].Remove(enemy);
                    }

                    //calc dist
                    var dist = Vector3.Distance(this.gameObject.transform.position,
                        enemy.transform.position);
                    if (dist < closest && targetValid(enemy)) {
                        closest = dist;
                        result = enemy;
                    }
                }
            }

            return result;
        }

        public SaveLoad.SerializationInfo getSerialize() {
            return new serializationData(movePosCache, positionTarget);
        }

        public void handleDeserialization(SaveLoad.SerializationInfo info) {
            var data = (serializationData) info;
            movePosCache = data.cachedTarget;
            positionTarget = data.curTarget;
        }


        [System.Serializable]
        class serializationData : SaveLoad.SerializationInfo {
            public SaveLoad.SerializableVector3 cachedTarget;
            public SaveLoad.SerializableVector3 curTarget;

            public serializationData(SaveLoad.SerializableVector3 cachedTarget, SaveLoad.SerializableVector3 curTarget) {
                this.cachedTarget = cachedTarget;
                this.curTarget = curTarget;
            }

            public override string scriptTarget {
                get { return "CombatUnit"; }
            }
        }
    }
}