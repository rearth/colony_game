﻿using System.Collections;
using System.Collections.Generic;
using Content.Helpers.Combat;
using UnityEngine;

public class CombatTank : CombatSoldier {

    public ParticleSystem impactAnim;
    private Vector3 impactPos;

//    internal override void FixedUpdate() {
//        base.FixedUpdate();
//
//        if (activeEnemy != null) {
//            var dir = activeEnemy.transform.position - turretPivot.position;
//            turretPivot.rotation = Quaternion.LookRotation(dir);
//        }
//    }

    public override void Fire() {
        if (activeEnemy == null ||
            Vector3.Distance(activeEnemy.transform.position, this.transform.position) > maxRange) return;
        
        //display shot renderer
        lineRenderer.SetPosition(0, lineRenderer.gameObject.transform.position);
        impactPos = getImpactPos(enemyCollider);
        lineRenderer.SetPosition(1, impactPos);
        
        ShotAnimator.SetTrigger(Shoot);
        impactAnim.transform.position = impactPos;
        impactAnim.Play(true);
        enemyHP.HP -= damage;
    }

    private bool isActiveFighting() {
        return (activeMode == MovementMode.inRange_closing || activeMode == MovementMode.inRange_opening ||
                activeMode == MovementMode.inRange_staying) && activeEnemy != null;
    }
    
}