﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PlayerLoop;
using Random = UnityEngine.Random;

public class AirMover : MonoBehaviour, HPHandler.IDestroyAction, SaveLoad.SerializableInfo {
    public float acceleration;
    public float maxSpeed;
    public float targetHeight;
    public float minHeight = 5;
    public float rotationSpeedScalar;
    public Transform checkHeightAt;
    public float wantedDist;
    public float speedBoost;
    public int raycastLayer;

    private static readonly Dictionary<AirMover, Vector3> plannedPositions = new Dictionary<AirMover, Vector3>();

    private float currentSpeed;
    private Vector3 targetPosition;
    private int counter;

    private void Start() {
        counter = Random.Range(0, 60);
    }

    private void FixedUpdate() {
        if (counter++ % 60 == 0) {
            updateAvoidance();
        }

        Debug.DrawLine(this.transform.position, targetPosition);
        updatePosition();
        updateHeight();
    }

    private void updateHeight() {
        var curHeight = getCurrentHeight();
        var dist = targetHeight - curHeight;
        dist *= 2f * Time.deltaTime;
        this.transform.Translate(0, dist, 0);
        var transformPosition = this.transform.position;
        if (transformPosition.y < minHeight) {
            transformPosition.y = minHeight;
            this.transform.position = transformPosition;
        }
    }

    private void updatePosition() {
        var ownPos = this.transform.position;
        var offset = targetPosition - ownPos;
        offset.y = 0;

        var dist = offset.magnitude;

        if (dist < 2f) return;

        var addRotationSpeed = 0f;
        if (dist < 10) addRotationSpeed = (10 - dist) * 0.05f;

        var targetRotation = Quaternion.LookRotation(offset);
        var diffRot = Quaternion.Inverse(transform.rotation) * targetRotation;
        var angles = diffRot.eulerAngles;
        if (angles.y > 180) angles.y = angles.y - 360;
        targetRotation *= Quaternion.Euler(-angles.y / 2 * Vector3.forward);

        targetRotation = Quaternion.Lerp(transform.rotation, targetRotation, rotationSpeedScalar + addRotationSpeed);
        this.transform.rotation = targetRotation;
        dist = Mathf.Clamp(dist, 0, (maxSpeed + speedBoost) * Time.deltaTime);

        var scaledAcc = acceleration * Time.deltaTime;
        currentSpeed = Mathf.Clamp(dist, currentSpeed - scaledAcc, currentSpeed + scaledAcc);

        var moveBy = Vector3.forward * currentSpeed;
        this.transform.Translate(moveBy, Space.Self);
    }

    private float getCurrentHeight() {
        var ray = new Ray(checkHeightAt.position, -checkHeightAt.up);
        var didHit = Physics.Raycast(ray, out var hit, targetHeight + 150);
        if (didHit) {
            var dist = hit.distance;
            return dist;
        }

        return targetHeight;
    }

    private void updateAvoidance() {
        var ownPos = this.targetPosition;
        ownPos.y = 0;
        var keys = plannedPositions.Keys.ToList();
        foreach (var key in keys) {
            if (key.Equals(this)) continue;
            if (key == null) {
                plannedPositions.Remove(key);
                continue;
            }
            var value = plannedPositions[key];
            value.y = 0;
            if (Vector3.Distance(ownPos, value) < wantedDist) {
                var dir = this.transform.position - key.transform.position;
                var newPos = ownPos + 1.05f * wantedDist * dir.normalized;
                Debug.DrawLine(ownPos, newPos, Color.red, 1f);
                key.setTargetPosition(newPos);
            }
        }
    }

    public float getCurrentSpeed() {
        return currentSpeed;
    }

    public void setTargetPosition(Vector3 value) {
        targetPosition = value;
        plannedPositions[this] = value;
    }

    public Vector3 getTargetPosition() {
        return targetPosition;
    }

    public float beforeDestroy() {
        plannedPositions.Remove(this);
        return 0;
    }

    public SaveLoad.SerializationInfo getSerialize() {
        return new serializationData(currentSpeed, targetPosition);
    }

    public void handleDeserialization(SaveLoad.SerializationInfo info) {
        var data = (serializationData) info;
        print("deserilazing air mover..., target=" + data.curTarget);
        this.currentSpeed = data.curSpeed;
        this.setTargetPosition(data.curTarget);
    }
    
    [System.Serializable]
    class serializationData : SaveLoad.SerializationInfo {
        public float curSpeed;
        public SaveLoad.SerializableVector3 curTarget;

        public serializationData(float curSpeed, Vector3 curTarget) {
            this.curSpeed = curSpeed;
            this.curTarget = curTarget;
        }

        public override string scriptTarget {
            get {
                return "AirMover";
            }
        }
    }
}