﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Content;
using Game.Systems;
using UnityEngine;
using UnityEngine.AI;

public class ActionController : MonoBehaviour, SaveLoad.SerializableInfo {
    public int APS = 2;
    public int idletimer = 100;
    public float AD = 5.0f;
    public float range = 1.5f;

    private float timer = 0f;
    private int idleTimer = 0;
    private int idleDeliveryTimer = 0;
    public State curState = State.Idle;
    private ressourceStack delivery = null;
    private GameObject target = null;
    private GameObject lastTarget = null;
    private GameObject deliverTarget = null;
    private GameObject deliverFrom = null;
    private DeliveryRoutes.route route = null;
    private bool stopDelivering = false;

    private serializationData loadedData;

    public enum State {
        Idle,
        Walking,
        Attacking,
        ConstructionDelivering,
        RouteDelivering
    };

    private void Start() {
        deliveryTargetManager = GameObject.Find("Terrain").GetComponent<DeliveryTargetManager>();
        inventory = this.GetComponent<inventory>();
        animator = GetComponent<Animator>();
        movementController = this.GetComponent<movementController>();
    }

    // Update is called once per frame
    void FixedUpdate() {

        timer += Time.deltaTime;

        if (timer < 0.5f && loadedData != null) {
            return;
        } else if (timer > 0.5f && loadedData != null) {
            loadInvs(loadedData);
        }
        
        if (curState == State.ConstructionDelivering && movementController.agent.isStopped && idleTimer++ < idletimer) {
            this.stop();
            this.setState(State.Idle);
            movementController.moveRand();
            deliveryTargetManager.triggerRefresh();
        }

        if ((curState == State.ConstructionDelivering || curState == State.RouteDelivering) &&
            movementController.agent.velocity.magnitude <= 0.1f) {
            idleTimer++;
            if (idleTimer > idletimer) {
                stop();
                movementController.moveRand();
                this.curState = State.Idle;
                clearDelivery();
                deliveryTargetManager.triggerRefresh();
            }
        }
        else if (curState == State.ConstructionDelivering || curState == State.RouteDelivering) {
            idleTimer = 0;
        }

        if (curState == State.ConstructionDelivering || curState == State.RouteDelivering) {
            return;
        }

        if (curState == State.Idle && inventory.getAmount() > 0) {
            idleDeliveryTimer++;
            if (idleDeliveryTimer > 30) {
                //Debug.Log("Inventory of " + transform.gameObject.name + " is not empty, whilst being idle, going to return point");
                goToBase();
                target = null;
                deliveryTargetManager.triggerRefresh();
            }
        }

        if (movementController.agent.velocity.magnitude > 0.1f && target == null) {
            curState = State.Walking;
        }
        else if (target != null && isAttackTarget(target)) {
            curState = State.Attacking;
        }
        else {
            curState = State.Idle;
        }

        if (target == null || !isAttackTarget(target)) {
            return;
        }

        if (timer >= 1f / (float) APS) {
            if (!inRange()) {
                //stop();
                movementController.moveTo(target.transform.position);
                return;
            }

            timer = 0f;
            attack();
        }
    }

    private bool isAttackTarget(GameObject obj) {
        if (obj.GetComponent<HPHandler>() == null) return false;
        if (obj.GetComponent<harvestableRessource>() != null) return true;
        return false;
    }

    private bool inRange() {
        return inRange(target.transform);
    }

    private bool inRange(Transform transform) {
        Vector3 closestPoint = transform.gameObject.GetComponent<Collider>().ClosestPoint(this.transform.position);
        return Vector3.Distance(closestPoint, this.transform.position) < range;
    }

    public void stop() {
        target = null;
        //Debug.Log("done attacking " + GetComponent<inventory>());
    }

    private void clearDelivery() {
        this.lastTarget = null;
        this.deliverFrom = null;
        this.deliverTarget = null;
        this.delivery = null;
    }

    private void checkFull() {
        if (inventory.isFull()) {
            Debug.Log("Inventory of " + transform.gameObject.name + " is full, going to return point");
            goToBase();
            target = null;
        }
    }

    private void attack() {
        //Debug.Log("starting attack anim");
        animator.SetTrigger("attack");
    }

    private void goToBase() {
        Transform target = movementController.getClosest("dropBase");
        movementController.setTarget(target);
        if (inRange(target)) {
            handleTarget(target);
        }
    }

    public void handleTarget(Transform target) {
        if (target == null) {
            return;
        }

        if (!inRange(target)) {
            return;
        }

        if (curState == State.RouteDelivering) {
            if (target.gameObject.Equals(route.getTarget())) {
                //arrived to target to drop off ressouces
                inventory.transferAll(route.getTarget().GetComponent<inventory>());

                if (stopDelivering) {
                    this.stopDelivering = false;
                    this.setState(State.Idle);
                    movementController.moveRand();
                    deliveryTargetManager.triggerRefresh();
                    return;
                }

                movementController.setTarget(route.getOrigin().transform);
            }
            else if (target.gameObject.Equals(route.getOrigin())) {
                //arrived at source to take ressources
                if (route is DeliveryRoutes.routeSolotype) {
                    //solotype
                    route.getOrigin().GetComponent<inventory>().transferSafe(this.GetComponent<inventory>(),
                        ((DeliveryRoutes.routeSolotype) route).getType());
                }
                else {
                    //allType
                    route.getOrigin().GetComponent<inventory>().transferAllSafe(this.GetComponent<inventory>());
                }

                movementController.setTarget(route.getTarget().transform);
            }

            return;
        }

        if (target.gameObject.Equals(deliverTarget)) {
            deliverTargetReached();
            return;
        }

        if (curState == State.ConstructionDelivering && target.gameObject.Equals(deliverFrom)) {
            deliveryFromReached();
            return;
        }
        else if (target.CompareTag("dropBase")) {
            BaseReached(target);
            return;
        }

        if (target.gameObject.name.StartsWith("Recycler")) {
            Debug.Log("reached recycling platform, destroying self now");
            if (target.GetComponent<WorkerRecycler>().hasEnoughEnergy()) {
                target.GetComponent<WorkerRecycler>().workerArrived(this.gameObject);
                Destroy(this.gameObject);
            }

            return;
        }

        if (target.gameObject.name.Contains("PickupBox")) {
            print("reached Pickup, taking all content");
            target.GetComponent<inventory>().transferAll(this.GetComponent<inventory>());
            goToBase();
            GameObject.Destroy(target.gameObject);
        }

        Vector3 toTarget = target.position - transform.position;
        this.transform.rotation = Quaternion.LookRotation(toTarget);
        lastTarget = null;

        setTarget(target.gameObject);
    }

    private void deliverTargetReached() {
        inventory.transferAll(deliverTarget.GetComponent<inventory>());
        this.curState = State.Idle;
        movementController.moveRand();

        stop();

        try {
            deliverTarget.GetComponent<building_marker>().deliveryArrived(delivery);
        }
        catch (NullReferenceException ex) {
            print("delivery target doesnt have building_marker component!");
        }

        deliveryTargetManager.triggerRefresh();
    }

    private void deliveryFromReached() {
        print("delivery from reached!");

        if (!deliverFrom.GetComponent<inventory>().canTake(delivery)) {
            print("unable to take from target inventory (too few ressources?)");
            return;
        }

        this.deliverFrom.GetComponent<inventory>().remove(delivery);
        inventory.add(delivery);

        movementController.setTarget(deliverTarget.transform, "deliver");
        this.setTarget(deliverTarget);
    }

    private void BaseReached(Transform target) {
        GetComponent<inventory>().transferAll(target.GetComponent<inventory>());
        if (lastTarget == null) {
            stop();
            movementController.moveRand();
            return;
        }

        movementController.setTarget(lastTarget.transform);
    }

    private void setTarget(GameObject target) {
        timer = 0f;
        this.target = target;
    }

    public void setState(State state) {
        /*if (curState.Equals(State.Delivering) && !state.Equals(State.Delivering)) {
            Debug.Log("delivery cancelled, removing from ordered list");
            deliverTarget.GetComponent<building_marker>().removeOrdered(this.delivery);
        }*/
        if (curState == State.RouteDelivering && state != State.RouteDelivering) {
            stopDelivering = false;
        }

        this.curState = state;
    }

    public void Hit() {
        var amount = AD;

        if (target.GetComponent<HPHandler>().HP < AD) {
            amount = target.GetComponent<HPHandler>().HP;
        }

        if (target == null || target.GetComponent<HPHandler>().HP <= 0) {
            lastTarget = null;
            stop();
            return;
        }

        ressourceStack gain = target.GetComponent<HPHandler>().inflictDamage(amount, this);
        GetComponent<inventory>().add(gain);

        var origin = new Vector3(this.transform.position.x, this.transform.position.y + 1.5f,
            this.transform.position.z);
        // Ray ray = new Ray(origin, target.transform.position - origin);
        // var hitinfo = new RaycastHit();
        // target.GetComponent<Collider>().Raycast(ray, out hitinfo, 3);
        // Debug.Log("Got harvest hit position: " + hitinfo.point);

        var rb = target.GetComponent<Rigidbody>();
        var res = target.transform.position - origin;
        //var pos = hitinfo.point;
        if (rb != null) {
            rb.AddForce(res * 100, ForceMode.Impulse);
        }

        lastTarget = target;

        checkFull();
    }

    public State getState() {
        return curState;
    }

    public void deliverTo(GameObject takeFrom, GameObject bringTo, ressourceStack stack) {

        var path = new NavMeshPath();
        if (!this.movementController.agent.CalculatePath(bringTo.transform.position, path) && Vector3.Distance(path.corners.LastOrDefault(), bringTo.GetComponent<Collider>().ClosestPoint(path.corners.LastOrDefault())) < 2f) return;
        
        print(this.name + " is delivering to: " + bringTo);

        lastTarget = null;

        if (stack.getAmount() > inventory.maxSize) {
            stack.setAmount(this.GetComponent<inventory>().maxSize);
        }

        if (this.GetComponent<inventory>().maxSize - inventory.getAmount() < stack.getAmount()) {
            Debug.Log("mover is idle and not enough inv space to deliver, emptying now (inv:)" + inventory);
            goToBase();
            target = null;
            return;
        }

        curState = State.ConstructionDelivering;
        this.setTarget(takeFrom);
        deliverTarget = bringTo;
        deliverFrom = takeFrom;
        delivery = stack;
        idleTimer = 0;

        movementController.setTarget(takeFrom.transform);
    }

    public GameObject getDeliverTarget() {
        return deliverTarget;
    }

    public ressourceStack getDelivery() {
        return this.delivery;
    }

    public SaveLoad.SerializationInfo getSerialize() {
        inventory target = this.target != null ? this.target.GetComponent<inventory>() : null;
        inventory lastTarget = this.lastTarget != null ? this.lastTarget.GetComponent<inventory>() : null;
        inventory deliverTarget = this.deliverTarget != null ? this.deliverTarget.GetComponent<inventory>() : null;
        inventory deliverFrom = this.deliverFrom != null ? this.deliverFrom.GetComponent<inventory>() : null;
        return new serializationData(idleTimer, idleDeliveryTimer, curState, delivery, target, lastTarget, deliverTarget, deliverFrom);
    }

    public void handleDeserialization(SaveLoad.SerializationInfo info) {
        serializationData data = (serializationData) info;
        this.idleTimer = data.idleTimer;
        this.idleDeliveryTimer = data.idleDeliveryTimer;
        this.curState = data.curState;
        this.delivery = data.delivery;
    }

    private void loadInvs(serializationData data) {
        var invs = inventory.inventories;
        tryLoadReference(data.target, invs, ref this.target);
        tryLoadReference(data.lastTarget, invs, ref this.lastTarget);
        tryLoadReference(data.deliverTarget, invs, ref this.deliverTarget);
        tryLoadReference(data.deliverFrom, invs, ref this.deliverFrom);
    }

    public static void tryLoadReference(int id, Dictionary<int, inventory> invs, ref GameObject assign) {
        if (id == 0 || !invs.TryGetValue(id, out var val)) return;
        assign = val.gameObject;
    }

    [System.Serializable]
    class serializationData : SaveLoad.SerializationInfo {
        public int idleTimer;
        public int idleDeliveryTimer;
        public State curState;
        public ressourceStack delivery;
        public int target;
        public int lastTarget;
        public int deliverTarget;
        public int deliverFrom;

        public serializationData(int idleTimer, int idleDeliveryTimer, State curState, ressourceStack delivery,
            inventory target, inventory lastTarget, inventory deliverTarget, inventory deliverFrom) {
            this.idleTimer = idleTimer;
            this.idleDeliveryTimer = idleDeliveryTimer;
            this.curState = curState;
            if (curState == State.RouteDelivering)
                this.curState = State.Idle;
            this.delivery = delivery;
            if (target) this.target = target.id;
            if (lastTarget) this.lastTarget = lastTarget.id;
            if (deliverFrom) this.deliverFrom = deliverFrom.id;
            if (deliverTarget) this.deliverTarget = deliverTarget.id;
        }


        public override string scriptTarget {
            get { return "ActionController"; }
        }
    }

    private static GameObject[] objs = null;
    private movementController movementController;
    private Animator animator;
    public inventory inventory;
    private DeliveryTargetManager deliveryTargetManager;

    public void setRoute(DeliveryRoutes.route route) {
        Debug.Log("Got Route: " + route + " self: " + this.gameObject.name);
        this.setState(State.RouteDelivering);
        this.route = route;
        movementController.setTarget(route.getTarget().transform);
    }

    public void stopDeliveryRoute() {
        this.stopDelivering = true;
    }

    public static GameObject atPos(Vector3 position) {
        print("searching for GameObject at: " + position);

        if (position == Vector3.zero) {
            return null;
        }

        if (objs == null) {
            objs = (GameObject[]) FindObjectsOfType(typeof(GameObject));
        }

        GameObject myObject = null;
        foreach (GameObject go in objs) {
            /*if (!go.name.Contains("Clone")) {
                continue;
            }*/
            print("distance: " + Vector3.Distance(go.transform.position, position) + " go pos=" +
                  go.transform.position + " name=" + go.name);
            if (Vector3.Distance(go.transform.position, position) < 0.5) {
                myObject = go;
                break;
            }
        }

        print("found: " + myObject);

        return myObject;
    }
}