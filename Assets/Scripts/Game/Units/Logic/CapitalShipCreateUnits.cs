﻿using System;
using System.Collections;
using System.Collections.Generic;
using Content.Helpers.Combat;
using Game.Combat;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class CapitalShipCreateUnits : MonoBehaviour {
    public GameObject dropPodPrefab;
    public GameObject unitAPrefab;
    public GameObject unitBPrefab;
    public float spawnTimer;
    public float unitCountForAttack;
    public int spawnedLayer;

    private float spawnProgress;
    private List<CombatUnit> spawned = new List<CombatUnit>();
    private Collider collider1;
    private HPHandler hpHandler;

    private void Start() {
        hpHandler = this.GetComponent<HPHandler>();
        collider1 = this.GetComponent<Collider>();
    }

    private void FixedUpdate() {
        spawnProgress += Time.deltaTime;
        if (spawnProgress > spawnTimer && spawned.Count < unitCountForAttack) {
            spawnProgress = 0;
            spawnUnit();
        }

        if (spawned.Count >= unitCountForAttack) {
            startAttack();
        }
    }

    private void startAttack() {
        print("starting capital ship unit attack");
        var target = getTarget();
        var toRemove = new List<CombatUnit>();
        foreach (var unit in spawned) {
            var navmeshunit = unit.GetComponent<NavMeshCombatUnit>();
            if (navmeshunit != null && (navmeshunit.agent == null || !navmeshunit.agent.isOnNavMesh)) {
                unit.GetComponent<HPHandler>().HP = 0;
            }
            else {
                unit.moveTo(target.transform.position);
            }

            toRemove.Add(unit);
        }

        foreach (var elem in toRemove) {
            spawned.Remove(elem);
        }
    }

    private GameObject getTarget() {
        GameObject target = null;
        var potTargets = GameObject.FindGameObjectsWithTag("dropBase");
        var minDist = Single.MaxValue;
        foreach (var elem in potTargets) {
            var dist = Vector3.Distance(elem.transform.position, this.transform.position);
            if (dist < minDist) {
                minDist = dist;
                target = elem;
            }
        }

        return target;
    }

    private void spawnUnit() {
        var useB = Random.value > 0.6f;
        var toSpawn = unitAPrefab;
        if (useB) toSpawn = unitBPrefab;
        var addHeight = this.GetComponent<Collider>().bounds.extents.y * 1.1f;
        var spawnPos = collider1.bounds.center;
        spawnPos.y += addHeight;

        var dropPod = GameObject.Instantiate(dropPodPrefab, spawnPos, Quaternion.identity);
        var dropScript = dropPod.GetComponent<DropPod>();
        dropScript.createOnDropped = toSpawn;
        dropScript.callbackOnSpawned = spawnPodLanded;
        dropPod.GetComponent<HPHandler>().faction = hpHandler.faction;

        var createdRB = dropPod.GetComponent<Rigidbody>();
        var force = new Vector3(Random.Range(-2f, 2f), 4, Random.Range(-2f, 2f));
        createdRB.AddForce(force * 5000);
        createdRB.AddTorque(force * 1000);
    }

    private void spawnPodLanded(GameObject obj) {
        var unit = obj.GetComponent<CombatUnit>();
        spawned.Add(unit);
        unit.GetComponent<HPHandler>().faction = hpHandler.faction;
        unit.moveRand();
        obj.layer = spawnedLayer;
    }
}