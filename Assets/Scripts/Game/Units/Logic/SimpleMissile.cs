﻿using System;
using Game.Combat;
using UnityEngine;

namespace Game.Units.Logic {
    public class SimpleMissile : MonoBehaviour, IProjectile {
        
        public Collider targetPosition;
        public LineRenderer designator;
        public ParticleSystem emitOnFlight;
        public ParticleSystem impactAnim;
        public float acceleration;
        public float maxSpeed;
        public float currentSpeed;
        public float damageAmount;
        public bool fired = false;
        public HPHandler.Faction owner;

        private bool hasDesignator;
        private Rigidbody rb;
        private Vector3 usePosition;
        private int ticks;
        public GameObject explosionPrefab;

        private void Start() {
            if (designator != null) {
                hasDesignator = true;
            }

            rb = this.GetComponent<Rigidbody>();
            rb.detectCollisions = false;

        }

        private void FixedUpdate() {
            if (!fired) return;
            
            if (this.ticks++ > 600) DestroySelf();

            if (targetPosition != null)
                usePosition = targetPosition.bounds.center;

            if (targetPosition == null && Vector3.Distance(usePosition, this.transform.position) < 1f) {
                DestroySelf();
            }
            
            updatePosition();
            updateRotation();
        }

        private void updateRotation() {
            this.transform.LookAt(usePosition);
            this.transform.Rotate(new Vector3(90, 0, 0));
        }

        public void Fire() {
            rb.detectCollisions = true;
            fired = true;
            var emissionModule = emitOnFlight.emission;
            emissionModule.enabled = true;
        }

        private void updatePosition() {
            var targetPos = usePosition;
            var ownPos = this.transform.position;

            if (hasDesignator) {
                designator.SetPosition(0, ownPos);
                designator.SetPosition(1, targetPos);
            }

            currentSpeed += acceleration * Time.deltaTime;
            currentSpeed = Mathf.Clamp(currentSpeed, 0, maxSpeed * Time.deltaTime);

            var dir = (targetPos - ownPos).normalized;
            var moveBy = dir * currentSpeed;
            this.transform.position += moveBy;
        }

        private void OnTriggerEnter(Collider other) {
            var otherHP = other.GetComponent<HPHandler>();
            if (otherHP == null || otherHP.faction == owner) return;

            otherHP.inflictDamage(damageAmount, null);
            DestroySelf();
        }

        private void DestroySelf() {
            
            var explosion = GameObject.Instantiate(explosionPrefab, this.transform.position + this.transform.up * 4, Quaternion.identity);
            explosion.GetComponent<SphereExplosion>().range = 3;
            explosion.GetComponent<SphereExplosion>().Damage = damageAmount;
            this.GetComponent<HPHandler>().HP = 0;
        }

        public HPHandler.Faction getFaction() {
            return this.GetComponent<HPHandler>().faction;
        }
        
        
        public Vector3 futurePosition(float delta) {
            var ownPos = this.transform.position;

            var expectedSpeed = currentSpeed + acceleration * (delta / 2);

            var dir = (usePosition - ownPos).normalized;
            var moveBy = dir * expectedSpeed;

            return ownPos + this.transform.up * 2 + moveBy;
        }
    }
}