﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemCollisionListener : MonoBehaviour {
    public PointDefenseTurret redirectTo;

    private void OnParticleCollision(GameObject other) {

        var collider = other.GetComponent<Collider>();
        if (collider != null) {
            redirectTo.onBulletCollision(collider);
        }
    }
}
