﻿using System;
using Content;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Units.Logic {
    public interface IWorker {
        bool isIdle();
        void moveRand();
        void moveTo(Vector3 targetPos, Action onTargetReached, Color pathDisplayColor);
        void moveTo(Transform targetPos, Action onTargetReached, Color pathDisplayColor);
        void displayPath(Vector3 targetPos, Color color);
        inventory getInventory();
        GameObject getGameObject();

        IWorkerTarget getCurTarget();
    }

    public interface IWorkerTarget {
        Vector3 getPosition();
    }

    public class PositionWorkerTarget : IWorkerTarget {
        private Vector3 target;

        public PositionWorkerTarget(Vector3 target) {
            this.target = target;
        }


        public Vector3 getPosition() {
            return target;
        }
    }

    public class TransformTarget : IWorkerTarget {
        private Transform target;

        public TransformTarget(Transform target) {
            this.target = target;
        }

        public Vector3 getPosition() {
            return target.position;
        }

        public Transform getTarget() {
            return target;
        }
    }
}