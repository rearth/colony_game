﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyHandler : MonoBehaviour {
    public float maxConnectionRange = 20f;
    public List<List<EnergyContainer>> networks;

    public static readonly List<EnergyContainer> allContainers = new List<EnergyContainer>();

    private int counter = 0;
    private static readonly int delayBegin = 30;
    private static readonly int skips = 5;
    

    public void reload() {
        //avoid reloading with every building that gets spawned at the begin
        if (counter < delayBegin) return;

        this.networks = findAllConnections(allContainers.GetRange(0, allContainers.Count));
        print("reloading energy connections");
    }

    private void FixedUpdate() {
        //reload only once at begin of the game
        if (counter == delayBegin + 1) {
            reload();
        }

        counter++;

        if (counter % skips == 0) {
            updateEnergy();
        }
    }

    private void updateEnergy() {
        if (networks == null) {
            return;
        }

        foreach (var network in networks) {
            float availAmount = 0;

            //split list into dicts using priority
            /* priorities:
             2: turrets
             3: barracks/similar
             4: production structures
             6: harvesting structures
             7: other
             8: batteries
             10: generators
             */

            Dictionary<int, List<EnergyContainer>> prioritiesDict = new Dictionary<int, List<EnergyContainer>>();

            for (int i = 1; i < 11; i++) {
                prioritiesDict[i] = new List<EnergyContainer>();
            }

            foreach (var elem in network) {
                var prio = elem.getPriority();
                if (prio < 1 || prio > 10) {
                    throw new Exception("invalid priority value!");
                }
                
                prioritiesDict[prio].Add(elem);
            }

            var time = Time.deltaTime * skips;

            //get total output (prios 8-10)
            for (int i = 8; i < 11; i++) {
                foreach (var elem in prioritiesDict[i]) {
                    var amount = elem.getCurEnergy() > elem.getMaxOutput() * time ? elem.getMaxOutput() * time : elem.getCurEnergy();
                    availAmount += amount;
                    elem.addEnergy(-amount, elem);
                }
            }

            for (int i = 1; i < 11; i++) {
                if (i < 8)
                    Shuffle(prioritiesDict[i]);
                var curAvail = i < 8 ? availAmount * 0.8f : availAmount;

                foreach (var elem in prioritiesDict[i]) {
                    var avail = elem.getMaxEnergy() - elem.getCurEnergy();
                    float takeAmount = avail > elem.getMaxInput() * time ? elem.getMaxInput() * time : avail;
                    takeAmount = takeAmount > availAmount ? availAmount : takeAmount;
                    elem.addEnergy(takeAmount, elem);
                    availAmount -= takeAmount;
                    curAvail -= takeAmount;

                    if (curAvail <= 0) {
                        break;
                    }
                }

            }

            /*

            //legacy from here
            //categorize into producing and draining
            foreach (var elem in network) {
                if (elem.getMaxOutput() == 0 && elem.getCurEnergy() < elem.getMaxEnergy() && elem.getMaxInput() > 0) {
                    toFill.Add(elem);
                }
                else if (elem.getMaxOutput() > 0 && elem.getCurEnergy() > 0) {
                    var amount = elem.getCurEnergy() > elem.getMaxOutput() ? elem.getMaxOutput() : elem.getCurEnergy();
                    availAmount += amount;
                    elem.addEnergy(-amount, elem);
                }
            }

            //give energy to elements that take it            
            Shuffle(toFill);
            foreach (var elem in toFill) {
                var avail = elem.getMaxEnergy() - elem.getCurEnergy();
                var takeAmount = avail > elem.getMaxInput() ? elem.getMaxInput() : avail;
                takeAmount = takeAmount > availAmount ? availAmount : takeAmount;
                elem.addEnergy(takeAmount, elem);
                availAmount -= takeAmount;

                if (availAmount < 1) {
                    break;
                }
            }

            //put remaining energy back to producing elements
            if (availAmount > 0) {
                foreach (var elem in network) {
                    if (elem.getMaxOutput() > 0 && elem.getCurEnergy() < elem.getMaxEnergy()) {
                        var avail = elem.getMaxEnergy() - elem.getCurEnergy();
                        var takeAmount = avail > elem.getMaxOutput() ? elem.getMaxOutput() : avail;
                        takeAmount = takeAmount > availAmount ? availAmount : takeAmount;
                        elem.addEnergy(takeAmount, elem);
                        availAmount -= takeAmount;

                        if (availAmount < 1) {
                            break;
                        }
                    }
                }
            }*/
        }
    }

    private Dictionary<EnergyContainer, Dictionary<EnergyContainer, float>> getDistances(List<EnergyContainer> all) {
        var dict = new Dictionary<EnergyContainer, Dictionary<EnergyContainer, float>>();

        foreach (var elemS in all) {
            elemS.clearPipes();
            dict[elemS] = new Dictionary<EnergyContainer, float>();
            foreach (var elemT in all) {
                if (elemS == elemT) continue;
                var dist = getDist(elemS, elemT);
                dict[elemS][elemT] = dist;
            }
        }

        return dict;
    }

    private List<List<EnergyContainer>> findAllConnections(List<EnergyContainer> remaining) {
        //pick a starting item
        //remove item from remaining list
        //find object with closest connection point
        //add to current network list
        //remove from remaining list
        //repeat until closest connection point is further away than maxConnectionRange
        //start again with item from remaining list
        var distances = getDistances(remaining);
        var connections = new List<List<EnergyContainer>>();

        while (remaining.Count > 0) {
            List<EnergyContainer> network = new List<EnergyContainer>();

            var startingObj = remaining[0];
            remaining.Remove(startingObj);
            network.Add(startingObj);

            while (true) {
                float minDist = float.MaxValue;
                EnergyContainer to = null;
                EnergyContainer from = null;
                foreach (var elem in network) {
                    float dist;
                    var tempRes = findClosest(elem, remaining, distances, out dist);
                    if (dist < minDist) {
                        to = tempRes;
                        minDist = dist;
                        from = elem;
                    }
                }

                if (to == null) break;
                remaining.Remove(to);
                network.Add(to);
                Piping.createPipes(from.GetGameObject().transform, to.GetGameObject().transform,
                    from.getPipes());

                //debug line
                Debug.DrawLine(from.GetGameObject().transform.position, to.GetGameObject().transform.position,
                    Color.red, 5f, false);
            }

            connections.Add(network);
        }

        return connections;
    }

    private EnergyContainer findClosest(EnergyContainer startingObj, List<EnergyContainer> remaining,
        Dictionary<EnergyContainer, Dictionary<EnergyContainer, float>> distances, out float distance) {
        float minDist = float.MaxValue;
        EnergyContainer res = null;
        foreach (var elem in remaining) {
            var dist = distances[startingObj][elem];
            if (dist < minDist && dist <= maxConnectionRange) {
                res = elem;
                minDist = dist;
            }
        }

        distance = minDist;
        return res;
    }

    private float getDist(EnergyContainer from, EnergyContainer to) {
        List<GameObject> startingPoints = findStartingPoints(from);
        List<GameObject> endPoints = findStartingPoints(to);

        float minDist = float.MaxValue;

        foreach (var sPoint in startingPoints) {
            foreach (var ePoint in endPoints) {
                var dist = Vector3.Distance(sPoint.transform.position, ePoint.transform.position);
                if (dist < minDist) {
                    minDist = dist;
                }
            }
        }

        return minDist;
    }

    private List<GameObject> findStartingPoints(EnergyContainer startingObj) {
        var list = new List<GameObject>();

        foreach (Transform child in startingObj.GetGameObject().transform) {
            if (child.gameObject.name.StartsWith("ConnectionBegin")) {
                list.Add(child.gameObject);
            }
        }

        return list;
    }

    public static void Shuffle<T>(IList<T> ts) {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i) {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }
}