﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorParticleController : MonoBehaviour {

    public ParticleSystem particleSystem;
    public Transform endpoint;
    public float workingSpeed = 5;    //number of items per second
    public bool reversed;

    private float distance = 0f;
    private float speed = 0f;
    private ParticleSystem.MainModule particleSystemMain;
    private ParticleSystem.EmissionModule particleSystemEmission;
    private Vector3 left;
    private Vector3 right;

    private void Start() {
        right = endpoint.transform.position;
        left = particleSystem.transform.position;
        distance = Vector3.Distance(left	, right);
        particleSystemMain = particleSystem.main;
        particleSystemEmission = particleSystem.emission;
        speed = particleSystemMain.startSpeed.constant;
    }

    // Update is called once per frame
    void FixedUpdate() {
        var targetTime = distance / speed;
        particleSystemMain.startLifetime = targetTime;
        
        if (workingSpeed > 10) workingSpeed = 10;
        workingSpeed = Mathf.CeilToInt(workingSpeed);
        
        particleSystemEmission.rateOverTime = workingSpeed;

        if (reversed) {
            particleSystem.transform.position = left;
            particleSystem.transform.LookAt	(right);
        }
        else {
            particleSystem.transform.position = right;
            particleSystem.transform.LookAt	(left);
        }
    }
}