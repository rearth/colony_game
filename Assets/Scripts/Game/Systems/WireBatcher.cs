﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WireBatcher : MonoBehaviour {

	private int counter = 0;
	public GameObject[] elems;
	
	private void FixedUpdate() {
		if (counter++ % 600 == 0) {
			batchAllChildren();
		}
	}

	private void batchAllChildren() {
		var childCount = this.transform.childCount;
		GameObject[] elems = new GameObject[childCount - 1];
		GameObject[] elemsB = new GameObject[childCount - 1];
		GameObject[] elemsC = new GameObject[childCount - 1];
		
		int count = 0;
		bool first = true;
		foreach (Transform elem in this.transform) {
			if (first) {
				first = false;
				continue;
			}
			elems[count] = elem.transform.GetChild(0).gameObject;
			elemsB[count] = elem.transform.GetChild(1).gameObject;
			elemsC[count] = elem.transform.GetChild(2).gameObject;
			count++;
		}
		StaticBatchingUtility.Combine(elems, this.transform.GetChild(0).GetChild(0).gameObject);
		StaticBatchingUtility.Combine(elemsB, this.transform.GetChild(0).GetChild(1).gameObject);
		StaticBatchingUtility.Combine(elemsC, this.transform.GetChild(0).GetChild(2).gameObject);


		this.elems = elems;
	}
}
