﻿using System;
using UnityEngine;

namespace Content {
    public class LinkedInventory : inventory {

        private inventory linkedTo;

        private void FixedUpdate() {
            if (linkedTo == null) findLink();
        }

        private void findLink() {
            
            linkedTo = GameObject.Find("Spaceship").GetComponent<inventory>();
            this.ownContent = linkedTo.ownContent;
        }

        private void Start() {
            findLink();
        }
    }
}
