﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Combat;
using UnityEngine;
using UnityEngine.AI;

public partial class HPHandler : MonoBehaviour, SaveLoad.SerializableInfo {
    
    public float HP = 100;
    public ressources type = ressources.Stone;
    public Faction faction = Faction.Terran;
    public bool unregistered = false;
    //[HideInInspector]
    public props foundFlags = props.None;
    private float initialHP;
    private bool destroyCalled = false;

    
    public static readonly Dictionary<Faction, List<GameObject>> factionMembers = new Dictionary<Faction, List<GameObject>>();

    public interface IDestroyAction {
        float beforeDestroy();
    }

    public enum Faction {
        Terran, Hostile, Alien, Neutral, Ally
    }
    
    [Flags]
    public enum props {
        None = 0,Air = 1, Projectile = 2, NavmeshUser = 4, HasCollider = 8, GroundCombatUnit = 16
    }

    private void OnEnable() {
        initialHP = this.HP;
    }

    // Use this for initialization
	void Start () {
        if (this.GetComponent<AirMover>() != null || this.GetComponent<CapitalShipCreateUnits>() != null) foundFlags |= props.Air;
        if (this.GetComponent<IProjectile>() != null) foundFlags |= props.Projectile;
        if (this.GetComponent<Collider>() != null) foundFlags |= props.HasCollider;
        if (this.GetComponent<NavMeshAgent>() != null) foundFlags |= props.NavmeshUser;
        if (this.GetComponent<NavMeshCombatUnit>() != null) foundFlags |= props.GroundCombatUnit;

        if (unregistered) return;
        try {
            factionMembers[faction].Add(this.gameObject);
        }
        catch (KeyNotFoundException ex) {
            foreach (HPHandler.Faction item in Enum.GetValues(typeof(HPHandler.Faction))) {
                factionMembers[item] = new List<GameObject>();
            }
            factionMembers[faction].Add(this.gameObject);
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
		if (this.HP <= 0) {
            destruct();
        }
	}

    public void destruct() {

        if (destroyCalled) return;
        
        destroyCalled = true;
        if (!unregistered)
            factionMembers[faction].Remove(this.gameObject);

        float size = this.gameObject.GetComponent<Collider>().bounds.size.magnitude;
        Debug.Log("destroying object: " + this.transform.gameObject.name + " size:" + size);
        var effect = GameObject.Instantiate(GameObject.Find("Terrain").GetComponent<Scene_Controller>().destroyParticle, this.transform.position, this.transform.rotation);
        size = 0.2f + size * 0.1f;
        effect.transform.localScale = new Vector3(size, size, size);

        float delay = 0;

        foreach (var action in this.GetComponents<IDestroyAction>()) {
            var time = action.beforeDestroy();
            if (time > delay) delay = time;
        }

        Destroy(this);
        Destroy(this.transform.gameObject, delay);
    }

    public ressourceStack inflictDamage(float amount, ActionController attacker) {
        //Debug.Log("taking damage: " + amount + " hp: " + HP);

        float multiplier = 1f;
        if (this.GetComponent<harvestableRessource>() != null)
            multiplier = this.GetComponent<harvestableRessource>().getHarvestMultiplier();

        this.HP -= amount * multiplier;
        return getReturn(amount * multiplier);
    }

    private ressourceStack getReturn(float amount) {
        return new ressourceStack(amount, this.type);
    }

    public string niceText() {
        return this.type.ToString() + ": " + this.HP.ToString() +  "/" + this.initialHP.ToString();
    }

    public float getInitialHP() {
        return initialHP;
    }

    public void overrideInitialHP(float val) {
        this.initialHP = val;
    }

    public SaveLoad.SerializationInfo getSerialize() {
        return new serializationData(HP, type, initialHP, faction);
    }

    public void handleDeserialization(SaveLoad.SerializationInfo info) {
        serializationData data = (serializationData) info;
        this.HP = data.HP;
        this.initialHP = data.initialHP;
        this.type = data.type;
        this.faction = data.faction;
    }

    [System.Serializable]
    class serializationData : SaveLoad.SerializationInfo {
        public float HP;
        public ressources type;
        public float initialHP;
        public Faction faction;

        public serializationData(float hp, ressources type, float initialHp, Faction faction) {
            HP = hp;
            this.type = type;
            initialHP = initialHp;
            this.faction = faction;
        }

        public override string scriptTarget {
            get {
                return "HPHandler";
            }
        }
    }
}
