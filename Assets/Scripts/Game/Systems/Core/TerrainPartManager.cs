﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class TerrainPartManager : MonoBehaviour {
    public List<terrainPart> parts;
    public Transform mainCamera;
    public LayerMask terrainLayer;

    private terrainPart focused;
    private int counter = 0;

    private void Start() {
        focused = parts.First();
        foreach (var part in parts) {
            var bounds = new Bounds {center = part.builder.transform.position, size = part.builder.m_Size};
            part.bounds = bounds;
        }
    }

    private void FixedUpdate() {
        if (counter % 60 == 0) {
            findActive();
            updateAll();
        }

        counter++;
    }

    private void findActive() {
        var ray = new Ray(mainCamera.position, mainCamera.forward);
        if (Physics.Raycast(ray, out var hit, 150, terrainLayer)) {
            var hitPoint = hit.point;
            foreach (var part in parts) {
                if (!part.bounds.Contains(hitPoint)) continue;
                focused = part;
                return;
            }
        }
    }

    private void updateAll() {
        foreach (var part in parts) {
            part.builder.autoUpdate = part.Equals(focused);
        }
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = new Color(1, 1, 0, 0.75F);
        foreach (var part in parts) {
            Gizmos.DrawWireCube(part.bounds.center, part.bounds.size);
        }
    }

    [System.Serializable]
    public class terrainPart {
        public Bounds bounds;
        public LocalNavMeshBuilder builder;
    }
}