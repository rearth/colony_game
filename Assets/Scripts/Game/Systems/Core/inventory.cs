﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Content {
    public class inventory : MonoBehaviour, SaveLoad.SerializableInfo {

        public static Dictionary<int, inventory> inventories = new Dictionary<int, inventory>();

        [SerializeField]
        protected internal int id;
    
        [FormerlySerializedAs("newContent")] [SerializeField]
        protected internal Dictionary<ressources, float> ownContent = new Dictionary<ressources, float>();

        public float maxSize = 50f;

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        void OnEnable() {
            foreach (ressources item in Enum.GetValues(typeof(ressources))) {
                ownContent[item] = 0;
            }

            if (id == 0) {
                id = Random.Range(100, Int32.MaxValue);
                inventories[id] = this;
            }
        }

        public void remove(ressourceStack stack) {
            ownContent[stack.getRessource()] -= stack.getAmount();
        }

        public float getAmount(ressources kind) {
            return ownContent[kind];
        }

        public void takeAmount(ressources kind, float amount) {
            ownContent[kind] -= amount;
        }

        public void transferAll(inventory target) {
            foreach (ressources item in Enum.GetValues(typeof(ressources))) {
                target.ownContent[item] += this.ownContent[item];
                this.ownContent[item] = 0;
            }
        }

        public void transferAllSafe(inventory target) {
            foreach (ressources item in Enum.GetValues(typeof(ressources))) {
                transferSafe(target, item);
            }
        }

        public float transferAllSafeCounted(inventory target, float maxSpeed = 50) {
            float count = 0;
            foreach (ressources item in Enum.GetValues(typeof(ressources))) {
                count += transferSafeCounted(target, item, maxSpeed);
            }

            return count;
        }

        public void transfer(inventory target, ressources type, float amount) {
            target.ownContent[type] += amount;
            this.ownContent[type] -= amount;
        }

        public void transfer(inventory target, ressourceStack stack) {
            target.add(stack);
            this.remove(stack);
        }

        public virtual void transferSafe(inventory target, ressources item) {
            var amount = ownContent[item];
            var freeSpace = target.getFreeSpace(item);
            if (amount > freeSpace) {
                amount = freeSpace;
            }

            target.ownContent[item] += amount;
            this.ownContent[item] -= amount;
        }
        
        public virtual float transferSafeCounted(inventory target, ressources item, float maxSpeed = 50) {
            var amount = ownContent[item];
            var freeSpace = target.getFreeSpace(item);
            if (amount > freeSpace) {
                amount = freeSpace;
            }

            amount = Mathf.Clamp(amount, 0, maxSpeed);

            target.ownContent[item] += amount;
            this.ownContent[item] -= amount;

            return amount;
        }

        public void add(ressourceStack stack) {
            this.ownContent[stack.getRessource()] += stack.getAmount();
        }

        public void add(ressources type, float amount) {
            this.ownContent[type] += amount;
        }

        public override string ToString() {
            string text = "";
            foreach (ressources item in Enum.GetValues(typeof(ressources))) {
                text += item.ToString() + ": " + this.ownContent[item] + " ";
            }

            return "inventory: " + text + " unit:" + this.transform.gameObject.name;
        }

        public bool isFull() {
            return getAmount() >= maxSize;
        }

        public float getAmount() {
            float amount = 0;
            foreach (ressources item in Enum.GetValues(typeof(ressources))) {
                amount += ownContent[item];
            }

            return amount;
        }

        //checks if there's enough ressources to be taken
        public bool canTake(ressourceStack stack) {
            return ownContent[stack.getRessource()] >= stack.getAmount();
        }

        public bool canTake(ressources kind, float amount) {
            return ownContent[kind] >= amount;
        }

        public virtual float getFreeSpace(ressources item) {
            return this.maxSize - this.getAmount();
        }

        public float getFillPercent() {
            return this.getAmount() / this.maxSize;
        }

        public float getFillPercent(ressources kind) {
            return this.getAmount(kind) / this.maxSize;
        }

        public SaveLoad.SerializationInfo getSerialize() {
            return new serializationData(maxSize, ownContent, id);
        }

        public void handleDeserialization(SaveLoad.SerializationInfo info) {

            serializationData data = (serializationData) info;
            this.maxSize = data.maxSize;
            this.ownContent = data.newContent;
            this.id = data.id;
            inventories[this.id] = this;
        }

        [System.Serializable]
        class serializationData : SaveLoad.SerializationInfo {
            public float maxSize;
            public Dictionary<ressources, float> newContent = new Dictionary<ressources, float>();
            public int id;

            public serializationData(float maxSize, Dictionary<ressources, float> newContent, int id) {
                this.maxSize = maxSize;
                this.newContent = newContent;
                this.id = id;
            }

            public override string scriptTarget {
                get { return "inventory"; }
            }
        }

        public bool canTake(List<ressourceStack> elemCost) {
            foreach (var elem in elemCost) {
                if (!canTake(elem)) return false;
            }

            return true;
        }

        public void remove(List<ressourceStack> chosenCost) {
            foreach (var elem in chosenCost)
                remove(elem);
        }
    }
}