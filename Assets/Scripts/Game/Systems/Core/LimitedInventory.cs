using System;
using System.Collections.Generic;
using Content;

namespace Game.Systems.Core {
    public class LimitedInventory : inventory {

        public Dictionary<ressources, float> limits = new Dictionary<ressources, float>();
        public List<limitEditorInput> editorLimits;

        void Start() {
            initLimits();
        }

        private void initLimits() {
            foreach (var elem in editorLimits) {
                limits[elem.kind] = elem.limit;
            }
        }

        public override float getFreeSpace(ressources item) {
            if (limits.ContainsKey(item)) {
                return limits[item] - this.getAmount(item);
            }
            
            return this.maxSize - this.getAmount();
        }
        
        [System.Serializable]
        public class limitEditorInput {
            public ressources kind;
            public float limit;
        }
    }
}