﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface EnergyContainer {

	int getMaxEnergy();
    int getCurEnergy();
    int getMaxOutput();
    int getMaxInput();
    int getPriority();          //lower = more imporant
    GameObject GetGameObject();
    List<GameObject> getPipes();
    void clearPipes();
    void addEnergy(float amount, EnergyContainer from);

    void register();
    void deregister();

}
