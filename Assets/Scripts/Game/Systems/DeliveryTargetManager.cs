﻿using System;
using System.Collections.Generic;
using System.Linq;
using Content;
using Game.Units.Logic;
using UnityEngine;

namespace Game.Systems {
    public class DeliveryTargetManager : MonoBehaviour {
        private readonly Dictionary<inventory, targetAmountData> targetData =
            new Dictionary<inventory, targetAmountData>();

        private readonly List<IWorker> allMovers = new List<IWorker>();

        private inventory baseInventory;

        private int counter = 30;
        private bool refreshTriggered = false;

        private void Start() {
            this.baseInventory = GameObject.FindGameObjectWithTag("dropBase").GetComponent<inventory>();
        }

        private void FixedUpdate() {
            counter++;
            if (counter % 300 == 0 || refreshTriggered) {
                refreshMovers();
            }

            if (counter % 30 == 0 || refreshTriggered) {
                var avail = getIdleMovers(allMovers);
                if (avail.Count > 0 && targetData.Count > 0) {
                    processData(avail);
                }

                refreshTriggered = false;
            }

            //collect available movers
            //gather elements with highest priority
            //process those elements, make priority low again
            //increase priority off all elements, those with high missing amount get even more
        }

        public void triggerRefresh() {
            refreshTriggered = true;
        }

        private void processData(List<IWorker> availMovers) {
            var invsByPriority = targetData.Keys.ToList();
            invsByPriority = invsByPriority.OrderBy(o => targetData[o].priority).ToList();
            updatePriorities();

            int prog = 0;
            foreach (var mover in availMovers) {
                for (int i = prog; i < invsByPriority.Count; i++) {
                    var inv = invsByPriority[i];
                    if (commandMover(mover, inv)) {
                        prog++;
                        targetData[inv].priority = 40;
                        targetData[inv].lastModified = Time.time;
                        break;
                    }
                }
            }
        }

        private bool commandMover(IWorker mover, inventory inv) {
            //check if ressources need to be removed
            //find out which ressources are required
            //check if ressources are available

            var data = targetData[inv];
            var useWants = new List<ressourceStack>();

            foreach (var want in data.wants) {
                var amount = want.getAmount();
                amount -= data.ordered[want.getRessource()];

                useWants.Add(new ressourceStack(amount, want.getRessource()));
            }

            var toTake = new List<ressourceStack>();
            foreach (var resourceType in inv.ownContent.Keys) {
                if (!listHasRessourceKind(resourceType, useWants)) {
                    if (inv.getAmount(resourceType) > 0) {
                        toTake.Add(new ressourceStack(inv.getAmount(resourceType), resourceType));
                    }
                }
            }

            if (toTake.Count > 0) {
                //deliverFromInvToBase
                pickupFromInv(toTake, mover, inv, data);
                foreach (var stack in toTake.Where(x => x.getAmount() > 0))
                    data.ordered[stack.getRessource()] -= stack.getAmount();
                return true;
            }

            var toBring = new List<ressourceStack>();
            var summedSize = 0f;
            foreach (var stack in useWants) {
                var missingAmount = stack.getAmount() - inv.getAmount(stack.getRessource());
                var moverStack = stack.clone();
                moverStack.setAmount(missingAmount);
                if (moverStack.getAmount() < 0.01f) continue;
                //adjust stack size for mover
                var moverCapacity = mover.getInventory().maxSize;

                if (summedSize > moverCapacity) break;
                moverCapacity -= summedSize;

                if (moverStack.getAmount() > moverCapacity) moverStack.setAmount(moverCapacity);
                if (baseInventory.canTake(moverStack)) {
                    toBring.Add(moverStack);
                    summedSize += moverStack.getAmount();
                }
            }

            if (toBring.Count > 0) {
                //deliverListToInv
                deliverToInv(toBring, mover, inv, data);
                foreach (var stack in toBring.Where(x => x.getAmount() > 0))
                    data.ordered[stack.getRessource()] += stack.getAmount();
                return true;
            }

            return false;
        }

        private void deliverToInv(List<ressourceStack> toBring, IWorker mover, inventory bringTo,
            targetAmountData data) {
            var dropBase = RessourceHelper.getClosestDropbase(mover.getGameObject().transform.position);
            EnergyHandler.Shuffle(toBring);

            mover.moveTo(dropBase.transform,
                () => collectFromReached(dropBase.transform, mover, toBring, bringTo, data), Color.red);

            //mover.deliverTo(dropBase, bringTo.gameObject, toBring[0]);
        }

        private void collectFromReached(Transform collectorFrom, IWorker mover, List<ressourceStack> toBring,
            inventory bringTo, targetAmountData data) {
            var collectInv = collectorFrom.GetComponent<inventory>();
            var moverInv = mover.getInventory();
            moverInv.transferAllSafe(collectInv);

            foreach (var stack in toBring) {
                collectInv.transferSafeCounted(moverInv, stack.getRessource(), stack.getAmount());
            }

            mover.moveTo(bringTo.transform, () => {
                    foreach (var stack in mover.getInventory().ownContent
                        .Where((pair => pair.Value > 0 && data.ordered.ContainsKey(pair.Key))))
                        data.ordered[stack.Key] -= stack.Value;
                    mover.getInventory().transferAllSafe(bringTo);
                },
                Color.yellow);
        }

        private void pickupFromInv(List<ressourceStack> toTake, IWorker mover, inventory takeFrom,
            targetAmountData data) {
            var dropBase = RessourceHelper.getClosestDropbase(takeFrom.transform.position);
            EnergyHandler.Shuffle(toTake);

            mover.moveTo(takeFrom.transform,
                () =>
                    collectFromReached(takeFrom.transform, mover, toTake, dropBase.GetComponent<inventory>(), data),
                Color.green);

            //mover.deliverTo(takeFrom.gameObject, dropBase, toTake[0]);
        }


        private void updatePriorities() {
            foreach (var targetAmountData in targetData) {
                //check if needs ressources
                targetAmountData.Value.priority++;

                if (Time.time - targetAmountData.Value.lastModified > 120) {
                    targetAmountData.Value.ordered.Clear();
                }

                var fillPercent = 0f;
                foreach (var stack in targetAmountData.Value.wants) {
                    fillPercent += targetAmountData.Key.getFillPercent(stack.getRessource());

                    if (!targetAmountData.Key.canTake(stack)) {
                        targetAmountData.Value.priority += 3;
                    }

                    //check if ressource is available
                    if (baseInventory.canTake(stack)) {
                        targetAmountData.Value.priority += 3;
                    }
                }

                if (fillPercent < targetAmountData.Value.targetFillAmount) {
                    targetAmountData.Value.priority += 2;
                }

                //check for unwanted ressources in inventory
                foreach (var ressource in targetAmountData.Key.ownContent.Keys) {
                    if (!listHasRessourceKind(ressource, targetAmountData.Value.wants)) {
                        if (targetAmountData.Key.getAmount(ressource) > 15) {
                            targetAmountData.Value.priority += 4;
                        }
                        else if (targetAmountData.Key.getAmount(ressource) > 1) {
                            targetAmountData.Value.priority += 1;
                        }
                    }
                }
            }
        }

        private bool listHasRessourceKind(ressources resource, List<ressourceStack> list) {
            foreach (var elem in list) {
                if (elem.getRessource().Equals(resource)) return true;
            }

            return false;
        }

        public void SetWantData(inventory inv, List<ressourceStack> wants, float targetFillAmount) {
            var data = new targetAmountData(wants, targetFillAmount, inv.maxSize);
            targetData[inv] = data;
            foreach (var stack in inv.ownContent) {
                data.ordered[stack.Key] = 0;
            }

            print("added new targetData");
        }

        public void clear(inventory inv) {
            targetData.Remove(inv);
        }

        private List<IWorker> getIdleMovers(List<IWorker> allMovers) {
            return allMovers.Where(controller => controller != null && controller.isIdle()).ToList();
        }

        private void refreshMovers() {
            var elems = GameObject.FindGameObjectsWithTag("mover");
            allMovers.Clear();
            foreach (var elem in elems) {
                allMovers.Add(elem.GetComponent<IWorker>());
            }
        }
    }

    class targetAmountData {
        internal List<ressourceStack> wants;
        internal Dictionary<ressources, float> ordered = new Dictionary<ressources, float>();
        internal float targetFillAmount;
        internal int priority = 50; //higher = better
        internal float lastModified = Time.time;

        public targetAmountData(List<ressourceStack> wants, float targetFillAmount, float invSize) {
            //wantsSum = targetFillAmount * invSize
            //300 = 0.6 * 1000
            //300 = 600
            //-> elem * (600 / 300)

            //scale stacks to reflection fraction of total amount
            var wantsSize = wants.Sum(elem => elem.getAmount());
            var scale = invSize * targetFillAmount / wantsSize;

            var scaledList = new List<ressourceStack>();
            foreach (var elem in wants) {
                var copy = elem.clone();
                copy.setAmount(copy.getAmount() * scale);
                scaledList.Add(copy);
            }


            this.wants = scaledList;
            this.targetFillAmount = targetFillAmount;
        }
    }
}