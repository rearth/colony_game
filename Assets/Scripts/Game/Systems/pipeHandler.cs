﻿using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

public class pipeHandler : MonoBehaviour, clickable, SaveLoad.SerializableInfo {
    public Sprite infoBut;
    public ConveyorCreator.conveyorConnection data;
    public GameObject ConveyorBegin;
    public ConveyorParticleController particleController;
    private serializationData loaded = null;
    private inventory fromInv = null;
    private inventory toInv;

    public void salvage() {
        Debug.Log("Got salvage request!");
        foreach (var obj in data.createdObjs) {
            GameObject.Destroy(obj);
        }
    }

    public PopUpCanvas.popUpOption[] getOptions() {
        PopUpCanvas.popUpOption conf = new PopUpCanvas.popUpOption(infoBut, true, "configure", doConf);

        var options = new PopUpCanvas.popUpOption[] {conf};
        return options;
    }

    public void handleLongClick() {
        this.GetComponent<ClickOptions>().Create();
        print("data: " + this.data);
    }

    private void doConf() {
        Scene_Controller.getInstance().conveyorConfigurator.SetActive(true);
        Scene_Controller.getInstance().conveyorConfigurator.GetComponent<ConveyorConfigurator>().setInstance(this);
        clickDetector.menusOpened++;
    }

    void FixedUpdate() {
        var conveyorConnection = this.getData();

        if (conveyorConnection.from == null || conveyorConnection.to == null) {
            if (loaded != null) {
                print("from or to gameobj is missing, attempting to restore from save");
                loadConveyordata(loaded);
                
                return;
            }

            print("unable to load restored configs, deleting...");
            salvage();
            return;
        }

        if (toInv == null || fromInv == null) {
            toInv = conveyorConnection.to.GetComponent<inventory>();
            fromInv = conveyorConnection.from.GetComponent<inventory>();
        }
        float left = 0f;
        float right = 0f;
        bool reversed = false;

        //all from origin to target
        if (conveyorConnection.drainAllLeft) {
            left += fromInv.transferAllSafeCounted(toInv);
        }

        //all from target to origin
        if (conveyorConnection.drainAllRight) {
            right += toInv.transferAllSafeCounted(fromInv);
        }

        if (conveyorConnection.drainLeft) {
            foreach (var elem in conveyorConnection.drainingLeft) {
                left += fromInv.transferSafeCounted(toInv, elem);
            }
        }

        if (conveyorConnection.drainRight) {
            foreach (var elem in conveyorConnection.drainingRight) {
                right += toInv.transferSafeCounted(fromInv, elem);
            }
        }

        var amount = left + right;
        
        particleController.reversed = left > right;
        particleController.workingSpeed = amount;
    }

    public void setData(ConveyorCreator.conveyorConnection data) {
        this.data = data;
        print("connection got data: " + data.from + " -> " + data.to);
    }

    public ConveyorCreator.conveyorConnection getData() {
        return data;
    }

    public void handleClick() {
        if (Salvaging.isActive()) {
            salvage();
            return;
        }
    }


    public SaveLoad.SerializationInfo getSerialize() {
        return new serializationData(this.getData(), fromInv.id, toInv.id);
    }

    public void handleDeserialization(SaveLoad.SerializationInfo info) {
        serializationData serializedData = (serializationData) info;

        //create new data
        if (inventory.inventories.ContainsKey(serializedData.@from) &&
            inventory.inventories.ContainsKey(serializedData.to)) {
            loadConveyordata(serializedData);
        }
        
        this.loaded = serializedData;
    }

    private void loadConveyordata(serializationData serializedData) {
        
        var from = inventory.inventories[serializedData.@from].gameObject;
        var to = inventory.inventories[serializedData.@to].gameObject;
        var connection = serializedData.connection.getOriginal(from, to);
        print("deserialized data: " + connection);
        this.setData(connection);

        //create conveyor boxes at end
        var rotation = ConveyorBegin.transform.rotation;
        var bBox = GameObject.Instantiate(ConveyorBegin, serializedData.fromPos, rotation);
        var eBox = GameObject.Instantiate(ConveyorBegin, serializedData.toPos, rotation);
        connection.createdObjs.Add(bBox);
        connection.createdObjs.Add(eBox);
        connection.createdObjs.Add(this.gameObject);
    }


    [System.Serializable]
    public class serializationData : SaveLoad.SerializationInfo {
        public conveyorConnectionSave connection;
        public int from;
        public int to;
        public SaveLoad.SerializableVector3 fromPos;
        public SaveLoad.SerializableVector3 toPos;

        public serializationData(ConveyorCreator.conveyorConnection conveyorConnection, int fromInvId, int toInvId) {
            this.connection = new conveyorConnectionSave(conveyorConnection);
            this.@from = fromInvId;
            this.to = toInvId;
            this.fromPos = conveyorConnection.createdObjs[0].transform.position;
            this.toPos = conveyorConnection.createdObjs[1].transform.position;

        }


        public override string scriptTarget {
            get { return "pipeHandler"; }
        }
    }

    [System.Serializable]
    public class conveyorConnectionSave {
        public List<ressources> drainingLeft;
        public List<ressources> drainingRight;
        public bool drainAllLeft;
        public bool drainAllRight;
        public bool drainLeft;
        public bool drainRight;

        public conveyorConnectionSave(ConveyorCreator.conveyorConnection conn) {
            drainAllLeft = conn.drainAllLeft;
            drainAllRight = conn.drainAllRight;
            drainingLeft = conn.drainingLeft;
            drainingRight = conn.drainingRight;
            drainLeft = conn.drainLeft;
            drainRight = conn.drainRight;
        }

        public ConveyorCreator.conveyorConnection getOriginal(GameObject from, GameObject to) {
            var res = new ConveyorCreator.conveyorConnection(from, to, new List<GameObject>());
            res.drainAllLeft = drainAllLeft;
            res.drainAllRight = drainAllRight;
            res.drainingLeft = drainingLeft;
            res.drainingRight = drainingRight;
            res.drainLeft = drainLeft;
            res.drainRight = drainRight;
            return res;
        }
    }
}