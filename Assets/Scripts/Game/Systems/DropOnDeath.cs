﻿using System.Collections.Generic;
using Content;
using UnityEngine;

public class DropOnDeath : MonoBehaviour, HPHandler.IDestroyAction {
    
    public GameObject createOnDeath;
    public List<ressourceStack> addToCreated;
    public float scaleCreated = 1f;

    public float beforeDestroy() {
        var created = GameObject.Instantiate(createOnDeath, this.transform.position, Quaternion.identity);
        created.transform.localScale *= scaleCreated;
        if (addToCreated.Count > 0) {
            var inv = created.GetComponent<inventory>();
            foreach (var stack in addToCreated) {
                inv.add(stack);
            }
        }
        return 0f;
    }
}