﻿using System.Collections;
using System.Collections.Generic;
using Content;
using UnityEngine;

public class WallConstruction : MonoBehaviour {
    private Vector3 pos1;
    private Vector3 pos2;

    public GameObject prefabWall;
    public GameObject prefabPillar;
    public GameObject bridgePrefab;
    public GameObject surfacePrefab;

    private List<ressourceStack> cost;
    private inventory inventory;
    private int counter = 0;
    private bool bridge = false;
    private bool surface;

    private void Start() {
        inventory = this.GetComponent<inventory>();
    }

    public void setData(Vector3 start, Vector3 end, ressourceStack[] cost, bool bridge, bool surface = false) {
        this.pos1 = start;
        this.pos2 = end;
        this.bridge = bridge;
        this.surface = surface;
        init(cost);
    }

    public void setData(Vector3 start, ressourceStack[] cost) {
        this.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        setData(start, Vector3.zero, cost, false);
    }

    private void init(ressourceStack[] cost) {
        this.cost = new List<ressourceStack>(cost);
    }

    private void finishConstruction() {
        GameObject result;

        if (surface) {
            result = GameObject.Instantiate(surfacePrefab, pos1, Quaternion.identity);
        } else if (pos2.Equals(Vector3.zero)) {
            result = GameObject.Instantiate(prefabPillar, pos1, Quaternion.identity);
        } else {
            var middle = pos1 + (pos2 - pos1) / 2;
            var dir = Quaternion.LookRotation(pos2 - pos1);
            var length = (pos1 - pos2).magnitude;
            print("creating wall with length: " + length + " from=" + pos1 + " to=" + pos2);

            //instantiate wall, and scale it
            if (bridge) {
                //TODO create prefab for bridges
                result = GameObject.Instantiate(prefabWall, middle, dir);
                result.transform.localScale = new Vector3(length, 0.5f, 3);
            }
            else {
                result = GameObject.Instantiate(prefabWall, middle, dir);
                result.transform.localScale = new Vector3(length, 3, 1);
            }
            result.transform.Rotate(new Vector3(1, -90, 1));
        }

        Destroy(this.gameObject);
    }

    void FixedUpdate() {
        var hasMissing = false;
        foreach (var stack in this.cost) {
            if (!inventory.canTake(stack)) {
                hasMissing = true;
                if (counter % 180 == 0 && this.inventory.getFillPercent() < 0.9f) {
                    RessourceHelper.deliverTo(this.gameObject, false, stack.getRessource());
                }
            }
        }

        if (!hasMissing) {
            //finished!
            finishConstruction();
        }

        counter++;
    }
}