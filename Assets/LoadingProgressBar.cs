﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingProgressBar : MonoBehaviour {
    public Image progressBar;
    public RectTransform spinner;

    private static LoadingProgressBar instance;
    private float progressTarget = 0;
    private bool sceneLoadDone = false;
    public Animator animator;

    public static LoadingProgressBar getInstance() {
        return instance;
    }

    private void Start() {
        instance = this;
        setRealBarProgress(0f);
    }

    private void Update() {
        var setProgress = Mathf.Lerp(progressBar.fillAmount, progressTarget, 0.3f);
        setRealBarProgress(setProgress);
        spinner.transform.Rotate(0, 0, Time.deltaTime * 360);
    }

    private void setRealBarProgress(float percent) {
        progressBar.fillAmount = percent;
    }

    public void setSceneLoadProgress(float prog) {
        if (!sceneLoadDone)
            progressTarget = prog * 0.6f;
    }

    public void setInitLoadProgress(float prog) {
        sceneLoadDone = true;
        progressTarget = 0.6f + prog * 0.4f;
        if (prog > 0.99) {
            startGame();
        }
    }

    private void startGame() {
        animator.SetTrigger("done");
    }

    public void animDone() {
        SceneManager.UnloadSceneAsync("LoadingScreen");
    }
}