﻿using System;
using System.Linq;
using Content;
using Game.Units.Logic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshWorker : MonoBehaviour, IWorker {
    public NavMeshAgent agent;
    public Animator animator;
    public GameObject trailEmitter;
    public inventory ownInv;

    private IWorkerTarget curTarget;
    private Action positionReachedCallback;
    private float lastDistance;
    private int idleTicks = 0;

    private void FixedUpdate() {
        animator.SetFloat("moveSpeed", agent.velocity.magnitude);

        var remainingDist = agent.remainingDistance;
        if (remainingDist < 1f) {
            targetReached();
        }

        if (Math.Abs(lastDistance - remainingDist) < 0.01f) idleTicks++;
        else idleTicks = 0;

        if (idleTicks > 20)
            workerStuck();

        lastDistance = remainingDist;
    }

    private void workerStuck() {
        reset();
    }

    private void targetReached() {
        var toCall = positionReachedCallback;
        reset();

        try {
            toCall?.Invoke();
        }
        catch (Exception ex) {
            print("error doing callback: " + ex);
        }
    }

    public bool isIdle() {
        return curTarget == null;
    }

    public void moveRand() {
        var goTo = this.transform.forward * UnityEngine.Random.Range(1, 5) +
                   this.transform.right * UnityEngine.Random.Range(-3f, 3f);
        moveTo(goTo, null, new Color(0, 0, 0, 0));
    }

    public void moveTo(Vector3 targetPos, Action onTargetReached, Color pathDisplayColor) {
        curTarget = new PositionWorkerTarget(targetPos);
        positionReachedCallback = onTargetReached;
        handleTarget(curTarget, pathDisplayColor);
    }

    public void moveTo(Transform targetPos, Action onTargetReached, Color pathDisplayColor) {
        curTarget = new TransformTarget(targetPos);
        positionReachedCallback = onTargetReached;
        handleTarget(curTarget, pathDisplayColor);
    }

    private void handleTarget(IWorkerTarget target, Color color) {
        var colliderClosestPoint = target.getPosition();
        if (target is TransformTarget castedTarget && castedTarget.getTarget().GetComponent<Collider>() != null) {
            colliderClosestPoint =
                castedTarget.getTarget().GetComponent<Collider>().ClosestPoint(this.transform.position);
        }

        var calculatedPath = new NavMeshPath();
        NavMesh.SamplePosition(colliderClosestPoint, out var hit, 20f,
            Scene_Controller.getInstance().normalRayCheck);

        var success = agent.CalculatePath(hit.position, calculatedPath);
        if (!success) {
            reset();
            return;
        }

        //debug draw path
        for (var i = 0; i < calculatedPath.corners.Length - 1; i++)
            Debug.DrawLine(calculatedPath.corners[i], calculatedPath.corners[i + 1], Color.magenta, 2f);

        displayPath(calculatedPath.corners.LastOrDefault(), color);
        agent.SetPath(calculatedPath);
    }

    private void reset() {
        print("resetting worker!");
        positionReachedCallback = null;
        curTarget = null;
        agent.ResetPath();
    }

    public void displayPath(Vector3 target, Color color) {
        if (Vector3.Distance(transform.position, target) < 1.0f) {
            return;
        }

        Vector3 spawnAt = transform.position;
        spawnAt.y += 1f;
        Vector3 targetAt = target;
        //targetAt.y += 2f;

        GameObject emitter = Instantiate(trailEmitter, spawnAt, transform.rotation);
        emitter.GetComponent<TrailPathMover>().setPath(targetAt);
        emitter.GetComponent<TrailPathMover>().setColor(color);
    }

    public inventory getInventory() {
        return ownInv;
    }

    public GameObject getGameObject() {
        return this.gameObject;
    }

    public IWorkerTarget getCurTarget() {
        return curTarget;
    }
}