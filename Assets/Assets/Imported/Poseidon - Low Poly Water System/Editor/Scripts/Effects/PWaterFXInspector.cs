using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
#if UNITY_POST_PROCESSING_STACK_V2
using UnityEngine.Rendering.PostProcessing;
#endif

namespace Pinwheel.Poseidon.FX
{
    [CustomEditor(typeof(PWaterFX))]
    public class PWaterFXInspector : UnityEditor.Editor
    {
        private PWaterFX instance;
        private void OnEnable()
        {
            instance = target as PWaterFX;
            if (instance.Profile != null)
            {
#if UNITY_POST_PROCESSING_STACK_V2
                if (instance.PostProcessProfile != null)
                {
                    instance.Profile.UpdatePostProcessingProfile(instance.PostProcessProfile);
                }
#endif
            }
        }

        public override void OnInspectorGUI()
        {
            if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Universal)
            {
                EditorGUILayout.LabelField("Water effect doesn't support Universal Render Pipeline (yet).", PEditorCommon.WordWrapItalicLabel);
            }
            else
            {
#if UNITY_POST_PROCESSING_STACK_V2
                instance.Water = EditorGUILayout.ObjectField("Water", instance.Water, typeof(PWater), true) as PWater;
                instance.Profile = PEditorCommon.ScriptableObjectField<PWaterFXProfile>("Profile", instance.Profile);

                if (instance.Water == null || instance.Profile == null)
                    return;
                DrawVolumesGUI();
                EditorGUI.BeginChangeCheck();
                PWaterFXProfileInspectorDrawer.Create(instance.Profile, instance.Water).DrawGUI();
                if (EditorGUI.EndChangeCheck())
                {
                    if (instance.PostProcessProfile != null)
                    {
                        instance.Profile.UpdatePostProcessingProfile(instance.PostProcessProfile);
                    }
                }
                DrawEventsGUI();
#else
                EditorGUILayout.LabelField("Water effect need the Post Processing Stack V2 to work. Please install it using the Package Manager", PEditorCommon.WordWrapItalicLabel);
#endif
            }
        }

        private void DrawVolumesGUI()
        {
            string label = "Volumes";
            string id = "volumes" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, false, id, () =>
            {
#if UNITY_POST_PROCESSING_STACK_V2
                instance.VolumeExtent = PEditorCommon.InlineVector3Field("Extent", instance.VolumeExtent);
                instance.VolumeLayer = EditorGUILayout.LayerField("Layer", instance.VolumeLayer);
#else
                EditorGUILayout.LabelField("Please install the Post Processing Stack V2 to set up volumes.", PEditorCommon.WordWrapItalicLabel);
#endif
            });
        }

        private void DrawEventsGUI()
        {
            string label = "Events";
            string id = "events" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, false, id, () =>
            {
                SerializedObject so = new SerializedObject(instance);
                SerializedProperty sp0 = so.FindProperty("onEnterWater");
                if (sp0 != null)
                {
                    EditorGUILayout.PropertyField(sp0);
                }

                SerializedProperty sp1 = so.FindProperty("onExitWater");
                if (sp1 != null)
                {
                    EditorGUILayout.PropertyField(sp1);
                }
                so.ApplyModifiedProperties();
            });
        }

        private void OnSceneGUI()
        {
            if (instance.Water == null || instance.Profile == null)
                return;

#if UNITY_POST_PROCESSING_STACK_V2 
            Bounds waterBounds = instance.Water.Bounds;
            Vector3 size = waterBounds.size + instance.VolumeExtent;

            Color color = Handles.yAxisColor;
            Matrix4x4 matrix = Matrix4x4.TRS(waterBounds.center + instance.transform.position, instance.transform.rotation, instance.transform.localScale);
            using (var scope = new Handles.DrawingScope(color, matrix))
            {
                Handles.DrawWireCube(Vector3.zero, size);
            }
#endif
        }
    }
}
