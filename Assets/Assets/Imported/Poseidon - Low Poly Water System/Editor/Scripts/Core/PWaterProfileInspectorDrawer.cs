using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace Pinwheel.Poseidon
{
    public class PWaterProfileInspectorDrawer
    {
        private PWaterProfile instance;
        private SerializedObject so;
        private SerializedProperty reflectionLayersSO;
        private SerializedProperty refractionLayersSO;

        private readonly int[] renderTextureSizes = new int[] { 128, 256, 512, 1024, 2048 };
        private readonly string[] renderTextureSizeLabels = new string[] { "128", "256", "512", "1024", "2048*" };

        private PWaterProfileInspectorDrawer(PWaterProfile instance)
        {
            this.instance = instance;
            so = new SerializedObject(instance);
            reflectionLayersSO = so.FindProperty("reflectionLayers");
            refractionLayersSO = so.FindProperty("refractionLayers");
        }

        ~PWaterProfileInspectorDrawer()
        {
            if (so != null)
            {
                so.Dispose();
            }

            if (reflectionLayersSO != null)
            {
                reflectionLayersSO.Dispose();
            }

            if (refractionLayersSO != null)
            {
                refractionLayersSO.Dispose();
            }
        }

        public static PWaterProfileInspectorDrawer Create(PWaterProfile instance)
        {
            return new PWaterProfileInspectorDrawer(instance);
        }

        public void DrawGUI()
        {
            EditorGUI.BeginChangeCheck();
            DrawRenderingSettingsGUI();
            DrawMeshSettingsGUI();
            DrawColorsSettingsGUI();
            DrawFresnelSettingsGUI();
            DrawRippleSettingsGUI();
            DrawLightAbsorbtionSettingsGUI();
            DrawFoamSettingsGUI();
            DrawReflectionSettings();
            DrawRefractionSettings();
            DrawCausticSettings();

            if (EditorGUI.EndChangeCheck())
            {
                EditorUtility.SetDirty(instance);
                instance.UpdateMaterialProperties();
            }
        }

        private void DrawRenderingSettingsGUI()
        {
            string label = "Rendering";
            string id = "water-profile-general" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, true, id, () =>
            {
                GUI.enabled = false;
                EditorGUILayout.ObjectField("Material", instance.Material, typeof(Material), false);
                GUI.enabled = true;

                if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Builtin)
                {
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PrefixLabel("Light Model");
                    Rect r = EditorGUILayout.GetControlRect();
                    if (GUI.Button(r, "Physical Based", EditorStyles.popup))
                    {
                        GenericMenu menu = new GenericMenu();
                        menu.AddItem(new GUIContent("Physical Based"), false, () => { });
                        menu.AddDisabledItem(new GUIContent("Blinn Phong (SRP)"));
                        menu.AddDisabledItem(new GUIContent("Lambert (SRP)"));
                        menu.DropDown(r);
                    }
                    EditorGUILayout.EndHorizontal();
                    instance.LightingModel = PLightingModel.PhysicalBased;
                }
                else
                {
                    instance.LightingModel = (PLightingModel)EditorGUILayout.EnumPopup("Light Model", instance.LightingModel);
                }

                instance.ShouldRenderBackface = EditorGUILayout.Toggle("Render Back Face", instance.ShouldRenderBackface);
            });
        }

        private void DrawMeshSettingsGUI()
        {
            string label = "Mesh";
            string id = "water-profile-mesh" + instance.GetInstanceID();
            GenericMenu menu = new GenericMenu();
            menu.AddItem(
                new GUIContent("Generate"),
                false,
                () => { instance.GenerateMesh(); });

            PEditorCommon.Foldout(label, true, id, () =>
            {
                EditorGUI.BeginChangeCheck();
                instance.Pattern = (PPlaneMeshPattern)EditorGUILayout.EnumPopup("Pattern", instance.Pattern);
                instance.MeshResolution = EditorGUILayout.DelayedIntField("Resolution", instance.MeshResolution);
                if (EditorGUI.EndChangeCheck())
                {
                    instance.GenerateMesh();
                }

                instance.MeshNoise = EditorGUILayout.FloatField("Noise", instance.MeshNoise);
            }, menu);
        }

        private void DrawColorsSettingsGUI()
        {
            string label = "Colors";
            string id = "water-profile-colors" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, true, id, () =>
            {
                instance.Color = EditorGUILayout.ColorField("Color", instance.Color);
                if (instance.EnableLightAbsorption)
                {
                    instance.DepthColor = EditorGUILayout.ColorField("Depth Color", instance.DepthColor);
                }
                if (instance.LightingModel == PLightingModel.PhysicalBased || instance.LightingModel == PLightingModel.BlinnPhong)
                {
                    //instance.SpecColor = EditorGUILayout.ColorField("Specular Color", instance.SpecColor);
                    instance.SpecColor = EditorGUILayout.ColorField(new GUIContent("Specular Color"), instance.SpecColor, true, false, true);
                    instance.Smoothness = EditorGUILayout.Slider("Smoothness", instance.Smoothness, 0f, 1f);
                }
            });
        }

        private void DrawFresnelSettingsGUI()
        {
            string label = "Fresnel";
            string id = "water-profile-fresnel" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, true, id, () =>
            {
                instance.FresnelStrength = EditorGUILayout.Slider("Strength", instance.FresnelStrength, 0f, 10f);
                instance.FresnelBias = EditorGUILayout.Slider("Bias", instance.FresnelBias, 0f, 1f);
            });
        }

        private void DrawLightAbsorbtionSettingsGUI()
        {
            string label = "Light Absorption";
            string id = "water-profile-absorption" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, true, id, () =>
            {
                instance.EnableLightAbsorption = EditorGUILayout.Toggle("Enable", instance.EnableLightAbsorption);
                if (instance.EnableLightAbsorption)
                {
                    instance.DepthColor = EditorGUILayout.ColorField("Depth Color", instance.DepthColor);
                    instance.MaxDepth = EditorGUILayout.FloatField("Max Depth", instance.MaxDepth);
                }
            });
        }

        private void DrawFoamSettingsGUI()
        {
            string label = "Foam";
            string id = "water-profile-foam" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, true, id, () =>
            {
                instance.EnableFoam = EditorGUILayout.Toggle("Enable", instance.EnableFoam);
                if (instance.EnableFoam)
                {
                    instance.FoamColor = EditorGUILayout.ColorField("Color", instance.FoamColor);
                    instance.FoamDistance = EditorGUILayout.FloatField("Distance", instance.FoamDistance);
                    instance.EnableFoamHQ = EditorGUILayout.Toggle("High Quality",
                        instance.EnableFoamHQ);
                    if (instance.EnableFoamHQ)
                    {
                        instance.FoamNoiseScaleHQ = EditorGUILayout.FloatField("Scale", instance.FoamNoiseScaleHQ);
                        instance.FoamNoiseSpeedHQ = EditorGUILayout.FloatField("Speed", instance.FoamNoiseSpeedHQ);
                    }
                }
            });
        }

        private void DrawRippleSettingsGUI()
        {
            string label = "Ripple";
            string id = "water-profile-ripple" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, true, id, () =>
            {
                //instance.EnableRipple = EditorGUILayout.Toggle("Enable", instance.EnableRipple);
                if (instance.EnableRipple)
                {
                    instance.RippleHeight = EditorGUILayout.Slider("Height", instance.RippleHeight, 0f, 1f);
                    instance.RippleNoiseScale = EditorGUILayout.FloatField("Scale", instance.RippleNoiseScale);
                    instance.RippleSpeed = EditorGUILayout.FloatField("Speed", instance.RippleSpeed);
                }
            });
        }

        private void DrawReflectionSettings()
        {
            string label = "Reflection";
            string id = "water-profile-reflection" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, true, id, () =>
            {
                bool valid = !PlayerSettings.virtualRealitySupported;
                if (!valid)
                {
                    EditorGUILayout.LabelField("Reflection not support VR cameras yet!", PEditorCommon.WordWrapItalicLabel);
                }

                instance.EnableReflection = EditorGUILayout.Toggle("Enable", instance.EnableReflection);
                if (instance.EnableReflection)
                {
                    instance.ReflectCustomSkybox = EditorGUILayout.Toggle("Custom Skybox", instance.ReflectCustomSkybox);
                    instance.EnableReflectionPixelLight = EditorGUILayout.Toggle("Pixel Light", instance.EnableReflectionPixelLight);
                    instance.EnableReflectionBlur = EditorGUILayout.Toggle("Blur", instance.EnableReflectionBlur);
                    instance.ReflectionClipPlaneOffset = EditorGUILayout.FloatField("Clip Plane Offset", instance.ReflectionClipPlaneOffset);

                    if (reflectionLayersSO != null)
                    {
                        EditorGUILayout.PropertyField(reflectionLayersSO);
                    }
                    so.ApplyModifiedProperties();

                    instance.ReflectionTextureResolution = EditorGUILayout.IntPopup("Resolution", instance.ReflectionTextureResolution, renderTextureSizeLabels, renderTextureSizes);
                    instance.ReflectionDistortionStrength = EditorGUILayout.FloatField("Distortion", instance.ReflectionDistortionStrength);
                }
            });
        }

        private void DrawRefractionSettings()
        {
            string label = "Refraction";
            string id = "water-profile-refraction" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, true, id, () =>
            {
                bool valid = !PlayerSettings.virtualRealitySupported;
                if (!valid)
                {
                    EditorGUILayout.LabelField("Refraction not support VR cameras yet!", PEditorCommon.WordWrapItalicLabel);
                }

                instance.EnableRefraction = EditorGUILayout.Toggle("Enable", instance.EnableRefraction);
                if (instance.EnableRefraction)
                {
                    if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Builtin)
                    {
                        instance.EnableRefractionPixelLight = EditorGUILayout.Toggle("Pixel Light", instance.EnableRefractionPixelLight);
                        instance.RefractionClipPlaneOffset = EditorGUILayout.FloatField("Clip Plane Offset", instance.RefractionClipPlaneOffset);

                        if (refractionLayersSO != null)
                        {
                            EditorGUILayout.PropertyField(refractionLayersSO);
                        }
                        so.ApplyModifiedProperties();

                        instance.RefractionTextureResolution = EditorGUILayout.IntPopup("Resolution", instance.RefractionTextureResolution, renderTextureSizeLabels, renderTextureSizes);
                    }
                    instance.RefractionDistortionStrength = EditorGUILayout.FloatField("Distortion", instance.RefractionDistortionStrength);

                    if (PCommon.CurrentRenderPipeline != PRenderPipelineType.Builtin)
                    {
                        EditorGUILayout.LabelField("Refraction effect reads directly from Camera Opaque Texture in LWRP/URP", PEditorCommon.WordWrapItalicLabel);
                    }
                }
            });
        }

        private void DrawCausticSettings()
        {
            string label = "Caustic";
            string id = "water-profile-caustic" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, true, id, () =>
            {
                bool valid = instance.EnableLightAbsorption && instance.EnableRefraction;
                if (!valid)
                {
                    EditorGUILayout.LabelField("Requires Light Absorbtion and Refraction.", PEditorCommon.WordWrapItalicLabel);
                    instance.EnableCaustic = false;
                }

                GUI.enabled = valid;
                instance.EnableCaustic = EditorGUILayout.Toggle("Enable", instance.EnableCaustic);
                if (instance.EnableCaustic)
                {
                    instance.CausticTexture = PEditorCommon.InlineTextureField("Texture", instance.CausticTexture, -1);
                    instance.CausticSize = EditorGUILayout.FloatField("Size", instance.CausticSize);
                    instance.CausticStrength = EditorGUILayout.Slider("Strength", instance.CausticStrength, 0f, 1f);
                    instance.CausticDistortionStrength = EditorGUILayout.FloatField("Distortion", instance.CausticDistortionStrength);
                }
                GUI.enabled = true;
            });
        }
    }
}
