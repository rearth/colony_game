using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using Pinwheel.Poseidon.FX;

namespace Pinwheel.Poseidon
{
    [CustomEditor(typeof(PWater))]
    public class PWaterInspector : UnityEditor.Editor
    {
        private PWater instance;
        private bool isTilePinningActive;
        private bool willDrawDebugGUI = true;

        private void OnEnable()
        {
            instance = target as PWater;
            if (instance.Profile != null)
            {
                instance.ReCalculateBounds();
            }
        }

        public override void OnInspectorGUI()
        {
            instance.Profile = PEditorCommon.ScriptableObjectField<PWaterProfile>("Profile", instance.Profile);
            if (instance.Profile == null)
                return;

            PWaterProfileInspectorDrawer.Create(instance.Profile).DrawGUI();
            DrawTilesGUI();
            DrawEffectsGUI();
            if (willDrawDebugGUI)
                DrawDebugGUI();
        }

        private void DrawEffectsGUI()
        {
            if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Universal)
                return;
            PWaterFX fx = instance.GetComponent<PWaterFX>();
            if (fx != null)
                return;

            string label = "Effects";
            string id = "effects" + instance.GetInstanceID();

            PEditorCommon.Foldout(label, false, id, () =>
            {
                bool isStackV2Installed = false;
#if UNITY_POST_PROCESSING_STACK_V2
                isStackV2Installed = true;
#endif
                if (!isStackV2Installed)
                {
                    EditorGUILayout.LabelField("Water effect need the Post Processing Stack V2 to work. Please install it using the Package Manager", PEditorCommon.WordWrapItalicLabel);
                }
                GUI.enabled = isStackV2Installed;
                if (GUILayout.Button("Add Effects"))
                {
                    fx = instance.gameObject.AddComponent<PWaterFX>();
                    fx.Water = instance;
                }
                GUI.enabled = true;
            });
        }

        private void DrawTilesGUI()
        {
            string label = "Tiles";
            string id = "water-tiles" + instance.GetInstanceID();

            GenericMenu menu = new GenericMenu();
            menu.AddItem(
                new GUIContent("Recalculate Bounds"),
                false,
                () => { instance.ReCalculateBounds(); });

            isTilePinningActive = PEditorCommon.Foldout(label, false, id, () =>
            {
                EditorGUILayout.LabelField("In the Scene View, Click to pin, Ctrl+Click to unpin", PEditorCommon.WordWrapItalicLabel);
                EditorGUILayout.LabelField("Scene View Gizmos must be enabled.", PEditorCommon.WordWrapItalicLabel);

                EditorGUI.BeginChangeCheck();
                instance.TileSize = PEditorCommon.InlineVector2Field("Tile Size", instance.TileSize);
                if (EditorGUI.EndChangeCheck())
                {
                    instance.ReCalculateBounds();
                }

                SerializedObject so = new SerializedObject(instance);
                SerializedProperty sp = so.FindProperty("tileIndices");

                if (sp != null)
                {
                    EditorGUI.BeginChangeCheck();
                    EditorGUILayout.PropertyField(sp, true);
                    if (EditorGUI.EndChangeCheck())
                    {
                        so.ApplyModifiedProperties();
                    }
                }

                sp.Dispose();
                so.Dispose();
            },
            menu);
        }

        private void OnSceneGUI()
        {
            Tools.hidden = isTilePinningActive;
            DrawTilesPinningGUI();
            DrawBounds();
        }

        private void DrawTilesPinningGUI()
        {
            if (!isTilePinningActive)
                return;
            if (Event.current == null)
                return;
            if (instance.Profile == null)
                return;

            Plane plane = new Plane(Vector3.up, instance.transform.position);
            Ray r = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            float distance = -1;
            if (plane.Raycast(r, out distance))
            {
                Vector3 hitWorldPos = r.origin + r.direction * distance;
                Vector3 hitLocalPos = instance.transform.InverseTransformPoint(hitWorldPos);
                PIndex2D index = new PIndex2D(
                    Mathf.FloorToInt(hitLocalPos.x / instance.TileSize.x),
                    Mathf.FloorToInt(hitLocalPos.z / instance.TileSize.y));
                Handles.Label(hitWorldPos, index.ToString());

                Vector3 rectCenter = instance.transform.TransformPoint(new Vector3(
                    (index.X + 0.5f) * instance.TileSize.x,
                    hitLocalPos.y,
                    (index.Z + 0.5f) * instance.TileSize.y));
                Vector3 buttonSize = instance.transform.TransformVector(Vector3.one * Mathf.Min(instance.TileSize.x, instance.TileSize.y) * 0.5f);

                if (Handles.Button(rectCenter, Quaternion.Euler(90, 0, 0), buttonSize.x, buttonSize.x, Handles.RectangleHandleCap))
                {
                    if (Event.current.button != 0)
                        return;
                    if (Event.current.control)
                    {
                        instance.TileIndices.Remove(index);
                        instance.ReCalculateBounds();
                    }
                    else
                    {
                        if (!instance.TileIndices.Contains(index))
                        {
                            instance.TileIndices.Add(index);
                            instance.ReCalculateBounds();
                        }
                    }
                    PUtilities.MarkCurrentSceneDirty();
                    EditorUtility.SetDirty(instance);
                }
            }
        }

        private void DrawBounds()
        {
            if (!isTilePinningActive)
                return;
            if (Event.current == null)
                return;
            if (instance.Profile == null)
                return;

            Vector3 center = instance.transform.TransformPoint(instance.Bounds.center);
            Vector3 size = instance.transform.TransformVector(instance.Bounds.size);
            Handles.color = Color.yellow;
            Handles.DrawWireCube(center, size);
        }

        public void DrawDebugGUI()
        {
            string label = "Debug";
            string id = "debug" + instance.GetInstanceID().ToString();

            PEditorCommon.Foldout(label, false, id, () =>
            {
                Camera[] cams = instance.GetComponentsInChildren<Camera>();
                for (int i = 0; i < cams.Length; ++i)
                {
                    if (!cams[i].name.StartsWith("~"))
                        continue;
                    if (cams[i].targetTexture == null)
                        continue;
                    EditorGUILayout.LabelField(cams[i].name);
                    Rect r = GUILayoutUtility.GetAspectRect(1);
                    EditorGUI.DrawPreviewTexture(r, cams[i].targetTexture);
                    EditorGUILayout.Space();
                }
            });
        }

        public override bool RequiresConstantRepaint()
        {
            return willDrawDebugGUI;
        }
    }
}
