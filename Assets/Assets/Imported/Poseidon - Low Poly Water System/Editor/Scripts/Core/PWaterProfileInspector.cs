using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace Pinwheel.Poseidon
{
    [CustomEditor(typeof(PWaterProfile))]
    public class PWaterProfileInspector : UnityEditor.Editor
    {
        private PWaterProfile instance;
        private void OnEnable()
        {
            instance = target as PWaterProfile;
        }
        
        public override void OnInspectorGUI()
        {
            PWaterProfileInspectorDrawer.Create(instance).DrawGUI();
        }
    }
}
