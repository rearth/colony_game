using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Pinwheel.Poseidon
{
    [System.Serializable]
    public struct PInternalShaderSettings
    {
        [SerializeField]
        private Shader copyTextureShader;
        public Shader CopyTextureShader
        {
            get
            {
                return copyTextureShader;
            }
            set
            {
                copyTextureShader = value;
            }
        }

        [SerializeField]
        private Shader solidColorShader;
        public Shader SolidColorShader
        {
            get
            {
                return solidColorShader;
            }
            set
            {
                solidColorShader = value;
            }
        }

#if UNITY_POST_PROCESSING_STACK_V2
        [SerializeField]
        private Shader underwaterShader;
        public Shader UnderwaterShader
        {
            get
            {
                return underwaterShader;
            }
            set
            {
                underwaterShader = value;
            }
        }

        [SerializeField]
        private Shader wetLensShader;
        public Shader WetLensShader
        {
            get
            {
                return wetLensShader;
            }
            set
            {
                wetLensShader = value;
            }
        }
#endif
    }
}
