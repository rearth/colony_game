using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Pinwheel.Poseidon
{
    public enum PRenderPipelineType
    {
        Builtin, Lightweight, Universal
    }
}
