using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering;
using UnityEngine.Events;
#if UNITY_POST_PROCESSING_STACK_V2
using Pinwheel.Poseidon.FX.PostProcessing;
using UnityEngine.Rendering.PostProcessing;
#endif

namespace Pinwheel.Poseidon.FX
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(PWater))]
    public class PWaterFX : MonoBehaviour
    {
        [SerializeField]
        private PWater water;
        public PWater Water
        {
            get
            {
                return water;
            }
            set
            {
                water = value;
            }
        }

        [SerializeField]
        private PWaterFXProfile profile;
        public PWaterFXProfile Profile
        {
            get
            {
                return profile;
            }
            set
            {
                PWaterFXProfile oldProfile = profile;
                PWaterFXProfile newProfile = value;
                profile = newProfile;

                if (oldProfile != newProfile)
                {
#if UNITY_POST_PROCESSING_STACK_V2
                    if (PostProcessProfile != null)
                    {
                        profile.UpdatePostProcessingProfile(PostProcessProfile);
                    }
#endif
                }
            }
        }

#if UNITY_POST_PROCESSING_STACK_V2
        public PostProcessProfile PostProcessProfile { get; private set; }
        public PostProcessVolume PostProcessVolume { get; private set; }

        [SerializeField]
        private Vector3 volumeExtent;
        public Vector3 VolumeExtent
        {
            get
            {
                return volumeExtent;
            }
            set
            {
                Vector3 v = value;
                //v.x = Mathf.Max(0, v.x);
                //v.y = Mathf.Max(0, v.y);
                //v.z = Mathf.Max(0, v.z);
                volumeExtent = v;
            }
        }

        [SerializeField]
        private LayerMask volumeLayer;
        public LayerMask VolumeLayer
        {
            get
            {
                return volumeLayer;
            }
            set
            {
                volumeLayer = value;
            }
        }

        private Vector3 lastCameraPos;
        private float wetLensTime;
#endif

        [SerializeField]
        private UnityEvent onEnterWater;
        public UnityEvent OnEnterWater
        {
            get
            {
                return onEnterWater;
            }
            set
            {
                onEnterWater = value;
            }
        }

        [SerializeField]
        private UnityEvent onExitWater;
        public UnityEvent OnExitWater
        {
            get
            {
                return onExitWater;
            }
            set
            {
                onExitWater = value;
            }
        }

        private void Reset()
        {
            Water = GetComponent<PWater>();
#if UNITY_POST_PROCESSING_STACK_V2
            VolumeExtent = new Vector3(0, 100, 0);
#endif
        }

        private void OnEnable()
        {
            Camera.onPreCull += OnCameraPreCull;
            RenderPipelineManager.beginCameraRendering += OnBeginCameraRendering;

#if UNITY_POST_PROCESSING_STACK_V2
            lastCameraPos = Camera.main ? Camera.main.transform.position : Vector3.zero;
            wetLensTime = Mathf.Infinity;

            if (PUtilities.IsPlaying)
            {
                SetupQuickVolume();
            }
#endif
        }

        private void OnDisable()
        {
            Camera.onPreCull -= OnCameraPreCull;
            RenderPipelineManager.beginCameraRendering -= OnBeginCameraRendering;
#if UNITY_POST_PROCESSING_STACK_V2
            CleanupQuickVolume();
#endif
        }

        private void OnBeginCameraRendering(ScriptableRenderContext context, Camera cam)
        {
            OnCameraPreCull(cam);
        }

        private void OnCameraPreCull(Camera cam)
        {
#if UNITY_POST_PROCESSING_STACK_V2
            UpdatePostProcessing(cam);
            if (cam == Camera.main && Water != null)
            {
                Vector3 cameraPos = Camera.main.transform.position;
                if (cameraPos.y <= transform.position.y && lastCameraPos.y > transform.position.y)
                {
                    if (Water.CheckTilesContainPoint(cameraPos))
                    {
                        OnEnterWater.Invoke();
                    }
                }
                if (cameraPos.y > transform.position.y && lastCameraPos.y <= transform.position.y)
                {
                    if (Water.CheckTilesContainPoint(cameraPos))
                    {
                        OnExitWater.Invoke();
                    }
                }
                lastCameraPos = cameraPos;
            }
#endif
        }

#if UNITY_POST_PROCESSING_STACK_V2
        private void SetupQuickVolume()
        {
            if (Water == null || Profile == null)
                return;

            Water.ReCalculateBounds();
            Bounds bounds = Water.Bounds;

            PostProcessEffectSettings[] settings = new PostProcessEffectSettings[0];
            PostProcessVolume volume = PostProcessManager.instance.QuickVolume(VolumeLayer, 0, settings);
            volume.isGlobal = false;
            volume.gameObject.hideFlags = HideFlags.DontSave;
            volume.gameObject.transform.parent = transform;
            volume.gameObject.transform.localPosition = bounds.center;
            volume.gameObject.transform.localRotation = Quaternion.identity;
            volume.gameObject.transform.localScale = Vector3.one;
            volume.gameObject.name = "~WaterPostFXVolume";
            PostProcessVolume = volume;

            BoxCollider b = volume.gameObject.AddComponent<BoxCollider>();
            b.center = new Vector3(0, 0, 0);
            b.size = bounds.size + VolumeExtent;
            b.isTrigger = true;
            PostProcessProfile = volume.profile;
            Profile.UpdatePostProcessingProfile(PostProcessProfile);
        }

        private void CleanupQuickVolume()
        {
            if (PostProcessProfile != null)
            {
                PUtilities.DestroyObject(PostProcessProfile);
            }
            if (PostProcessVolume != null)
            {
                PUtilities.DestroyGameobject(PostProcessVolume.gameObject);
            }
        }

        private void UpdatePostProcessing(Camera cam)
        {
            if (cam == null)
                return;
            if (cam != Camera.main)
                return;
            if (Profile == null)
                return;
            if (PostProcessProfile == null)
                return;

            if (Profile.EnableUnderwater)
            {
                PUnderwater underwaterSettings;
                if (PostProcessProfile.TryGetSettings<PUnderwater>(out underwaterSettings))
                {
                    float intensity = cam.transform.position.y < underwaterSettings.waterLevel ? 1 : 0;
                    underwaterSettings.intensity.Override(intensity);
                }
            }

            Vector3 cameraPos = Camera.main.transform.position;
            if (Profile.EnableWetLens)
            {
                if (cameraPos.y > transform.position.y && lastCameraPos.y <= transform.position.y)
                {
                    wetLensTime = 0;
                }

                PWetLens wetLensSettings;
                if (PostProcessProfile.TryGetSettings<PWetLens>(out wetLensSettings))
                {
                    if (cameraPos.y <= transform.position.y)
                    {
                        wetLensSettings.intensity.Override(0);
                    }
                    else if (wetLensTime > Profile.WetLensDuration)
                    {
                        wetLensSettings.intensity.Override(0);
                    }
                    else
                    {
                        float f = Mathf.InverseLerp(0, Profile.WetLensDuration, wetLensTime);
                        float intensity = Profile.WetLensFadeCurve.Evaluate(f);
                        wetLensSettings.intensity.Override(intensity);
                    }
                }
            }
            wetLensTime += PUtilities.DeltaTime;
        }
#endif
    }
}
