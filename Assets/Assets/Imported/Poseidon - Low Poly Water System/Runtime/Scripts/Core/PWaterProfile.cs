using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Rendering;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Pinwheel.Poseidon
{
    [CreateAssetMenu(menuName = "Poseidon/Water Profile")]
    public class PWaterProfile : ScriptableObject
    {
        [SerializeField]
        private PWaterMode waterMode;
        public PWaterMode WaterMode
        {
            get
            {
                return waterMode;
            }
            set
            {
                waterMode = value;
            }
        }

        [SerializeField]
        private PPlaneMeshPattern pattern;
        public PPlaneMeshPattern Pattern
        {
            get
            {
                return pattern;
            }
            set
            {
                pattern = value;
            }
        }

        [SerializeField]
        private int meshResolution;
        public int MeshResolution
        {
            get
            {
                return meshResolution;
            }
            set
            {
                meshResolution = Mathf.Clamp(value, 2, 100);
                if (meshResolution % 2 == 1)
                {
                    meshResolution -= 1;
                }
            }
        }

        [SerializeField]
        private float meshNoise;
        public float MeshNoise
        {
            get
            {
                return meshNoise;
            }
            set
            {
                meshNoise = Mathf.Max(0, value);
            }
        }

        [SerializeField]
        private Mesh mesh;
        public Mesh Mesh
        {
            get
            {
                if (mesh == null)
                {
                    mesh = new Mesh();
                    mesh.MarkDynamic();
                    GenerateMesh();
                }
#if UNITY_EDITOR
                if (!AssetDatabase.Contains(mesh) && EditorUtility.IsPersistent(this))
                {
                    AssetDatabase.AddObjectToAsset(mesh, this);
                }
#endif
                return mesh;
            }
        }

        [SerializeField]
        private Material material;
        public Material Material
        {
            get
            {
                if (material == null)
                {
                    Shader shader =
                        PCommon.CurrentRenderPipeline == PRenderPipelineType.Universal ? PPoseidonSettings.Instance.WaterBasicURPShader :
                        PCommon.CurrentRenderPipeline == PRenderPipelineType.Lightweight ? PPoseidonSettings.Instance.WaterBasicLWRPShader :
                        PPoseidonSettings.Instance.WaterBasicShader;
                    material = new Material(shader);
                    UpdateMaterialProperties();
                }
#if UNITY_EDITOR
                if (!AssetDatabase.Contains(material) && EditorUtility.IsPersistent(this))
                {
                    AssetDatabase.AddObjectToAsset(material, this);
                }
#endif
                material.name = material.shader.name;
                return material;
            }
        }

        [SerializeField]
        private PLightingModel lightingModel;
        public PLightingModel LightingModel
        {
            get
            {
                if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Builtin)
                {
                    lightingModel = PLightingModel.PhysicalBased;
                }
                return lightingModel;
            }
            set
            {
                lightingModel = value;
            }
        }

        [SerializeField]
        private bool shouldRenderBackFace;
        public bool ShouldRenderBackface
        {
            get
            {
                return shouldRenderBackFace;
            }
            set
            {
                shouldRenderBackFace = value;
            }
        }

        [SerializeField]
        private Color color;
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        [SerializeField]
        private Color specColor;
        public Color SpecColor
        {
            get
            {
                return specColor;
            }
            set
            {
                specColor = value;
            }
        }

        [SerializeField]
        private float smoothness;
        public float Smoothness
        {
            get
            {
                return smoothness;
            }
            set
            {
                smoothness = Mathf.Clamp01(value);
            }
        }

        [FormerlySerializedAs("enableLightAbsorbtion")]
        [SerializeField]
        private bool enableLightAbsorption;
        public bool EnableLightAbsorption
        {
            get
            {
                return enableLightAbsorption;
            }
            set
            {
                enableLightAbsorption = value;
            }
        }

        [SerializeField]
        private Color depthColor;
        public Color DepthColor
        {
            get
            {
                return depthColor;
            }
            set
            {
                depthColor = value;
            }
        }

        [SerializeField]
        private float maxDepth;
        public float MaxDepth
        {
            get
            {
                return maxDepth;
            }
            set
            {
                maxDepth = Mathf.Max(0, value);
            }
        }

        [SerializeField]
        private bool enableFoam;
        public bool EnableFoam
        {
            get
            {
                return enableFoam;
            }
            set
            {
                enableFoam = value;
            }
        }

        [SerializeField]
        private Color foamColor;
        public Color FoamColor
        {
            get
            {
                return foamColor;
            }
            set
            {
                foamColor = value;
            }
        }

        [SerializeField]
        private float foamDistance;
        public float FoamDistance
        {
            get
            {
                return foamDistance;
            }
            set
            {
                foamDistance = Mathf.Max(0, value);
            }
        }

        [SerializeField]
        private bool enableFoamHQ;
        public bool EnableFoamHQ
        {
            get
            {
                return enableFoamHQ;
            }
            set
            {
                enableFoamHQ = value;
            }
        }

        [SerializeField]
        private float foamNoiseScaleHQ;
        public float FoamNoiseScaleHQ
        {
            get
            {
                return foamNoiseScaleHQ;
            }
            set
            {
                foamNoiseScaleHQ = value;
            }
        }

        [SerializeField]
        private float foamNoiseSpeedHQ;
        public float FoamNoiseSpeedHQ
        {
            get
            {
                return foamNoiseSpeedHQ;
            }
            set
            {
                foamNoiseSpeedHQ = value;
            }
        }

#pragma warning disable 0414
        [SerializeField]
        private bool enableRipple;
#pragma warning restore 0414
        public bool EnableRipple
        {
            get
            {
                return true;
            }
            set
            {
                //enableRipple = value;
                enableRipple = true;
            }
        }

        [SerializeField]
        private float rippleHeight;
        public float RippleHeight
        {
            get
            {
                return rippleHeight;
            }
            set
            {
                rippleHeight = Mathf.Clamp01(value);
            }
        }

        [SerializeField]
        private float rippleSpeed;
        public float RippleSpeed
        {
            get
            {
                return rippleSpeed;
            }
            set
            {
                rippleSpeed = value;
            }
        }

        [SerializeField]
        private float rippleNoiseScale;
        public float RippleNoiseScale
        {
            get
            {
                return rippleNoiseScale;
            }
            set
            {
                rippleNoiseScale = value;
            }
        }

        [SerializeField]
        private float fresnelStrength;
        public float FresnelStrength
        {
            get
            {
                return fresnelStrength;
            }
            set
            {
                fresnelStrength = Mathf.Max(0, value);
            }
        }

        [SerializeField]
        private float fresnelBias;
        public float FresnelBias
        {
            get
            {
                return fresnelBias;
            }
            set
            {
                fresnelBias = Mathf.Clamp01(value);
            }
        }

        [SerializeField]
        private bool enableReflection;
        public bool EnableReflection
        {
            get
            {
                return enableReflection;
            }
            set
            {
                enableReflection = value;
            }
        }

        [SerializeField]
        private int reflectionTextureResolution;
        public int ReflectionTextureResolution
        {
            get
            {
                return reflectionTextureResolution;
            }
            set
            {
                reflectionTextureResolution = Mathf.Clamp(Mathf.ClosestPowerOfTwo(value), 32, 2048);
            }
        }

        [SerializeField]
        private bool enableReflectionPixelLight;
        public bool EnableReflectionPixelLight
        {
            get
            {
                return enableReflectionPixelLight;
            }
            set
            {
                enableReflectionPixelLight = value;
            }
        }

        [SerializeField]
        private bool enableReflectionBlur;
        public bool EnableReflectionBlur
        {
            get
            {
                return enableReflectionBlur;
            }
            set
            {
                enableReflectionBlur = value;
            }
        }

        [SerializeField]
        private float reflectionClipPlaneOffset;
        public float ReflectionClipPlaneOffset
        {
            get
            {
                return reflectionClipPlaneOffset;
            }
            set
            {
                reflectionClipPlaneOffset = value;
            }
        }

        [SerializeField]
        private LayerMask reflectionLayers;
        public LayerMask ReflectionLayers
        {
            get
            {
                return reflectionLayers;
            }
            set
            {
                reflectionLayers = value;
            }
        }

        [SerializeField]
        private bool reflectCustomSkybox;
        public bool ReflectCustomSkybox
        {
            get
            {
                return reflectCustomSkybox;
            }
            set
            {
                reflectCustomSkybox = value;
            }
        }

        [SerializeField]
        private float reflectionDistortionStrength;
        public float ReflectionDistortionStrength
        {
            get
            {
                return reflectionDistortionStrength;
            }
            set
            {
                reflectionDistortionStrength = value;
            }
        }

        [SerializeField]
        private bool enableRefraction;
        public bool EnableRefraction
        {
            get
            {
                return enableRefraction;
            }
            set
            {
                enableRefraction = value;
            }
        }

        [SerializeField]
        private int refractionTextureResolution;
        public int RefractionTextureResolution
        {
            get
            {
                return refractionTextureResolution;
            }
            set
            {
                refractionTextureResolution = Mathf.Clamp(Mathf.ClosestPowerOfTwo(value), 32, 2048);
            }
        }

        [SerializeField]
        private bool enableRefractionPixelLight;
        public bool EnableRefractionPixelLight
        {
            get
            {
                return enableRefractionPixelLight;
            }
            set
            {
                enableRefractionPixelLight = value;
            }
        }

        [SerializeField]
        private float refractionClipPlaneOffset;
        public float RefractionClipPlaneOffset
        {
            get
            {
                return refractionClipPlaneOffset;
            }
            set
            {
                refractionClipPlaneOffset = value;
            }
        }

        [SerializeField]
        private LayerMask refractionLayers;
        public LayerMask RefractionLayers
        {
            get
            {
                return refractionLayers;
            }
            set
            {
                refractionLayers = value;
            }
        }

        [SerializeField]
        private float refractionDistortionStrength;
        public float RefractionDistortionStrength
        {
            get
            {
                return refractionDistortionStrength;
            }
            set
            {
                refractionDistortionStrength = value;
            }
        }

        [SerializeField]
        private bool enableCaustic;
        public bool EnableCaustic
        {
            get
            {
                return enableCaustic;
            }
            set
            {
                enableCaustic = value;
            }
        }

        [SerializeField]
        private Texture causticTexture;
        public Texture CausticTexture
        {
            get
            {
                return causticTexture;
            }
            set
            {
                causticTexture = value;
            }
        }

        [SerializeField]
        private float causticSize;
        public float CausticSize
        {
            get
            {
                return causticSize;
            }
            set
            {
                causticSize = value;
            }
        }

        [SerializeField]
        private float causticStrength;
        public float CausticStrength
        {
            get
            {
                return causticStrength;
            }
            set
            {
                causticStrength = Mathf.Clamp01(value);
            }
        }

        [SerializeField]
        private float causticDistortionStrength;
        public float CausticDistortionStrength
        {
            get
            {
                return causticDistortionStrength;
            }
            set
            {
                causticDistortionStrength = value;
            }
        }

        public void Reset()
        {
            PWaterProfile defaultProfile = PPoseidonSettings.Instance.CalmWaterProfile;
            if (defaultProfile != null)
            {
                CopyFrom(defaultProfile);
            }
        }

        public void GenerateMesh()
        {
            IPMeshCreator meshCreator = null;
            if (Pattern == PPlaneMeshPattern.Hexagon)
            {
                meshCreator = new PHexMeshCreator();
            }
            else if (Pattern == PPlaneMeshPattern.Diamond)
            {
                meshCreator = new PDiamondMeshCreator();
            }
            else if (Pattern == PPlaneMeshPattern.Quad)
            {
                meshCreator = new PQuadMeshCreator();
            }
            else
            {
                meshCreator = new PHexMeshCreator();
            }
            meshCreator.Create(this);
        }

        private void UpdateMaterialProperties(Material mat)
        {
            if (mat == null)
                return;
            PMat.SetActiveMaterial(mat);

            if (EnableReflection || EnableRefraction)
            {
                Shader shader =
                    PCommon.CurrentRenderPipeline == PRenderPipelineType.Universal ? PPoseidonSettings.Instance.WaterAdvancedURPShader :
                    PCommon.CurrentRenderPipeline == PRenderPipelineType.Lightweight ? PPoseidonSettings.Instance.WaterAdvancedLWRPShader :
                    PPoseidonSettings.Instance.WaterAdvancedShader;
                PMat.SetShader(shader);
            }
            else
            {
                Shader shader =
                    PCommon.CurrentRenderPipeline == PRenderPipelineType.Universal ? PPoseidonSettings.Instance.WaterBasicURPShader :
                    PCommon.CurrentRenderPipeline == PRenderPipelineType.Lightweight ? PPoseidonSettings.Instance.WaterBasicLWRPShader :
                    PPoseidonSettings.Instance.WaterBasicShader;
                PMat.SetShader(shader);
            }

            PMat.SetKeywordEnable(PMat.KW_MESH_NOISE, meshNoise != 0);
            PMat.SetFloat(PMat.MESH_NOISE, meshNoise);

            PMat.SetKeywordEnable(PMat.KW_LIGHTING_PHYSICAL_BASED, lightingModel == PLightingModel.PhysicalBased);
            PMat.SetKeywordEnable(PMat.KW_LIGHTING_BLINN_PHONG, lightingModel == PLightingModel.BlinnPhong);
            PMat.SetKeywordEnable(PMat.KW_LIGHTING_LAMBERT, lightingModel == PLightingModel.Lambert);
            PMat.SetColor(PMat.COLOR, color);
            PMat.SetColor(PMat.SPEC_COLOR, specColor);
            PMat.SetFloat(PMat.SMOOTHNESS, smoothness);

            PMat.SetKeywordEnable(PMat.KW_LIGHT_ABSORPTION, enableLightAbsorption);
            PMat.SetColor(PMat.DEPTH_COLOR, depthColor);
            PMat.SetFloat(PMat.MAX_DEPTH, maxDepth);

            PMat.SetKeywordEnable(PMat.KW_FOAM, enableFoam);
            PMat.SetKeywordEnable(PMat.KW_FOAM_HQ, enableFoamHQ);
            PMat.SetColor(PMat.FOAM_COLOR, foamColor);
            PMat.SetFloat(PMat.FOAM_DISTANCE, foamDistance);
            PMat.SetFloat(PMat.FOAM_NOISE_SCALE_HQ, foamNoiseScaleHQ);
            PMat.SetFloat(PMat.FOAM_NOISE_SPEED_HQ, foamNoiseSpeedHQ);

            PMat.SetFloat(PMat.RIPPLE_HEIGHT, rippleHeight);
            PMat.SetFloat(PMat.RIPPLE_NOISE_SCALE, rippleNoiseScale);
            PMat.SetFloat(PMat.RIPPLE_SPEED, rippleSpeed);

            PMat.SetFloat(PMat.FRESNEL_STRENGTH, fresnelStrength);
            PMat.SetFloat(PMat.FRESNEL_BIAS, fresnelBias);

            PMat.SetKeywordEnable(PMat.KW_REFLECTION, enableReflection);
            PMat.SetKeywordEnable(PMat.KW_REFLECTION_BLUR, enableReflectionBlur);
            PMat.SetFloat(PMat.REFLECTION_DISTORTION_STRENGTH, reflectionDistortionStrength);

            PMat.SetKeywordEnable(PMat.KW_REFRACTION, enableRefraction);
            PMat.SetFloat(PMat.REFRACTION_DISTORTION_STRENGTH, refractionDistortionStrength);

            if (enableLightAbsorption && enableRefraction)
            {
                PMat.SetKeywordEnable(PMat.KW_CAUSTIC, enableCaustic);
            }
            else
            {
                PMat.SetKeywordEnable(PMat.KW_CAUSTIC, false);
            }
            PMat.SetTexture(PMat.CAUSTIC_TEX, causticTexture);
            PMat.SetFloat(PMat.CAUSTIC_SIZE, causticSize);
            PMat.SetFloat(PMat.CAUSTIC_STRENGTH, causticStrength);
            PMat.SetFloat(PMat.CAUSTIC_DISTORTION_STRENGTH, causticDistortionStrength);

            PMat.SetActiveMaterial(null);
        }

        public void UpdateMaterialProperties()
        {
            UpdateMaterialProperties(Material);
        }

        public void CopyFrom(PWaterProfile p)
        {
            WaterMode = p.WaterMode;
            MeshResolution = p.MeshResolution;
            MeshNoise = p.MeshNoise;

            LightingModel = p.LightingModel;
            Color = p.Color;
            SpecColor = p.SpecColor;
            Smoothness = p.Smoothness;

            EnableLightAbsorption = p.EnableLightAbsorption;
            DepthColor = p.DepthColor;
            MaxDepth = p.MaxDepth;

            EnableFoam = p.EnableFoam;
            FoamColor = p.FoamColor;
            FoamDistance = p.FoamDistance;
            EnableFoamHQ = p.EnableFoamHQ;
            FoamNoiseScaleHQ = p.FoamNoiseScaleHQ;
            FoamNoiseSpeedHQ = p.FoamNoiseSpeedHQ;

            EnableRipple = p.EnableRipple;
            RippleHeight = p.RippleHeight;
            RippleSpeed = p.RippleSpeed;
            RippleNoiseScale = p.RippleNoiseScale;

            FresnelStrength = p.FresnelStrength;
            FresnelBias = p.FresnelBias;

            EnableReflection = p.EnableReflection;
            ReflectionTextureResolution = p.ReflectionTextureResolution;
            EnableReflectionPixelLight = p.EnableReflectionPixelLight;
            EnableReflectionBlur = p.EnableReflectionBlur;
            ReflectionClipPlaneOffset = p.ReflectionClipPlaneOffset;
            ReflectionLayers = p.ReflectionLayers;
            ReflectCustomSkybox = p.ReflectCustomSkybox;
            ReflectionDistortionStrength = p.ReflectionDistortionStrength;

            EnableRefraction = p.EnableRefraction;
            RefractionTextureResolution = p.RefractionTextureResolution;
            EnableRefractionPixelLight = p.EnableRefractionPixelLight;
            RefractionLayers = p.RefractionLayers;
            RefractionDistortionStrength = p.RefractionDistortionStrength;

            EnableCaustic = p.EnableCaustic;
            CausticTexture = p.CausticTexture;
            CausticSize = p.CausticSize;
            CausticStrength = p.CausticStrength;
            CausticDistortionStrength = p.CausticDistortionStrength;

            UpdateMaterialProperties();
        }
    }
}
