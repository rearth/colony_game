using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Pinwheel.Poseidon
{
    //[CreateAssetMenu(menuName = "Poseidon/Settings")]
    public class PPoseidonSettings : ScriptableObject
    {
        private static PPoseidonSettings instance;
        public static PPoseidonSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = Resources.Load<PPoseidonSettings>("PoseidonSettings");
                    if (instance == null)
                    {
                        instance = ScriptableObject.CreateInstance<PPoseidonSettings>();
                    }
                }
                return instance;
            }
        }

        [SerializeField]
        private PWaterProfile calmWaterProfile;
        public PWaterProfile CalmWaterProfile
        {
            get
            {
                return calmWaterProfile;
            }
            set
            {
                calmWaterProfile = value;
            }
        }

        [SerializeField]
        private PWaterProfile calmWaterHQProfile;
        public PWaterProfile CalmWaterHQProfile
        {
            get
            {
                return calmWaterHQProfile;
            }
            set
            {
                calmWaterHQProfile = value;
            }
        }

        [SerializeField]
        private Texture2D noiseTexture;
        public Texture2D NoiseTexture
        {
            get
            {
                return noiseTexture;
            }
            set
            {
                noiseTexture = value;
            }
        }

        [SerializeField]
        private Texture2D defaultNormalMap;
        public Texture2D DefaultNormalMap
        {
            get
            {
                return defaultNormalMap;
            }
            set
            {
                defaultNormalMap = value;
            }
        }

        [SerializeField]
        private Texture2D defaultUnderwaterDistortionMap;
        public Texture2D DefaultUnderwaterDistortionMap
        {
            get
            {
                return defaultUnderwaterDistortionMap;
            }
            set
            {
                defaultUnderwaterDistortionMap = value;
            }
        }

        [SerializeField]
        private Texture2D defaultWetLensDistortionMap;
        public Texture2D DefaultWetLensDistortionMap
        {
            get
            {
                return defaultWetLensDistortionMap;
            }
            set
            {
                defaultWetLensDistortionMap = value;
            }
        }

//#if UNITY_EDITOR
//        [UnityEditor.MenuItem("Window/Poseidon/Reset shader ref")]
//        public static void ResetShaderRef()
//        {
//            PPoseidonSettings.Instance.WaterBasicShader = null;
//        }
//#endif

        private Shader waterBasicShader;
        public Shader WaterBasicShader
        {
            get
            {
                if (waterBasicShader == null)
                {
                    waterBasicShader = Shader.Find("Poseidon/Default/WaterBasic");
                }
                return waterBasicShader;
            }
            set
            {
                waterBasicShader = value;
            }
        }

        private Shader waterAdvancedShader;
        public Shader WaterAdvancedShader
        {
            get
            {
                if (waterAdvancedShader == null)
                {
                    waterAdvancedShader = Shader.Find("Poseidon/Default/WaterAdvanced");
                }
                return waterAdvancedShader;
            }
            set
            {
                waterAdvancedShader = value;
            }
        }

        private Shader waterBasicLWRPShader;
        public Shader WaterBasicLWRPShader
        {
            get
            {
                if (waterBasicLWRPShader == null)
                {
                    waterBasicLWRPShader = Shader.Find("Poseidon/LWRP/WaterBasicLWRP");
                }
                return waterBasicLWRPShader;
            }
            set
            {
                waterBasicLWRPShader = value;
            }
        }

        private Shader waterAdvancedLWRPShader;
        public Shader WaterAdvancedLWRPShader
        {
            get
            {
                if (waterAdvancedLWRPShader == null)
                {
                    waterAdvancedLWRPShader = Shader.Find("Poseidon/LWRP/WaterAdvancedLWRP");
                }
                return waterAdvancedLWRPShader;
            }
            set
            {
                waterAdvancedLWRPShader = value;
            }
        }

        private Shader waterBasicURPShader;
        public Shader WaterBasicURPShader
        {
            get
            {
                if (waterBasicURPShader == null)
                {
                    waterBasicURPShader = Shader.Find("Poseidon/URP/WaterBasicURP");
                }
                return waterBasicURPShader;
            }
            set
            {
                waterBasicURPShader = value;
            }
        }

        private Shader waterAdvancedURPShader;
        public Shader WaterAdvancedURPShader
        {
            get
            {
                if (waterAdvancedURPShader == null)
                {
                    waterAdvancedURPShader = Shader.Find("Poseidon/URP/WaterAdvancedURP");
                }
                return waterAdvancedURPShader;
            }
            set
            {
                waterAdvancedURPShader = value;
            }
        }

        [SerializeField]
        private PInternalShaderSettings internalShaders;
        public PInternalShaderSettings InternalShaders
        {
            get
            {
                return internalShaders;
            }
            set
            {
                internalShaders = value;
            }
        }

    }
}
