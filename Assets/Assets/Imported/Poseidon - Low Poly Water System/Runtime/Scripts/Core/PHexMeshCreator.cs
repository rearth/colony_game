using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pinwheel.Poseidon;

namespace Pinwheel.Poseidon
{
    public class PHexMeshCreator : IPMeshCreator
    {
        private Vector2[,] grid;
        private PWaterProfile profile;

        public void Create(PWaterProfile profile)
        {
            this.profile = profile;
            Init();
            GenerateGrid();
            UpdateMesh();
        }

        private void Init()
        {
            int resolution = profile.MeshResolution;
            int length = resolution + 1;
            grid = new Vector2[length, length];
        }

        private void GenerateGrid()
        {
            int length = grid.GetLength(0);
            int width = grid.GetLength(1);

            Vector2 p = Vector2.zero;
            for (int z = 0; z < length; ++z)
            {
                for (int x = 0; x < width; ++x)
                {
                    p.Set(
                        Mathf.InverseLerp(0, width - 1, x),
                        Mathf.InverseLerp(0, length - 1, z));
                    grid[z, x] = p;
                }
            }
        }

        private void UpdateMesh()
        {
            //vertices
            int length = grid.GetLength(0);
            int width = grid.GetLength(1);
            List<Vector3> vertices = new List<Vector3>();
            List<int> triangles = new List<int>();
            List<Vector4> uvs0 = new List<Vector4>(); //contain neighbor vertex position, for normal re-construction
            List<Color> colors = new List<Color>(); //contain neighbor vertex position, for normal re-construction

            Vector4 bl = Vector4.zero;
            Vector4 tl = Vector4.zero;
            Vector4 tr = Vector4.zero;
            Vector4 br = Vector4.zero;
            Vector4 hexOffset = new Vector4(-0.5f / width, 0, 0, 0);
            for (int z = 0; z < length - 1; ++z)
            {
                for (int x = 0; x < width - 1; ++x)
                {
                    int lastIndex = vertices.Count;
                    triangles.Add(lastIndex + 0);
                    triangles.Add(lastIndex + 1);
                    triangles.Add(lastIndex + 2);
                    triangles.Add(lastIndex + 3);
                    triangles.Add(lastIndex + 4);
                    triangles.Add(lastIndex + 5);

                    bl.Set(Mathf.InverseLerp(0, width - 1, x), 0, Mathf.InverseLerp(0, length - 1, z), 0);
                    tl.Set(Mathf.InverseLerp(0, width - 1, x), 0, Mathf.InverseLerp(0, length - 1, z + 1), 0);
                    tr.Set(Mathf.InverseLerp(0, width - 1, x + 1), 0, Mathf.InverseLerp(0, length - 1, z + 1), 0);
                    br.Set(Mathf.InverseLerp(0, width - 1, x + 1), 0, Mathf.InverseLerp(0, length - 1, z), 0);

                    if (z % 2 == 0)
                    {
                        vertices.Add(bl); /*============*/ uvs0.Add(tl + hexOffset); /**/ colors.Add(tr + hexOffset);
                        vertices.Add(tl + hexOffset); /**/ uvs0.Add(tr + hexOffset); /**/ colors.Add(bl);
                        vertices.Add(tr + hexOffset); /**/ uvs0.Add(bl); /*============*/ colors.Add(tl + hexOffset);

                        vertices.Add(bl); /*============*/ uvs0.Add(tr + hexOffset); /**/ colors.Add(br);
                        vertices.Add(tr + hexOffset); /**/ uvs0.Add(br); /*============*/ colors.Add(bl);
                        vertices.Add(br); /*============*/ uvs0.Add(bl); /*============*/ colors.Add(tr + hexOffset);
                    }
                    else
                    {
                        vertices.Add(bl + hexOffset); /**/ uvs0.Add(tl); /*============*/ colors.Add(br + hexOffset);
                        vertices.Add(tl); /*============*/ uvs0.Add(br + hexOffset); /**/ colors.Add(bl + hexOffset);
                        vertices.Add(br + hexOffset); /**/ uvs0.Add(bl + hexOffset); /**/ colors.Add(tl);

                        vertices.Add(tr); /*============*/ uvs0.Add(br + hexOffset); /**/ colors.Add(tl);
                        vertices.Add(br + hexOffset); /**/ uvs0.Add(tl); /*============*/ colors.Add(tr);
                        vertices.Add(tl); /*============*/ uvs0.Add(tr); /*============*/ colors.Add(br + hexOffset);
                    }
                }
            }

            Mesh m = profile.Mesh;
            m.Clear();
            m.SetVertices(vertices);
            m.SetTriangles(triangles, 0);
            m.SetUVs(0, uvs0);
            m.SetColors(colors);

            //m.RecalculateBounds();
            //m.RecalculateNormals();
            //m.RecalculateTangents();
            m.name = "Water Mesh";
            

            Bounds bounds = m.bounds;
            bounds.extents = new Vector3(bounds.extents.x, (bounds.extents.x + bounds.extents.z) * 0.5f, bounds.extents.z);
            m.bounds = bounds;
        }
    }
}
