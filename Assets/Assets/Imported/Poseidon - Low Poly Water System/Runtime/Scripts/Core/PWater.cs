using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
using System.Collections.Generic;
#if POSEIDON_LWRP
using UnityEngine.Rendering.LWRP;
#elif POSEIDON_URP
using UnityEngine.Rendering.Universal;
#endif

namespace Pinwheel.Poseidon
{
    [ExecuteInEditMode]
    public class PWater : MonoBehaviour
    {
        [SerializeField]
        private PWaterProfile profile;
        public PWaterProfile Profile
        {
            get
            {
                return profile;
            }
            set
            {
                profile = value;
            }
        }

        [SerializeField]
        private Vector2 tileSize;
        public Vector2 TileSize
        {
            get
            {
                return tileSize;
            }
            set
            {
                tileSize = new Vector2(Mathf.Max(1, value.x), Mathf.Max(1, value.y));
            }
        }

        [SerializeField]
        private List<PIndex2D> tileIndices;
        public List<PIndex2D> TileIndices
        {
            get
            {
                if (tileIndices == null)
                {
                    tileIndices = new List<PIndex2D>();
                }
                if (tileIndices.Count == 0)
                {
                    tileIndices.Add(new PIndex2D(0, 0));
                }
                return tileIndices;
            }
            set
            {
                tileIndices = value;
            }
        }

        private Dictionary<Camera, RenderTexture> reflRenderTextures;
        private Dictionary<Camera, RenderTexture> ReflRenderTextures
        {
            get
            {
                if (reflRenderTextures == null)
                {
                    reflRenderTextures = new Dictionary<Camera, RenderTexture>();
                }
                return reflRenderTextures;
            }
        }

        private Dictionary<Camera, Camera> reflCameras;
        private Dictionary<Camera, Camera> ReflCameras
        {
            get
            {
                if (reflCameras == null)
                {
                    reflCameras = new Dictionary<Camera, Camera>();
                }
                return reflCameras;
            }
        }

        private Dictionary<Camera, RenderTexture> refrRenderTextures;
        private Dictionary<Camera, RenderTexture> RefrRenderTextures
        {
            get
            {
                if (refrRenderTextures == null)
                {
                    refrRenderTextures = new Dictionary<Camera, RenderTexture>();
                }
                return refrRenderTextures;
            }
        }

        private Dictionary<Camera, Camera> refrCameras;
        private Dictionary<Camera, Camera> RefrCameras
        {
            get
            {
                if (refrCameras == null)
                {
                    refrCameras = new Dictionary<Camera, Camera>();
                }
                return refrCameras;
            }
        }

        private MaterialPropertyBlock materialProperties;
        private MaterialPropertyBlock MaterialProperties
        {
            get
            {
                if (materialProperties == null)
                {
                    materialProperties = new MaterialPropertyBlock();
                }
                return materialProperties;
            }
        }

        private List<GameObject> obsoletedGameObject;
        private List<GameObject> ObsoletedGameObject
        {
            get
            {
                if (obsoletedGameObject == null)
                {
                    obsoletedGameObject = new List<GameObject>();
                }
                return obsoletedGameObject;
            }
        }

        [SerializeField]
        private MeshFilter meshFilterComponent;
        private MeshFilter MeshFilterComponent
        {
            get
            {
                if (meshFilterComponent == null)
                {
                    meshFilterComponent = PUtilities.GetOrAddComponent<MeshFilter>(gameObject);
                }
                meshFilterComponent.hideFlags = HideFlags.HideInInspector;
                return meshFilterComponent;
            }
        }

        [SerializeField]
        private MeshRenderer meshRendererComponent;
        private MeshRenderer MeshRendererComponent
        {
            get
            {
                if (meshRendererComponent == null)
                {
                    meshRendererComponent = PUtilities.GetOrAddComponent<MeshRenderer>(gameObject);
                }
                meshRendererComponent.hideFlags = HideFlags.HideInInspector;
                return meshRendererComponent;
            }
        }

        private Mesh emptyMesh;
        public Mesh EmptyMesh
        {
            get
            {
                if (emptyMesh == null)
                {
                    emptyMesh = new Mesh();
                }
                return emptyMesh;
            }
        }

        private Material[] emptyMaterials;
        public Material[] EmptyMaterials
        {
            get
            {
                if (emptyMaterials == null)
                {
                    emptyMaterials = new Material[0];
                }
                return emptyMaterials;
            }
        }

        [SerializeField]
        private Bounds bounds;
        public Bounds Bounds
        {
            get
            {
                return bounds;
            }
            set
            {
                bounds = value;
                EmptyMesh.bounds = bounds;
            }
        }

        private void Reset()
        {
            TileSize = new Vector2(50, 50);
        }

        private void OnEnable()
        {
            Camera.onPreCull += OnPreCullCamera;

#if POSEIDON_LWRP || POSEIDON_URP
            RenderPipelineManager.beginCameraRendering += OnBeginCameraRenderingSRP;
#endif

            foreach (Transform child in transform)
            {
                if (child.name.StartsWith("~ReflectionCamera") || child.name.StartsWith("~RefractionCamera"))
                {
                    ObsoletedGameObject.Add(child.gameObject);
                }
            }

            ReCalculateBounds();
            if (Profile != null)
            {
                Profile.UpdateMaterialProperties();
            }
        }

        private void OnDisable()
        {
            Camera.onPreCull -= OnPreCullCamera;

#if POSEIDON_LWRP || POSEIDON_URP
            RenderPipelineManager.beginCameraRendering -= OnBeginCameraRenderingSRP;
#endif
        }

        private void OnDestroy()
        {
            CleanUp();
        }

        private void CleanUp()
        {
            foreach (RenderTexture rt in ReflRenderTextures.Values)
            {
                if (rt == null)
                    continue;
                rt.Release();
                PUtilities.DestroyObject(rt);
            }

            foreach (RenderTexture rt in RefrRenderTextures.Values)
            {
                if (rt == null)
                    continue;
                rt.Release();
                PUtilities.DestroyObject(rt);
            }

            foreach (Camera cam in ReflCameras.Values)
            {
                if (cam == null)
                    continue;
                PUtilities.DestroyGameobject(cam.gameObject);
            }

            foreach (Camera cam in RefrCameras.Values)
            {
                if (cam == null)
                    continue;
                PUtilities.DestroyGameobject(cam.gameObject);
            }
        }

        private void Update()
        {
            for (int i = 0; i < ObsoletedGameObject.Count; ++i)
            {
                GameObject o = ObsoletedGameObject[i];
                if (o != null)
                {
                    PUtilities.DestroyGameobject(o);
                }
            }
            ObsoletedGameObject.Clear();

            SetUpSelfLayer();
            SetUpDummyComponents();
        }

        private void SetUpSelfLayer()
        {
            int waterLayer = LayerMask.NameToLayer("Water");
            if (gameObject.layer != waterLayer)
            {
                gameObject.layer = LayerMask.NameToLayer("Water");
                string msg = string.Format("Game object '{0}' must be in 'Water' layer!", gameObject.name);
                Debug.Log(msg);
            }
        }

        private void SetUpDummyComponents()
        {
            if (Profile == null)
            {
                MeshRendererComponent.enabled = false;
            }
            else
            {
                MeshFilterComponent.sharedMesh = EmptyMesh;
                MeshRendererComponent.sharedMaterials = EmptyMaterials;
                MeshRendererComponent.shadowCastingMode = ShadowCastingMode.Off;
                MeshRendererComponent.receiveShadows = false;
                MeshRendererComponent.enabled = true;
            }
        }

        public void ReCalculateBounds()
        {
            int minX = int.MaxValue;
            int minZ = int.MaxValue;
            int maxX = int.MinValue;
            int maxZ = int.MinValue;

            for (int i = 0; i < TileIndices.Count; ++i)
            {
                PIndex2D index = TileIndices[i];
                minX = Mathf.Min(minX, index.X);
                minZ = Mathf.Min(minZ, index.Z);
                maxX = Mathf.Max(maxX, index.X);
                maxZ = Mathf.Max(maxZ, index.Z);
            }

            float width = (maxX - minX + 1) * TileSize.x;
            float length = (maxZ - minZ + 1) * TileSize.y;
            float height = 0;

            float centerX = Mathf.Lerp(minX, maxX + 1, 0.5f) * TileSize.x;
            float centerZ = Mathf.Lerp(minZ, maxZ + 1, 0.5f) * TileSize.y;
            float centerY = 0;

            Bounds = new Bounds(
                new Vector3(centerX, centerY, centerZ),
                new Vector3(width, height, length));
        }

        private void OnPreCullCamera(Camera cam)
        {
            ValidateMaterial();
            SubmitRenderList(cam);
        }

        private void SubmitRenderList(Camera cam)
        {
            if (cam.cameraType != CameraType.Game && cam.cameraType != CameraType.SceneView)
                return;
            if (cam.name.StartsWith("~"))
                return;
            if (Profile == null)
                return;
            transform.rotation = Quaternion.identity;

            if (Profile.EnableLightAbsorption || Profile.EnableFoam)
            {
                cam.depthTextureMode = DepthTextureMode.Depth;
#if POSEIDON_LWRP
                if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Lightweight)
                {
                    LightweightRenderPipelineAsset lwAsset = LightweightRenderPipeline.asset;
                    if (lwAsset.supportsCameraDepthTexture == false)
                    {
                        lwAsset.supportsCameraDepthTexture = true;
                        Debug.Log("Camera Depth Texture is required for Light Absorption and Foam to work!");
                    }
                    if (lwAsset.supportsCameraOpaqueTexture == false)
                    {
                        lwAsset.supportsCameraOpaqueTexture = true;
                        Debug.Log("Camera Opaque Texture is required for Light Absorption, Foam and Refraction to work!");
                    }
                    if (cam.stereoEnabled && lwAsset.msaaSampleCount < 2)
                    {
                        lwAsset.msaaSampleCount = 2;
                        Debug.Log("MSAA is required for Light Absorption, Foam and Refraction to work in VR!");
                    }
                }
#elif POSEIDON_URP
                if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Universal)
                {
                    UniversalRenderPipelineAsset uAsset = UniversalRenderPipeline.asset;
                    if (uAsset.supportsCameraDepthTexture == false)
                    {
                        uAsset.supportsCameraDepthTexture = true;
                        Debug.Log("Camera Depth Texture is required for Light Absorption and Foam to work!");
                    }
                    if (uAsset.supportsCameraOpaqueTexture == false)
                    {
                        uAsset.supportsCameraOpaqueTexture = true;
                        Debug.Log("Camera Opaque Texture is required for Light Absorption, Foam and Refraction to work!");
                    }
                    if (cam.stereoEnabled && uAsset.msaaSampleCount < 2)
                    {
                        uAsset.msaaSampleCount = 2;
                        Debug.Log("MSAA is required for Light Absorption, Foam and Refraction to work in VR!");
                    }
                }
#endif
            }

            bool isBackface = cam.transform.position.y < transform.position.y;
            if (isBackface && !Profile.ShouldRenderBackface)
                return;

            MaterialProperties.Clear();
            if (Profile.EnableReflection)
            {
                MaterialProperties.SetTexture(PMat.REFLECTION_TEX, GetReflectionRt(cam));
            }
            if (Profile.EnableRefraction && PCommon.CurrentRenderPipeline == PRenderPipelineType.Builtin)
            {
                MaterialProperties.SetTexture(PMat.REFRACTION_TEX, GetRefractionRt(cam));
            }
            MaterialProperties.SetTexture(PMat.NOISE_TEX, PPoseidonSettings.Instance.NoiseTexture);

            PMat.SetActiveMaterial(Profile.Material);
            PMat.SetKeywordEnable(PMat.KW_BACK_FACE, isBackface);
            PMat.SetActiveMaterial(null);

            for (int i = 0; i < TileIndices.Count; ++i)
            {
                PIndex2D index = TileIndices[i];
                Vector3 pos = transform.TransformPoint(new Vector3(index.X * TileSize.x, 0, index.Z * TileSize.y));
                Quaternion rotation = transform.rotation;
                Vector3 scale = transform.TransformVector(new Vector3(TileSize.x, transform.localScale.y, TileSize.y));

                Graphics.DrawMesh(
                    Profile.Mesh,
                    Matrix4x4.TRS(pos, rotation, scale),
                    Profile.Material,
                    gameObject.layer,
                    cam,
                    0,
                    MaterialProperties,
                    ShadowCastingMode.Off,
                    false,
                    null,
                    LightProbeUsage.BlendProbes,
                    null);
            }
        }

        private RenderTexture GetReflectionRt(Camera cam)
        {
            if (!ReflRenderTextures.ContainsKey(cam))
            {
                ReflRenderTextures.Add(cam, null);
            }

            int resolution = 128;
            if (Profile != null)
            {
                resolution = Profile.ReflectionTextureResolution;
            }
            RenderTexture rt = ReflRenderTextures[cam];
            if (rt == null)
            {
                rt = new RenderTexture(resolution, resolution, 16, RenderTextureFormat.ARGB32);
            }
            if (rt.width != resolution || rt.height != resolution)
            {
                Camera reflCam;
                if (ReflCameras.TryGetValue(cam, out reflCam))
                {
                    reflCam.targetTexture = null;
                }

                rt.Release();
                PUtilities.DestroyObject(rt);
                rt = new RenderTexture(resolution, resolution, 16, RenderTextureFormat.ARGB32);
            }
            rt.name = string.Format("~ReflectionRt_{0}_{1}", cam.name, resolution);
            ReflRenderTextures[cam] = rt;

            if (cam.stereoEnabled)
                rt.Release();

            return rt;
        }

        private RenderTexture GetRefractionRt(Camera cam)
        {
            if (!RefrRenderTextures.ContainsKey(cam))
            {
                RefrRenderTextures.Add(cam, null);
            }

            int resolution = 128;
            if (Profile != null)
            {
                resolution = Profile.RefractionTextureResolution;
            }
            RenderTexture rt = RefrRenderTextures[cam];
            if (rt == null)
            {
                rt = new RenderTexture(resolution, resolution, 16, RenderTextureFormat.ARGB32);
            }
            if (rt.width != resolution || rt.height != resolution)
            {
                Camera refrCam;
                if (RefrCameras.TryGetValue(cam, out refrCam))
                {
                    refrCam.targetTexture = null;
                }

                rt.Release();
                PUtilities.DestroyObject(rt);
                rt = new RenderTexture(resolution, resolution, 16, RenderTextureFormat.ARGB32);
            }
            rt.name = string.Format("~RefractionRt_{0}_{1}", cam.name, resolution);
            RefrRenderTextures[cam] = rt;

            if (cam.stereoEnabled)
                rt.Release();

            return rt;
        }

        private void OnWillRenderObject()
        {
            if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Lightweight ||
                PCommon.CurrentRenderPipeline == PRenderPipelineType.Universal)
                return;

            if (Profile == null)
                return;

            Camera currentCam = Camera.current;
            if (currentCam == null)
                return;
            if (currentCam.cameraType != CameraType.Game && currentCam.cameraType != CameraType.SceneView)
                return;
            if (currentCam.name.EndsWith("Preview Camera"))
                return;
            if (ReflCameras.ContainsValue(currentCam) || RefrCameras.ContainsValue(currentCam))
                return;

            if (Profile.EnableReflection)
            {
                RenderReflectionTexture(currentCam);
            }

            if (Profile.EnableRefraction)
            {
                RenderRefractionTexture(currentCam);
            }
        }

        private void RenderReflectionTexture(Camera cam)
        {
            bool isBackface = cam.transform.position.y < transform.position.y;
            if (isBackface && !Profile.ShouldRenderBackface)
                return;

            if (cam.stereoEnabled)
                return;

            //prepare reflection camera
            if (!ReflCameras.ContainsKey(cam))
            {
                ReflCameras.Add(cam, null);
            }
            if (ReflCameras[cam] == null)
            {
                GameObject g = new GameObject();
                g.name = "~ReflectionCamera_" + cam.name;
                g.hideFlags = HideFlags.HideAndDontSave;

                Camera rCam = g.AddComponent<Camera>();
                rCam.enabled = false;

                g.AddComponent<Skybox>();
                g.AddComponent<FlareLayer>();

                PUtilities.ResetTransform(g.transform, transform);
                ReflCameras[cam] = rCam;
            }

            //define reflection plane by position & normal in world space
            Vector3 planePos = transform.position;
            Vector3 planeNormal = Vector3.up;

            //disable pixel light if needed
            int oldPixelLightCount = QualitySettings.pixelLightCount;
            if (!Profile.EnableReflectionPixelLight)
            {
                QualitySettings.pixelLightCount = 0;
            }

            //set up camera and render
            Camera reflectionCamera = ReflCameras[cam];
            reflectionCamera.enabled = false;
            reflectionCamera.targetTexture = GetReflectionRt(cam);
            MatchCameraSettings(cam, reflectionCamera);

            // Reflect camera around reflection plane
            float d = -Vector3.Dot(planeNormal, planePos) - Profile.ReflectionClipPlaneOffset;
            Vector4 reflectionPlane = new Vector4(planeNormal.x, planeNormal.y, planeNormal.z, d);

            Matrix4x4 reflection = Matrix4x4.zero;
            CalculateReflectionMatrix(ref reflection, reflectionPlane);
            Vector3 oldpos = cam.transform.position;
            Vector3 newpos = reflection.MultiplyPoint(oldpos);
            reflectionCamera.worldToCameraMatrix = cam.worldToCameraMatrix * reflection;

            // Setup oblique projection matrix so that near plane is our reflection
            // plane. This way we clip everything below/above it for free.
            bool isBackFace = cam.transform.position.y < transform.position.y;
            Vector4 clipPlane = CameraSpacePlane(reflectionCamera, planePos, planeNormal, Profile.ReflectionClipPlaneOffset, isBackFace ? -1.0f : 1.0f);
            reflectionCamera.projectionMatrix = cam.CalculateObliqueMatrix(clipPlane);

            // Set custom culling matrix from the current camera
            reflectionCamera.cullingMatrix = cam.projectionMatrix * cam.worldToCameraMatrix;

            reflectionCamera.cullingMask = ~(1 << 4) & Profile.ReflectionLayers.value; // never render water layer
            //reflectionCamera.targetTexture = m_ReflectionTexture;
            bool oldCulling = GL.invertCulling;
            GL.invertCulling = !oldCulling;
            reflectionCamera.transform.position = newpos;
            Vector3 euler = cam.transform.eulerAngles;
            reflectionCamera.transform.eulerAngles = new Vector3(-euler.x, euler.y, euler.z);
            reflectionCamera.Render();

            reflectionCamera.transform.position = oldpos;
            GL.invertCulling = oldCulling;

            //restore pixel light
            if (!Profile.EnableReflectionPixelLight)
            {
                QualitySettings.pixelLightCount = oldPixelLightCount;
            }
        }

        private void RenderRefractionTexture(Camera cam)
        {
            if (cam.stereoEnabled)
                return;

            //prepare refraction camera
            if (!RefrCameras.ContainsKey(cam))
            {
                RefrCameras.Add(cam, null);
            }
            if (RefrCameras[cam] == null)
            {
                GameObject g = new GameObject();
                g.name = "~RefractionCamera_" + cam.name;
                g.hideFlags = HideFlags.HideAndDontSave;

                Camera rCam = g.AddComponent<Camera>();
                rCam.enabled = false;

                g.AddComponent<Skybox>();
                g.AddComponent<FlareLayer>();

                PUtilities.ResetTransform(g.transform, transform);
                RefrCameras[cam] = rCam;
            }


            //disable pixel light if needed
            int oldPixelLightCount = QualitySettings.pixelLightCount;
            if (!Profile.EnableReflectionPixelLight)
            {
                QualitySettings.pixelLightCount = 0;
            }

            Camera refractionCamera = RefrCameras[cam];
            refractionCamera.targetTexture = GetRefractionRt(cam);
            MatchCameraSettings(cam, refractionCamera);

            refractionCamera.worldToCameraMatrix = cam.worldToCameraMatrix;

            //define reflection plane by position & normal in world space
            Vector3 planePos = transform.position;
            Vector3 planeNormal = Vector3.up;

            // Setup oblique projection matrix so that near plane is our reflection
            // plane. This way we clip everything below/above it for free.
            bool isBackFace = cam.transform.position.y < transform.position.y;
            Vector4 clipPlane = CameraSpacePlane(refractionCamera, planePos, planeNormal, Profile.RefractionClipPlaneOffset, isBackFace ? 1.0f : -1.0f);
            refractionCamera.projectionMatrix = cam.CalculateObliqueMatrix(clipPlane);

            // Set custom culling matrix from the current camera
            refractionCamera.cullingMatrix = cam.projectionMatrix * cam.worldToCameraMatrix;
            refractionCamera.cullingMask = ~(1 << 4) & Profile.RefractionLayers.value; // never render water layer
            refractionCamera.transform.position = cam.transform.position;
            refractionCamera.transform.rotation = cam.transform.rotation;
            refractionCamera.Render();

            //restore pixel light
            if (!Profile.EnableReflectionPixelLight)
            {
                QualitySettings.pixelLightCount = oldPixelLightCount;
            }
        }

        private void MatchCameraSettings(Camera src, Camera dest)
        {
            if (dest == null)
            {
                return;
            }
            // set water camera to clear the same way as current camera
            dest.clearFlags = src.clearFlags;
            dest.backgroundColor = src.backgroundColor;
            if (src.clearFlags == CameraClearFlags.Skybox && Profile.ReflectCustomSkybox)
            {
                Skybox sky = src.GetComponent<Skybox>();
                Skybox mysky = dest.GetComponent<Skybox>();
                if (!sky || !sky.material)
                {
                    mysky.enabled = false;
                }
                else
                {
                    mysky.enabled = true;
                    mysky.material = sky.material;
                }
            }
            // update other values to match current camera.
            // even if we are supplying custom camera&projection matrices,
            // some of values are used elsewhere (e.g. skybox uses far plane)
            //dest.ResetWorldToCameraMatrix();
            dest.farClipPlane = src.farClipPlane;
            dest.nearClipPlane = src.nearClipPlane;
            dest.orthographic = src.orthographic;
            dest.fieldOfView = src.fieldOfView;
            dest.aspect = src.aspect;
            dest.orthographicSize = src.orthographicSize;
            dest.depthTextureMode = DepthTextureMode.None;
            dest.depth = float.MinValue;
            dest.stereoTargetEye = StereoTargetEyeMask.None;
            //dest.stereoSeparation = src.stereoSeparation;
            //dest.stereoConvergence = src.stereoConvergence;
            //dest.stereoTargetEye = src.stereoTargetEye;

            //if (src.stereoEnabled)
            //{
            //    if (src.stereoTargetEye == StereoTargetEyeMask.Left || src.stereoTargetEye == StereoTargetEyeMask.Both)
            //    {
            //        Vector3 eyePos = src.transform.TransformPoint(new Vector3(-0.5f * src.stereoSeparation, 0, 0));
            //        dest.transform.position = eyePos;

            //        Matrix4x4 projectionMatrix = src.GetStereoProjectionMatrix(Camera.StereoscopicEye.Left);
            //        dest.projectionMatrix = projectionMatrix;
            //    }
            //    else if (src.stereoTargetEye == StereoTargetEyeMask.Right)
            //    {
            //        Vector3 eyePos = src.transform.TransformPoint(new Vector3(0.5f * src.stereoSeparation, 0, 0));
            //        dest.transform.position = eyePos;

            //        Matrix4x4 projectionMatrix = src.GetStereoProjectionMatrix(Camera.StereoscopicEye.Right);
            //        dest.projectionMatrix = projectionMatrix;
            //    }
            //}
        }

        private static void CalculateReflectionMatrix(ref Matrix4x4 reflectionMat, Vector4 plane)
        {
            reflectionMat.m00 = (1F - 2F * plane[0] * plane[0]);
            reflectionMat.m01 = (-2F * plane[0] * plane[1]);
            reflectionMat.m02 = (-2F * plane[0] * plane[2]);
            reflectionMat.m03 = (-2F * plane[3] * plane[0]);

            reflectionMat.m10 = (-2F * plane[1] * plane[0]);
            reflectionMat.m11 = (1F - 2F * plane[1] * plane[1]);
            reflectionMat.m12 = (-2F * plane[1] * plane[2]);
            reflectionMat.m13 = (-2F * plane[3] * plane[1]);

            reflectionMat.m20 = (-2F * plane[2] * plane[0]);
            reflectionMat.m21 = (-2F * plane[2] * plane[1]);
            reflectionMat.m22 = (1F - 2F * plane[2] * plane[2]);
            reflectionMat.m23 = (-2F * plane[3] * plane[2]);

            reflectionMat.m30 = 0F;
            reflectionMat.m31 = 0F;
            reflectionMat.m32 = 0F;
            reflectionMat.m33 = 1F;
        }

        private static Vector4 CameraSpacePlane(Camera cam, Vector3 pos, Vector3 normal, float clipPlaneOffset, float sideSign)
        {
            Vector3 offsetPos = pos + normal * clipPlaneOffset;
            Matrix4x4 m = cam.worldToCameraMatrix;
            Vector3 cpos = m.MultiplyPoint(offsetPos);
            Vector3 cnormal = m.MultiplyVector(normal).normalized * sideSign;
            return new Vector4(cnormal.x, cnormal.y, cnormal.z, -Vector3.Dot(cpos, cnormal));
        }

        private void ValidateMaterial()
        {
            if (Profile == null)
                return;

            bool validate = true;
            if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Builtin)
            {
                validate =
                    Profile.Material.shader == PPoseidonSettings.Instance.WaterBasicShader ||
                    Profile.Material.shader == PPoseidonSettings.Instance.WaterAdvancedShader;
            }
            else if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Lightweight)
            {
                validate =
                    Profile.Material.shader == PPoseidonSettings.Instance.WaterBasicLWRPShader ||
                    Profile.Material.shader == PPoseidonSettings.Instance.WaterAdvancedLWRPShader;
            }
            else if (PCommon.CurrentRenderPipeline == PRenderPipelineType.Universal)
            {
                validate =
                    Profile.Material.shader == PPoseidonSettings.Instance.WaterBasicURPShader ||
                    Profile.Material.shader == PPoseidonSettings.Instance.WaterAdvancedURPShader;
            }

            if (!validate)
            {
                Profile.UpdateMaterialProperties();
            }
        }

#if POSEIDON_LWRP || POSEIDON_URP
        private void OnBeginCameraRenderingSRP(ScriptableRenderContext context, Camera cam)
        {
            ValidateMaterial();
            SubmitRenderList(cam);

            if (Profile == null)
                return;
            if (cam.cameraType != CameraType.Game && cam.cameraType != CameraType.SceneView)
                return;
            if (cam.name.EndsWith("Preview Camera"))
                return;
            if (ReflCameras.ContainsValue(cam))
            {
                //cam is a reflection camera
                return;
            }
            else if (RefrCameras.ContainsValue(cam))
            {
                //cam is a refraction camera
                return;
            }
            else
            {
                if (Profile.EnableReflection)
                {
                    RenderReflectionTextureSRP(context, cam);
                }
                if (Profile.EnableRefraction)
                {
                    //RenderRefractionTextureSRP(context, cam);
                }
            }
        }

        private void RenderReflectionTextureSRP(ScriptableRenderContext context, Camera cam)
        {
            bool isBackface = cam.transform.position.y < transform.position.y;
            if (isBackface && !Profile.ShouldRenderBackface)
                return;

            if (cam.stereoEnabled)
                return;

            //prepare reflection camera
            if (!ReflCameras.ContainsKey(cam))
            {
                ReflCameras.Add(cam, null);
            }
            if (ReflCameras[cam] == null)
            {
                GameObject g = new GameObject();
                g.name = "~ReflectionCamera_" + cam.name;
                //g.hideFlags = HideFlags.HideAndDontSave;

                Camera rCam = g.AddComponent<Camera>();
                rCam.enabled = false;

                g.AddComponent<Skybox>();
                g.AddComponent<FlareLayer>();

                PUtilities.ResetTransform(g.transform, transform);
                ReflCameras[cam] = rCam;
            }

            //define reflection plane by position & normal in world space
            Vector3 planePos = transform.position;
            Vector3 planeNormal = Vector3.up;

            int lastPixelLightCount = QualitySettings.pixelLightCount;
            if (!Profile.EnableReflectionPixelLight)
            {
                QualitySettings.pixelLightCount = 0;
            }

            bool lastInvertCulling = GL.invertCulling;
            GL.invertCulling = !lastInvertCulling;

            //set up camera and render
            Camera reflectionCamera = ReflCameras[cam];
            reflectionCamera.enabled = false;
            reflectionCamera.targetTexture = GetReflectionRt(cam);
            MatchCameraSettings(cam, reflectionCamera);

            // Reflect camera around reflection plane
            float d = -Vector3.Dot(planeNormal, planePos) - Profile.ReflectionClipPlaneOffset;
            Vector4 reflectionPlane = new Vector4(planeNormal.x, planeNormal.y, planeNormal.z, d);

            Matrix4x4 reflection = Matrix4x4.zero;
            CalculateReflectionMatrix(ref reflection, reflectionPlane);
            Vector3 oldpos = cam.transform.position;
            Vector3 newpos = reflection.MultiplyPoint(oldpos);
            reflectionCamera.worldToCameraMatrix = cam.worldToCameraMatrix * reflection;

            // Setup oblique projection matrix so that near plane is our reflection
            // plane. This way we clip everything below/above it for free.
            bool isBackFace = cam.transform.position.y < transform.position.y;
            Vector4 clipPlane = CameraSpacePlane(reflectionCamera, planePos, planeNormal, Profile.ReflectionClipPlaneOffset, isBackFace && Profile.ShouldRenderBackface ? -1.0f : 1.0f);

            reflectionCamera.projectionMatrix = cam.CalculateObliqueMatrix(clipPlane);

            // Set custom culling matrix from the current camera
            reflectionCamera.cullingMatrix = cam.projectionMatrix * cam.worldToCameraMatrix;

            reflectionCamera.cullingMask = ~(1 << 4) & Profile.ReflectionLayers.value; // never render water layer
            reflectionCamera.transform.position = newpos;
            Vector3 euler = cam.transform.eulerAngles;
            reflectionCamera.transform.eulerAngles = new Vector3(-euler.x, euler.y, euler.z + 180);

#if POSEIDON_URP
            UniversalRenderPipeline.RenderSingleCamera(context, reflectionCamera);
#elif POSEIDON_LWRP
            LightweightRenderPipeline.RenderSingleCamera(context, reflectionCamera);
#endif

            QualitySettings.pixelLightCount = lastPixelLightCount;
            GL.invertCulling = lastInvertCulling;
        }
#endif

        public PIndex2D WorldPointToTileIndex(Vector3 p)
        {
            Vector3 localPoint = transform.InverseTransformPoint(p);
            int x = Mathf.FloorToInt(localPoint.x / TileSize.x);
            int z = Mathf.FloorToInt(localPoint.z / TileSize.y);
            return new PIndex2D(x, z);
        }

        public bool CheckTilesContainPoint(Vector3 worldPoint)
        {
            PIndex2D index = WorldPointToTileIndex(worldPoint);
            return TileIndices.Contains(index);
        }
    }
}
