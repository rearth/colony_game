﻿Shader "Poseidon/Default/WaterBasic" 
{
	Properties
	{
		[HideInInspector] _MeshNoise ("Mesh Noise", Range(0.0, 1.0)) = 0

		[HideInInspector] _Color ("Color", Color) = (0.0, 0.8, 1.0, 0.5)
		[HideInInspector] _Specular ("Specular Color", Color) = (0.1, 0.1, 0.1, 1)
		[HideInInspector] _Smoothness ("Smoothness", Range(0.0, 1.0)) = 1
		
		[HideInInspector] _DepthColor ("Depth Color", Color) = (0.0, 0.45, 0.65, 0.85)
		[HideInInspector] _MaxDepth ("Max Depth", Float) = 5
		
		[HideInInspector] _FoamColor ("Foam Color", Color) = (1,1,1,1)
		[HideInInspector] _FoamDistance ("Foam Distance", Float) = 1.2
		[HideInInspector] _FoamNoiseScaleHQ ("Foam Noise Scale HQ", Float) = 3
		[HideInInspector] _FoamNoiseSpeedHQ ("Foam Noise Speed HQ", Float) = 1
		   
		[HideInInspector] _RippleHeight ("Ripple Height", Range(0 , 1)) = 0.1
		[HideInInspector] _RippleSpeed ("Ripple Speed", Float) = 5
		[HideInInspector] _RippleNoiseScale ("Ripple Noise Scale", Float) = 1

		[HideInInspector] _FresnelStrength ("Fresnel Strength", Range(0.0, 5.0)) = 1
		[HideInInspector] _FresnelBias ("Fresnel Bias", Range(0.0, 1.0)) = 0
	}

	SubShader
	{
		Tags{ "RenderType"="Transparent"  "Queue"="Transparent+0" "IgnoreProjector"="True" "ForceNoShadowCasting"="True" }
		Cull Off 

		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
	
		#pragma shader_feature_local MESH_NOISE
		#pragma shader_feature_local LIGHT_ABSORPTION
		#pragma shader_feature_local FOAM
		#pragma shader_feature_local FOAM_HQ
		#pragma shader_feature_local BACK_FACE

		#undef POSEIDON_WATER_ADVANCED
		#undef POSEIDON_SRP
		#include "./CGIncludes/PUniforms.cginc"
		#include "./CGIncludes/PMeshNoise.cginc"
		#include "./CGIncludes/PLightAbsorption.cginc"
		#include "./CGIncludes/PFoam.cginc"
		#include "./CGIncludes/PVertexAnimation.cginc"
		#include "./CGIncludes/PFresnel.cginc"
		#include "./CGIncludes/PCore.cginc"

		#pragma surface surfBasic StandardSpecular alpha:fade keepalpha nolightmap noinstancing vertex:vertexFunction finalcolor:finalColorFunction
		#pragma multi_compile_fog
 
		void surfBasic(Input i, inout SurfaceOutputStandardSpecular o)
		{ 
			half flipNormal = 1;
			#if BACK_FACE
				flipNormal = -1;
			#endif

			float3 worldNormal = UnityObjectToWorldNormal(i.normal);
			float fresnel;
			CalculateFresnelFactor(i.worldPos, flipNormal*worldNormal, _FresnelStrength, _FresnelBias, fresnel);

			float4 tintColor = _Color;
			#if LIGHT_ABSORPTION && !BACK_FACE
				float sceneDepth = GetSceneDepth(i.screenPos);
				float surfaceDepth = GetSurfaceDepth(float4(i.worldPos, 1));
				CalculateDeepWaterColor(sceneDepth, surfaceDepth, _Color, _DepthColor, _MaxDepth, tintColor); 
			#endif

			float4 waterColor = lerp(_Color, tintColor, fresnel);
			waterColor = saturate(waterColor);
			o.Albedo = waterColor.rgb;
			o.Specular = _Specular; 
			o.Alpha = waterColor.a;
			o.Smoothness = _Smoothness;
		}

		ENDCG
	}
	Fallback "Hidden/InternalErrorShader"
}
