﻿Shader "Poseidon/URP/WaterAdvancedURP"
{
    Properties
    {
		[HideInInspector] _MeshNoise ("Mesh Noise", Range(0.0, 1.0)) = 0

		[HideInInspector] _Color ("Color", Color) = (0.0, 0.8, 1.0, 0.5)
		[HideInInspector] _Specular ("Specular Color", Color) = (0.1, 0.1, 0.1, 1)
		[HideInInspector] _Smoothness ("Smoothness", Range(0.0, 1.0)) = 1
		
		[HideInInspector] _DepthColor ("Depth Color", Color) = (0.0, 0.45, 0.65, 0.85)
		[HideInInspector] _MaxDepth ("Max Depth", Float) = 5
		
		[HideInInspector] _FoamColor ("Foam Color", Color) = (1, 1, 1, 1)
		[HideInInspector] _FoamDistance ("Foam Distance", Float) = 1.2
		[HideInInspector] _FoamNoiseScaleHQ ("Foam Noise Scale HQ", Float) = 3
		[HideInInspector] _FoamNoiseSpeedHQ ("Foam Noise Speed HQ", Float) = 1
		   
		[HideInInspector] _RippleHeight ("Ripple Height", Range(0, 1)) = 0.1
		[HideInInspector] _RippleSpeed ("Ripple Speed", Float) = 5
		[HideInInspector] _RippleNoiseScale ("Ripple Noise Scale", Float) = 1
		
		[HideInInspector] _FresnelStrength ("Fresnel Strength", Range(0.0, 5.0)) = 1
		[HideInInspector] _FresnelBias ("Fresnel Bias", Range(0.0, 1.0)) = 0

		[HideInInspector] _ReflectionTex ("Reflection Texture", 2D) = "black" {}
		[HideInInspector] _ReflectionDistortionStrength ("Reflection Distortion Strength", Float) = 1

		[HideInInspector] _RefractionTex ("Refraction Texture", 2D) = "black" {}
		[HideInInspector] _RefractionDistortionStrength ("Refraction Distortion Strength", Float) = 1

		[HideInInspector] _CausticTex ("Caustic Texture", 2D) = "black" {}
		[HideInInspector] _CausticSize ("Caustic Size", Float) = 1
		[HideInInspector] _CausticStrength ("Caustic Strength", Range(0.0, 1.0)) = 1
		[HideInInspector] _CausticDistortionStrength ("Caustic Distortion Strength", Float) = 1
    }


    SubShader
    {
		
        Tags { "RenderPipeline"="UniversalPipeline" "RenderType"="Geometry" "Queue"="Transparent" }
		
		ZWrite Off
		ZTest LEqual
		Cull Off
		HLSLINCLUDE
		ENDHLSL
		
        Pass
        {
        	Tags { "LightMode"="UniversalForward" }

        	Name "Base"
            
        	HLSLPROGRAM
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0

            #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            #pragma multi_compile_fog

            #pragma vertex vertSRP
        	#pragma fragment fragAdvancedSRP

            #define _SPECULAR_SETUP 1
			#if LIGHT_ABSORPTION || FOAM || CAUSTIC
				#define REQUIRE_DEPTH_TEXTURE
			#endif
			#if REFRACTION
				#define REQUIRE_OPAQUE_TEXTURE
			#endif

			#pragma shader_feature_local MESH_NOISE
			#pragma shader_feature_local LIGHT_ABSORPTION
			#pragma shader_feature_local FOAM
			#pragma shader_feature_local FOAM_HQ
			#pragma shader_feature_local REFLECTION
			#pragma shader_feature_local REFLECTION_BLUR
			#pragma shader_feature_local REFRACTION
			#pragma shader_feature_local CAUSTIC
			#pragma shader_feature_local LIGHTING_PHYSICAL_BASED
			#pragma shader_feature_local LIGHTING_BLINN_PHONG
			#pragma shader_feature_local LIGHTING_LAMBERT
			#pragma shader_feature_local BACK_FACE

        	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
        	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
        	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
        	#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

			// Poseidon includes
			#define POSEIDON_WATER_ADVANCED
			#define POSEIDON_SRP
			#include "./CGIncludes/PUniforms.cginc"
			#include "./CGIncludes/PMeshNoise.cginc"
			#include "./CGIncludes/PVertexAnimation.cginc"
			#include "./CGIncludes/PDepth.cginc"
			#include "./CGIncludes/PLightAbsorption.cginc"
			#include "./CGIncludes/PFresnel.cginc"
			#include "./CGIncludes/PFoam.cginc"
			#include "./CGIncludes/PReflection.cginc"
			#include "./CGIncludes/PRefraction.cginc"
			#include "./CGIncludes/PCaustic.cginc"
			#include "./CGIncludes/PLightingSRP.cginc"
			#include "./CGIncludes/PCoreSRP.cginc"

        	ENDHLSL
        }
    }
    Fallback "Hidden/InternalErrorShader"	
}
