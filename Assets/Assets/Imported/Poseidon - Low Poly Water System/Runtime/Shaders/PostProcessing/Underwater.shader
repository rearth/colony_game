﻿Shader "Hidden/Poseidon/Underwater"
{
    HLSLINCLUDE

        #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"

		#pragma shader_feature_local CAUSTIC
		#pragma shader_feature_local DISTORTION

		TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
		TEXTURE2D_SAMPLER2D(_CameraDepthTexture, sampler_CameraDepthTexture);

		float _WaterLevel;
		float _MaxDepth;
		float _SurfaceColorBoost;
		
		float4 _ShallowFogColor;
        float4 _DeepFogColor;
		float _ViewDistance;

		#if CAUSTIC
			TEXTURE2D_SAMPLER2D(_CausticTex, sampler_CausticTex);
			float _CausticSize;
			float _CausticStrength;
		#endif

		#if DISTORTION
			TEXTURE2D_SAMPLER2D(_DistortionTex, sampler_DistortionTex);
			float _DistortionStrength;
			float _WaterFlowSpeed;
		#endif

		float3 _CameraViewDir;
		float _CameraFov;
		float4x4 _CameraToWorldMatrix;
		float _Intensity;

		TEXTURE2D_SAMPLER2D(_NoiseTex, sampler_NoiseTex);

		float NoiseTexFrag(float2 uv)
		{
			return SAMPLE_TEXTURE2D(_NoiseTex, sampler_NoiseTex, uv).r*2 - 1;
		}

		float InverseLerpUnclamped(float a, float b, float value)
		{
			return (value - a) / (b - a + 0.00000001);
		}

		float3 LinearDepthToWorldPosition(float depth, float2 uv)
		{
			float viewPlaneHeight = 2*depth*tan(radians(_CameraFov*0.5));
			float viewPlaneWidth = viewPlaneHeight*(_ScreenParams.x/_ScreenParams.y);
			float x = viewPlaneWidth*(uv.x-0.5);
			float y = viewPlaneHeight*(uv.y-0.5);
			float3 pos = float3(x,y,-depth);
			float3 worldPos = mul(_CameraToWorldMatrix, float4(pos.xyz,1)).xyz;
			return worldPos;
		}

		float3 GetWaterPlaneIntersection(float3 camPos, float3 dir)
		{
			float3 planeOrigin = float3(0, _WaterLevel - camPos.y, 0);
			float3 planeNormal = float3(0, 1, 0);
			float rayLength = dot(planeOrigin, planeNormal)/(dot(dir, planeNormal)+0.000001);
			float3 localIntersect = dir*rayLength;
			float3 worldIntersect = localIntersect + camPos;
			return worldIntersect;
		}

		#if CAUSTIC
			float4 SampleCaustic(float3 worldPos)
			{
				float2 uv = worldPos.xz/(_CausticSize + 0.0000001);
				float4 causticColor = SAMPLE_TEXTURE2D(_CausticTex, sampler_CausticTex, uv + _Time.y*0.0125).rrrr*_CausticStrength;
				float fade = lerp(0.25, 1, NoiseTexFrag(uv*0.05 - _Time.y*0.0125));
				causticColor *= fade;
				causticColor = saturate(causticColor);
			
				return causticColor;
			}
		#endif

		#if DISTORTION
			float2 DistorUV(float2 uv)
			{
				float depth = _WaterLevel - _WorldSpaceCameraPos.y;
				float fDepth = saturate(depth/_MaxDepth);
				float flow = _WaterFlowSpeed*lerp(1, 0.25, fDepth);
				flow = _WaterFlowSpeed;

				float3 n0 = SAMPLE_TEXTURE2D(_DistortionTex, sampler_DistortionTex, uv + flow*_Time.y*0.025).xyz;
				float3 n1 = SAMPLE_TEXTURE2D(_DistortionTex, sampler_DistortionTex, uv*2 - flow*_Time.y*2*0.025).xyz;
				float3 n = (n0+n1)*0.5;
				n = n*2-1;
				n = normalize(n)*_DistortionStrength*0.1;

				uv += float2(n.x, n.y);
				return uv;
			}
		#endif

        float4 Frag(VaryingsDefault i) : SV_Target
        {
			float isCameraBelowWater = _WorldSpaceCameraPos.y <= _WaterLevel;

			float2 uv = i.texcoord;
			#if DISTORTION
				uv = DistorUV(uv);
			#endif
            float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv);
			
			float sceneDepth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, sampler_CameraDepthTexture, uv).r);
			float3 worldPos = LinearDepthToWorldPosition(sceneDepth, uv);
			float aboveWaterSurface = worldPos.y >= _WaterLevel;

			float3 direction = normalize(worldPos-_WorldSpaceCameraPos);
			float3 waterIntersection = GetWaterPlaneIntersection(_WorldSpaceCameraPos, direction);
			worldPos = lerp(worldPos, waterIntersection, aboveWaterSurface);

			float depth = _WaterLevel - min(_WorldSpaceCameraPos.y, worldPos.y);
			float fDepth = saturate(depth/_MaxDepth);
			
			#if CAUSTIC
				float4 causticColor = SampleCaustic(worldPos);
				color += causticColor*(1-fDepth)*(1-aboveWaterSurface)*isCameraBelowWater;
			#endif

			float4 fogColor = lerp(_ShallowFogColor, _DeepFogColor, fDepth);

			float d = distance(worldPos, _WorldSpaceCameraPos);
			float fDistance = saturate(InverseLerpUnclamped(-_ViewDistance, _ViewDistance, d));

			float fColor = fDistance*fogColor.a;
			float f = fColor*_Intensity*isCameraBelowWater;

			color *= lerp(1, _SurfaceColorBoost, aboveWaterSurface);
			float4 result = lerp(color, fogColor, f);
			
            return result;
        }

    ENDHLSL

    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            HLSLPROGRAM

                #pragma vertex VertDefault
                #pragma fragment Frag

            ENDHLSL
        }
    }
}