﻿Shader "Hidden/Poseidon/WetLens"
{
    HLSLINCLUDE

        #include "Packages/com.unity.postprocessing/PostProcessing/Shaders/StdLib.hlsl"
		TEXTURE2D_SAMPLER2D(_MainTex, sampler_MainTex);
		TEXTURE2D_SAMPLER2D(_WetLensTex, sampler_WetLensTex);
		float _Strength;
        
		float2 DistorUV(float2 uv)
		{
			float3 n = SAMPLE_TEXTURE2D(_WetLensTex, sampler_WetLensTex, uv).xyz;
			n = n*2-1;
			n = lerp(float3(0,0,1), n, saturate(_Strength));

			n = normalize(n)*_Strength*0.1;

			uv += float2(-n.x, -n.y);
			return uv;
		}

		float4 Frag(VaryingsDefault i) : SV_Target
        {
            float2 uv = i.texcoordStereo;
			uv = DistorUV(uv);
            float4 color = SAMPLE_TEXTURE2D(_MainTex, sampler_MainTex, uv);

			return color;
        }

    ENDHLSL

    SubShader
    {
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            HLSLPROGRAM

                #pragma vertex VertDefault
                #pragma fragment Frag

            ENDHLSL
        }
    }
}