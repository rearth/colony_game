#ifndef PREFLECTION_INCLUDED
#define PREFLECTION_INCLUDED

void SampleReflectionTexture(sampler2D reflTex, float4 reflTexTexelSize, float4 screenPos, float3 worldNormal, half distortionStrength, out half4 color)
{
	float4 n = float4(worldNormal.x, worldNormal.z, 1, 1);
	half4 offset = half4(n.xy*reflTexTexelSize.xy*distortionStrength, 0, 0);

	float2 uv = float2(screenPos.xy / screenPos.w);
	//uv = UnityStereoTransformScreenSpaceTex(uv);
	uv += offset;
	color = tex2D(reflTex, uv.xy);
}

void SampleReflectionTextureBlur(sampler2D reflTex, float4 reflTexTexelSize, float4 screenPos, float3 worldNormal, half distortionStrength, out half4 color)
{
	float4 n = float4(worldNormal.x, worldNormal.z, 1, 1);
	float2 texel = reflTexTexelSize.xy;
	half4 offset = half4(n.xy*texel.xy*distortionStrength, 0, 0);
	
	float2 uv = float2(screenPos.xy / screenPos.w);
	uv = UnityStereoTransformScreenSpaceTex(uv);
	uv += offset;

	color = half4(0,0,0,0);
	color += tex2D(reflTex, uv.xy)*0.5;
	color += tex2D(reflTex, uv.xy-float2(texel.x, 0))*0.125;
	color += tex2D(reflTex, uv.xy+float2(0, texel.y))*0.125;
	color += tex2D(reflTex, uv.xy+float2(texel.x, 0))*0.125;
	color += tex2D(reflTex, uv.xy-float2(0, texel.y))*0.125;
}

#endif
