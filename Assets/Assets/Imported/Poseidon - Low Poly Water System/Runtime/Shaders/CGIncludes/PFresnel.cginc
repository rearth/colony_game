#ifndef PFRESNEL_INCLUDED
#define PFRESNEL_INCLUDED

void CalculateFresnelFactor(float3 worldPos, float3 worldNormal, half power, half bias, out half fresnel)
{
	float3 worldViewDir = normalize(_WorldSpaceCameraPos.xyz  - worldPos);
	half vDotN = dot(worldViewDir, worldNormal);
	fresnel = saturate(pow(max(0, 1 - vDotN), power)) - bias;
}

#endif
