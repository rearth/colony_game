#ifndef PCORE_INCLUDED
#define PCORE_INCLUDED

#include "PUniforms.cginc"

struct Input  
{
	float4 vertexPos;
	float3 worldPos;
	float4 screenPos;
	float3 normal;
	float fogCoord;
};
 
void vertexFunction(inout appdata_full v, out Input o)
{
	UNITY_INITIALIZE_OUTPUT(Input, o);
	o.vertexPos = v.vertex;
	
	#if MESH_NOISE
		ApplyMeshNoise(_MeshNoise, 100, v.vertex);
		ApplyMeshNoise(_MeshNoise, 100, v.texcoord);
		ApplyMeshNoise(_MeshNoise, 100, v.color);
	#endif

	float4 localVertexOffset;
	float3 localNormalVectorOffset;
	ApplyRipple(v.vertex, v.texcoord, v.color, v.normal, _RippleHeight, _RippleSpeed, _RippleNoiseScale, localVertexOffset, localNormalVectorOffset);
	v.vertex += localVertexOffset;
	v.normal += localNormalVectorOffset;
	o.normal = v.normal;

	UNITY_TRANSFER_FOG(o, UnityObjectToClipPos(v.vertex));
}

void finalColorFunction(Input i, SurfaceOutputStandardSpecular o, inout fixed4 color)
{
	#if FOAM
		float sceneDepth = GetSceneDepth(i.screenPos);
		float surfaceDepth = GetSurfaceDepth(float4(i.worldPos, 1));
		half4 foamColor = half4(0,0,0,0); 
		#if FOAM_HQ
			CalculateFoamColorHQ(sceneDepth, surfaceDepth, float4(i.worldPos, 1), _FoamColor, _FoamDistance, _FoamNoiseScaleHQ, _FoamNoiseSpeedHQ, foamColor);
		#else
			CalculateFoamColor(sceneDepth, surfaceDepth, _FoamColor, _FoamDistance, foamColor);
		#endif
		#if BACK_FACE
			color = lerp(color, foamColor, foamColor.a*0.5);
		#else
			color = lerp(color, foamColor, foamColor.a);
		#endif
	#endif 				

	UNITY_APPLY_FOG(i.fogCoord, color);
}
#endif
