#ifndef PUNIFORMS_INCLUDED
#define PUNIFORMS_INCLUDED

uniform half _MeshNoise;

uniform half4 _Color;
uniform half4 _Specular;
uniform half _Smoothness;

uniform half4 _DepthColor;
uniform half _MaxDepth;

uniform half4 _FoamColor;
uniform half _FoamDistance;
uniform half _FoamNoiseScaleHQ;
uniform half _FoamNoiseSpeedHQ;

uniform float _RippleHeight;
uniform float _RippleNoiseScale;
uniform float _RippleSpeed;

uniform half _FresnelStrength;
uniform half _FresnelBias;

#if defined(POSEIDON_WATER_ADVANCED)
	uniform sampler2D _ReflectionTex;
	uniform half4 _ReflectionTex_TexelSize;
	uniform half _ReflectionDistortionStrength;
		
	uniform sampler2D _RefractionTex;
	uniform half4 _RefractionTex_TexelSize;
	uniform half _RefractionDistortionStrength;

	uniform sampler2D _CausticTex;
	uniform half _CausticSize;
	uniform half _CausticStrength;
	uniform half _CausticDistortionStrength;
#endif

#endif
