#ifndef PVERTEXANIMATION_INCLUDED
#define PVERTEXANIMATION_INCLUDED

#include "PCommon.cginc"

float4 CalculateRippleVertexOffset(float4 localPos, float rippleHeight, float rippleSpeed, float rippleScale)
{
	float localRippleHeight = mul(unity_WorldToObject, float4(rippleHeight.xxx, 0)).y;
	float4 worldPos = mul(unity_ObjectToWorld, float4(localPos.xyz, 1));
	rippleScale *= 0.01;
	rippleSpeed *= 0.1;

	float noiseBase = NoiseTexVert((worldPos.xz - _Time.y*rippleSpeed)*rippleScale)*0.5 + 0.5;
	float noiseFade = lerp(0.5, 1, NoiseTexVert((worldPos.xz + _Time.y*rippleSpeed*3)*rippleScale)*0.5 + 0.5);

	float noise = noiseBase-noiseFade;

	float4 offset = float4(0, noise*localRippleHeight, 0, 0);
	return offset;
}

void ApplyRipple(float4 v0, float4 v1, float4 v2, float3 normal, float rippleHeight, float rippleSpeed, float rippleScale, out float4 localVertexOffset, inout float3 localNormalVectorOffset)
{
	localVertexOffset = CalculateRippleVertexOffset(v0, rippleHeight, rippleSpeed, rippleScale);

	float4 localPos0 = v0 + localVertexOffset;
	float4 localPos1 = v1 + CalculateRippleVertexOffset(v1, rippleHeight, rippleSpeed, rippleScale);
	float4 localPos2 = v2 + CalculateRippleVertexOffset(v2, rippleHeight, rippleSpeed, rippleScale);

	float4 dir0 = localPos1 - localPos0;
	float4 dir1 = localPos2 - localPos0;

	localNormalVectorOffset = normalize(cross(dir0.xyz, dir1.xyz)) - normal;
}
#endif
