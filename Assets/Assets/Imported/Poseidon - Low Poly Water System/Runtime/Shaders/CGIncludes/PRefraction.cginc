#ifndef PREFRACTION_INCLUDED
#define PREFRACTION_INCLUDED

void SampleRefractionTexture(sampler2D refrTex, float4 refrTexTexelSize, float4 screenPos, float3 worldNormal, half distortionStrength, out half4 color)
{
	#if defined(POSEIDON_SRP)
		refrTexTexelSize.xy = 1/_ScreenParams.xy;
	#endif

	float4 n = float4(worldNormal.x, worldNormal.z, 1, 1);
	half4 offset = half4(n.xy*refrTexTexelSize.xy*distortionStrength, 0, 0);

	float2 uv = float2(screenPos.xy / screenPos.w);
	uv = UnityStereoTransformScreenSpaceTex(uv);
	uv -= offset;
	
	#if defined(POSEIDON_SRP)
		color = float4(SHADERGRAPH_SAMPLE_SCENE_COLOR(uv.xy), 1);
	#else
		color = tex2D(refrTex, uv.xy);
	#endif
}

#endif
