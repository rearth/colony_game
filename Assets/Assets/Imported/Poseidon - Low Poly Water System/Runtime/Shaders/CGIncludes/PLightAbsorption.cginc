#ifndef PLIGHTABSORPTION_INCLUDED
#define PLIGHTABSORPTION_INCLUDED

#include "PCommon.cginc"
#include "PDepth.cginc"

void CalculateDeepWaterColor(float sceneDepth, float surfaceDepth, half4 color, half4 depthColor, half maxDepth, out half4 waterColor)
{
	float waterDepth = sceneDepth - surfaceDepth;
	float depthFade = saturate(InverseLerpUnclamped(0, maxDepth, waterDepth));

	waterColor = lerp(color, depthColor, depthFade);
}
#endif