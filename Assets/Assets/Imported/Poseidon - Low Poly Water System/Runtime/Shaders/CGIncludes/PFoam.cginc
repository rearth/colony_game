#ifndef PFOAM_INCLUDED
#define PFOAM_INCLUDED

#include "PCommon.cginc"
#include "PDepth.cginc"

void CalculateFoamColor(float sceneDepth, float surfaceDepth, half4 tint, half foamDistance, out half4 foamColor)
{
	float waterDepth = sceneDepth - surfaceDepth;
	float depthClip = waterDepth <= foamDistance;
	
	foamColor = depthClip*tint;
}

void CalculateFoamColorHQ(float sceneDepth, float surfaceDepth, float3 worldPos, half4 tint, half foamDistance, half noiseScale, half noiseSpeed, out half4 foamColor)
{
	float waterDepth = sceneDepth - surfaceDepth;
	float depthClip = waterDepth <= foamDistance;
	
	noiseScale *= 0.1;
	noiseSpeed *= 0.01;

	half noiseBase = NoiseTexFrag(worldPos.xz*noiseScale - noiseSpeed.xx*_Time.y*0.2)*0.5 + 0.5;
	half noiseFade = NoiseTexFrag(worldPos.xz*noiseScale + noiseSpeed.xx*_Time.y)*0.5 + 0.5;
	half noise = noiseBase*noiseFade;
	half depthFade = saturate(InverseLerpUnclamped(0, foamDistance, waterDepth));
	half noiseClip = noise >= lerp(-0, 1, depthFade);

	foamColor = depthClip*noiseClip*tint;
}

#endif
