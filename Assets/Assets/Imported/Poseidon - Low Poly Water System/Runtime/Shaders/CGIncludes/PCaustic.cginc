#ifndef CAUSTIC_INCLUDED
#define CAUSTIC_INCLUDED

#include "PCommon.cginc"
#include "PDepth.cginc"

void SampleCausticTexture(sampler2D _CausticTex, half size, half strength, float sceneDepth, float surfaceDepth, float3 fragWorldPos, float3 worldNormal, half distortionStrength, out half4 causticColor)
{
	fragWorldPos -= worldNormal*distortionStrength;
	float fragToCamSqrDistance = SqrDistance(fragWorldPos, _WorldSpaceCameraPos.xyz);
	float refWorldPosToCamSqrDistance = (sceneDepth*sceneDepth*fragToCamSqrDistance)/(surfaceDepth*surfaceDepth);

	float3 fragWorldDir = normalize(fragWorldPos - _WorldSpaceCameraPos.xyz);
	float3 refWorldPos = fragWorldDir*sqrt(refWorldPosToCamSqrDistance) + _WorldSpaceCameraPos.xyz;

	float2 uv = refWorldPos.xz/(size+0.0000001);
	causticColor = tex2D(_CausticTex, uv + _Time.y*0.0125).rrrr*strength;
	float fade = lerp(0.25, 1, NoiseTexFrag(uv*0.05 - _Time.y*0.0125));
	causticColor *= fade;
	causticColor = saturate(causticColor);
}

#endif
