#ifndef PMESHNOISE_INCLUDED
#define PMESHNOISE_INCLUDED

#include "PCommon.cginc"

void ApplyMeshNoise(half amount, half noiseFrequency, inout float4 localPos)
{
	float4 worldPos = mul(unity_ObjectToWorld, localPos);
	half offsetX = NoiseTexVert(worldPos.xz/noiseFrequency)*amount;
	half offsetZ = NoiseTexVert(-worldPos.xz/noiseFrequency)*amount;

	worldPos += float4(offsetX, 0, offsetZ, 0);
	localPos = mul(unity_WorldToObject, worldPos);
}

#endif
