#ifndef CORE_SRP_INCLUDED
#define CORE_SRP_INCLUDED

struct GraphVertexInput
{
    float4 vertex		: POSITION;
	float4 texcoord		: TEXCOORD0;
	float4 color		: COLOR;
    float3 normal		: NORMAL;
    float4 tangent		: TANGENT;
    float4 texcoord1	: TEXCOORD1;
};

struct GraphVertexOutput
{
    float4 clipPos					: SV_POSITION;
    float4 lightmapUVOrVertexSH		: TEXCOORD0;
    half4 fogFactorAndVertexLight	: TEXCOORD1; // x: fogFactor, yzw: vertex light
    float4 shadowCoord				: TEXCOORD2;
	float4 tSpace0					: TEXCOORD3;
	float4 tSpace1					: TEXCOORD4;
	float4 tSpace2					: TEXCOORD5;
	float4 screenPos				: TEXCOORD6;
    UNITY_VERTEX_OUTPUT_STEREO
};
						
GraphVertexOutput vertSRP (GraphVertexInput v  )
{
    GraphVertexOutput o = (GraphVertexOutput)0;
    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

	#if MESH_NOISE
		ApplyMeshNoise(_MeshNoise, 100, v.vertex);
		ApplyMeshNoise(_MeshNoise, 100, v.texcoord);
		ApplyMeshNoise(_MeshNoise, 100, v.color);
	#endif

	float4 localVertexOffset;
	float3 localNormalVectorOffset;
	ApplyRipple(v.vertex, v.texcoord, v.color, v.normal, _RippleHeight, _RippleSpeed, _RippleNoiseScale, localVertexOffset, localNormalVectorOffset);
	v.vertex += localVertexOffset;
	v.normal += localNormalVectorOffset;
				
    // Vertex shader outputs defined by graph
    float3 lwWNormal = TransformObjectToWorldNormal(v.normal);
	float3 lwWorldPos = TransformObjectToWorld(v.vertex.xyz);
	float3 lwWTangent = TransformObjectToWorldDir(v.tangent.xyz);
	float3 lwWBinormal = normalize(cross(lwWNormal, lwWTangent) * v.tangent.w);
	o.tSpace0 = float4(lwWTangent.x, lwWBinormal.x, lwWNormal.x, lwWorldPos.x);
	o.tSpace1 = float4(lwWTangent.y, lwWBinormal.y, lwWNormal.y, lwWorldPos.y);
	o.tSpace2 = float4(lwWTangent.z, lwWBinormal.z, lwWNormal.z, lwWorldPos.z);
				
    float4 ScreenPosition = ComputeScreenPos(mul(GetWorldToHClipMatrix(), mul(GetObjectToWorldMatrix(), v.vertex)), _ProjectionParams.x);
	o.screenPos = ScreenPosition;
	
	half flipNormal = 1;
	#if BACK_FACE
		flipNormal = -1;
	#endif

    VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex.xyz);
    OUTPUT_SH(flipNormal*lwWNormal, o.lightmapUVOrVertexSH.xyz);
    half3 vertexLight = VertexLighting(vertexInput.positionWS, flipNormal*lwWNormal);
    half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);
    o.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
    o.clipPos = vertexInput.positionCS;

    return o;
}

half4 fragBasicSRP (GraphVertexOutput IN  ) : SV_Target
{
	UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN);

	half flipNormal = 1;
	half3 flipView = half3(1, 1, 1);
	#if BACK_FACE
		flipNormal = -1;
		flipView = half3(1, -1, 1);
	#endif

    float3 WorldSpaceNormal = normalize(float3(IN.tSpace0.z,IN.tSpace1.z,IN.tSpace2.z));
	float3 WorldSpacePosition = float3(IN.tSpace0.w,IN.tSpace1.w,IN.tSpace2.w);
	float3 WorldSpaceViewDirection = flipView*SafeNormalize( _WorldSpaceCameraPos.xyz  - WorldSpacePosition );
				
	half fresnel;
	CalculateFresnelFactor(WorldSpacePosition, flipNormal*WorldSpaceNormal, _FresnelStrength, _FresnelBias, fresnel);

	#if (LIGHT_ABSORPTION && !BACK_FACE) || FOAM
		float sceneDepth = GetSceneDepth(IN.screenPos);
		float surfaceDepth = GetSurfaceDepth(float4(WorldSpacePosition, 1));
	#endif

	half4 tintColor = _Color;
	#if LIGHT_ABSORPTION && !BACK_FACE
		CalculateDeepWaterColor(sceneDepth, surfaceDepth, _Color, _DepthColor, _MaxDepth, tintColor);
	#endif

	half4 waterColor = lerp(_Color, tintColor, fresnel);
	waterColor = saturate(waterColor);

	half3 Albedo = waterColor.rgb;
	half3 Specular = _Specular.rgb;
	half Smoothness = _Smoothness;
	half Alpha = waterColor.a;

    InputData inputData;
    inputData.positionWS = WorldSpacePosition;

	#if !SHADER_HINT_NICE_QUALITY
		inputData.normalWS = WorldSpaceNormal;
	#else
        inputData.normalWS = normalize(WorldSpaceNormal);
	#endif

	#if !SHADER_HINT_NICE_QUALITY
        inputData.viewDirectionWS = WorldSpaceViewDirection;
	#else
        inputData.viewDirectionWS = normalize(WorldSpaceViewDirection);
	#endif

    inputData.fogCoord = IN.fogFactorAndVertexLight.x;
    inputData.vertexLighting = IN.fogFactorAndVertexLight.yzw;
    inputData.bakedGI = 0;

	half4 color;
	#if LIGHTING_PHYSICAL_BASED
        color = PoseidonFragmentPBR(
        	inputData, 
        	Albedo,
        	Specular, 
        	Smoothness,
        	Alpha);
	#elif LIGHTING_BLINN_PHONG
		color = PoseidonFragmentBlinnPhong(
			inputData,
			Albedo,
			half4(Specular,1),
			Smoothness,
			Alpha);
	#elif LIGHTING_LAMBERT
		color = PoseidonFragmentLambert(
			inputData,
			Albedo,
			Alpha);
	#else
		color = 0;
	#endif 

	#if FOAM
		half4 foamColor = float4(0,0,0,0); 
		#if FOAM_HQ
			CalculateFoamColorHQ(sceneDepth, surfaceDepth, WorldSpacePosition, _FoamColor, _FoamDistance, _FoamNoiseScaleHQ, _FoamNoiseSpeedHQ, foamColor);
		#else
			CalculateFoamColor(sceneDepth, surfaceDepth, _FoamColor, _FoamDistance, foamColor);
		#endif
		#if BACK_FACE
			color = lerp(color, foamColor, foamColor.a*0.5);
		#else
			color = lerp(color, foamColor, foamColor.a);
		#endif
	#endif 

	color.rgb = MixFog(color.rgb, IN.fogFactorAndVertexLight.x);
    return color;
}

half4 fragAdvancedSRP (GraphVertexOutput IN  ) : SV_Target
{
	UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN);

	half flipNormal = 1;
	half3 flipView = half3(1, 1, 1);
	#if BACK_FACE
		flipNormal = -1;
		flipView = half3(1, -1, 1);
	#endif

    float3 WorldSpaceNormal = normalize(float3(IN.tSpace0.z,IN.tSpace1.z,IN.tSpace2.z));
	float3 WorldSpacePosition = float3(IN.tSpace0.w,IN.tSpace1.w,IN.tSpace2.w);
	float3 WorldSpaceViewDirection = flipView*SafeNormalize( _WorldSpaceCameraPos.xyz  - WorldSpacePosition );
				
	half fresnel;
	CalculateFresnelFactor(WorldSpacePosition, flipNormal*WorldSpaceNormal, _FresnelStrength, _FresnelBias, fresnel);
	#if BACK_FACE
		fresnel = fresnel*fresnel;
	#endif

	#if (LIGHT_ABSORPTION && !BACK_FACE) || FOAM || (CAUSTIC && !BACK_FACE)
		float sceneDepth = GetSceneDepth(IN.screenPos);
		float surfaceDepth = GetSurfaceDepth(float4(WorldSpacePosition, 1));
	#endif

	half4 waterColor;
	half4 tintColor = _Color;
	#if LIGHT_ABSORPTION && !BACK_FACE
		CalculateDeepWaterColor(sceneDepth, surfaceDepth, _Color, _DepthColor, _MaxDepth, tintColor); 
	#endif

	half4 reflColor = _Color;
	#if REFLECTION
		#if REFLECTION_BLUR
			SampleReflectionTextureBlur(_ReflectionTex, _ReflectionTex_TexelSize, IN.screenPos, WorldSpaceNormal, _ReflectionDistortionStrength, reflColor);
		#else
			SampleReflectionTexture(_ReflectionTex, _ReflectionTex_TexelSize, IN.screenPos, WorldSpaceNormal, _ReflectionDistortionStrength, reflColor);
		#endif
	#endif

	half4 refrColor = _DepthColor;
	#if REFRACTION
		SampleRefractionTexture(_RefractionTex, _RefractionTex_TexelSize, IN.screenPos, WorldSpaceNormal, _RefractionDistortionStrength, refrColor);
	#endif

	half4 causticColor = half4(0,0,0,0);
	#if CAUSTIC && !BACK_FACE
		SampleCausticTexture(_CausticTex, _CausticSize, _CausticStrength, sceneDepth, surfaceDepth, WorldSpacePosition, WorldSpaceNormal, _CausticDistortionStrength, causticColor);
	#endif
	refrColor += causticColor;

	waterColor = tintColor*lerp(refrColor, reflColor, fresnel);
	waterColor = waterColor*tintColor.a + (1-tintColor.a)*refrColor;
	waterColor = saturate(waterColor);

	half3 Albedo = waterColor.rgb;
	half3 Specular = _Specular.rgb;
	half Smoothness = _Smoothness;
	half Alpha = 1;

    InputData inputData;
    inputData.positionWS = WorldSpacePosition;

	#if !SHADER_HINT_NICE_QUALITY
		inputData.normalWS = WorldSpaceNormal;
	#else
        inputData.normalWS = normalize(WorldSpaceNormal);
	#endif

	#if !SHADER_HINT_NICE_QUALITY
        inputData.viewDirectionWS = WorldSpaceViewDirection;
	#else
        inputData.viewDirectionWS = normalize(WorldSpaceViewDirection);
	#endif

    inputData.fogCoord = IN.fogFactorAndVertexLight.x;
    inputData.vertexLighting = IN.fogFactorAndVertexLight.yzw;
    inputData.bakedGI = 0;

	half4 color;
	#if LIGHTING_PHYSICAL_BASED
		color = PoseidonFragmentPBR(
        	inputData, 
        	Albedo,
        	Specular, 
        	Smoothness,
        	Alpha);
	#elif LIGHTING_BLINN_PHONG
		color = PoseidonFragmentBlinnPhong(
			inputData,
			Albedo,
			half4(Specular,1),
			Smoothness,
			Alpha);
	#elif LIGHTING_LAMBERT
		color = PoseidonFragmentLambert(
			inputData,
			Albedo,
			Alpha);
	#else
		color = 0;
	#endif 

	#if FOAM
		half4 foamColor = half4(0,0,0,0); 
		#if FOAM_HQ
			CalculateFoamColorHQ(sceneDepth, surfaceDepth, WorldSpacePosition, _FoamColor, _FoamDistance, _FoamNoiseScaleHQ, _FoamNoiseSpeedHQ, foamColor);
		#else
			CalculateFoamColor(sceneDepth, surfaceDepth, _FoamColor, _FoamDistance, foamColor);
		#endif		
		#if BACK_FACE
			color = lerp(color, foamColor, foamColor.a*0.5);
		#else
			color = lerp(color, foamColor, foamColor.a);
		#endif
	#endif 
				
	color.rgb = MixFog(color.rgb, IN.fogFactorAndVertexLight.x);
    return color;
}

#endif
