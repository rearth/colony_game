﻿Shader "Poseidon/Default/WaterAdvanced" 
{
	Properties
	{
		[HideInInspector] _MeshNoise ("Mesh Noise", Range(0.0, 1.0)) = 0

		[HideInInspector] _Color ("Color", Color) = (0.0, 0.8, 1.0, 0.5)
		[HideInInspector] _Specular ("Specular Color", Color) = (0.1, 0.1, 0.1, 1)
		[HideInInspector] _Smoothness ("Smoothness", Range(0.0, 1.0)) = 1
		
		[HideInInspector] _DepthColor ("Depth Color", Color) = (0.0, 0.45, 0.65, 0.85)
		[HideInInspector] _MaxDepth ("Max Depth", Float) = 5
		
		[HideInInspector] _FoamColor ("Foam Color", Color) = (1, 1, 1, 1)
		[HideInInspector] _FoamDistance ("Foam Distance", Float) = 1.2
		[HideInInspector] _FoamNoiseScaleHQ ("Foam Noise Scale HQ", Float) = 3
		[HideInInspector] _FoamNoiseSpeedHQ ("Foam Noise Speed HQ", Float) = 1
		   
		[HideInInspector] _RippleHeight ("Ripple Height", Range(0, 1)) = 0.1
		[HideInInspector] _RippleSpeed ("Ripple Speed", Float) = 5
		[HideInInspector] _RippleNoiseScale ("Ripple Noise Scale", Float) = 1
		
		[HideInInspector] _FresnelStrength ("Fresnel Strength", Range(0.0, 5.0)) = 1
		[HideInInspector] _FresnelBias ("Fresnel Bias", Range(0.0, 1.0)) = 0

		[HideInInspector] _ReflectionTex ("Reflection Texture", 2D) = "black" {}
		[HideInInspector] _ReflectionDistortionStrength ("Reflection Distortion Strength", Float) = 1

		[HideInInspector] _RefractionTex ("Refraction Texture", 2D) = "black" {}
		[HideInInspector] _RefractionDistortionStrength ("Refraction Distortion Strength", Float) = 1

		[HideInInspector] _CausticTex ("Caustic Texture", 2D) = "black" {}
		[HideInInspector] _CausticSize ("Caustic Size", Float) = 1
		[HideInInspector] _CausticStrength ("Caustic Strength", Range(0.0, 1.0)) = 1
		[HideInInspector] _CausticDistortionStrength ("Caustic Distortion Strength", Float) = 1
	}

	SubShader
	{	
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry" "IgnoreProjector" = "True" "ForceNoShadowCasting" = "True" }
		Cull Off

		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityCG.cginc"
		
		#pragma shader_feature_local MESH_NOISE
		#pragma shader_feature_local LIGHT_ABSORPTION
		#pragma shader_feature_local FOAM
		#pragma shader_feature_local FOAM_HQ
		#pragma shader_feature_local REFLECTION
		#pragma shader_feature_local REFLECTION_BLUR
		#pragma shader_feature_local REFRACTION
		#pragma shader_feature_local CAUSTIC
		#pragma shader_feature_local BACK_FACE

		#define POSEIDON_WATER_ADVANCED
		#undef POSEIDON_SRP
		#include "./CGIncludes/PUniforms.cginc"
		#include "./CGIncludes/PMeshNoise.cginc"
		#include "./CGIncludes/PLightAbsorption.cginc"
		#include "./CGIncludes/PFoam.cginc"
		#include "./CGIncludes/PVertexAnimation.cginc"
		#include "./CGIncludes/PFresnel.cginc"
		#include "./CGIncludes/PReflection.cginc"
		#include "./CGIncludes/PRefraction.cginc"
		#include "./CGIncludes/PCaustic.cginc"
		#include "./CGIncludes/PCore.cginc"
		
		#pragma multi_compile_fog
		#pragma surface surfAdvanced StandardSpecular nolightmap noinstancing vertex:vertexFunction finalcolor:finalColorFunction
		 
		void surfAdvanced(Input i, inout SurfaceOutputStandardSpecular o)
		{ 
			half flipNormal = 1;
			#if BACK_FACE
				flipNormal = -1;
			#endif

			#if (LIGHT_ABSORPTION && !BACK_FACE) || (CAUSTIC && !BACK_FACE)
				float sceneDepth = GetSceneDepth(i.screenPos);
				float surfaceDepth = GetSurfaceDepth(float4(i.worldPos, 1));
			#endif

			float4 waterColor;
			float4 tintColor = _Color;
			#if LIGHT_ABSORPTION && !BACK_FACE
				CalculateDeepWaterColor(sceneDepth, surfaceDepth, _Color, _DepthColor, _MaxDepth, tintColor); 
			#endif

			float3 worldNormal = UnityObjectToWorldNormal(i.normal);
			float fresnel;
			CalculateFresnelFactor(i.worldPos, flipNormal*worldNormal, _FresnelStrength, _FresnelBias, fresnel);	
			#if BACK_FACE
				fresnel = fresnel*fresnel;
			#endif

			float4 reflColor = _Color;
			#if REFLECTION
				#if REFLECTION_BLUR
					SampleReflectionTextureBlur(_ReflectionTex, _ReflectionTex_TexelSize, i.screenPos, worldNormal, _ReflectionDistortionStrength, reflColor);
				#else
					SampleReflectionTexture(_ReflectionTex, _ReflectionTex_TexelSize, i.screenPos, worldNormal, _ReflectionDistortionStrength, reflColor);
				#endif
			#endif

			float4 refrColor = _DepthColor;
			#if REFRACTION
				SampleRefractionTexture(_RefractionTex, _RefractionTex_TexelSize, i.screenPos, worldNormal, _RefractionDistortionStrength, refrColor);
			#endif

			half4 causticColor = half4(0,0,0,0);
			#if CAUSTIC && !BACK_FACE
				SampleCausticTexture(_CausticTex, _CausticSize, _CausticStrength, sceneDepth, surfaceDepth, float4(i.worldPos, 1), worldNormal, _CausticDistortionStrength, causticColor);
			#endif
			refrColor += causticColor;

			waterColor = tintColor*lerp(refrColor, reflColor, fresnel);
			waterColor = waterColor*tintColor.a + (1-tintColor.a)*refrColor;

			waterColor = saturate(waterColor);
			o.Albedo = waterColor.rgb;
			o.Specular = _Specular;
			o.Smoothness = _Smoothness; 
		}

		ENDCG
	}
	Fallback "Diffuse"
} 
