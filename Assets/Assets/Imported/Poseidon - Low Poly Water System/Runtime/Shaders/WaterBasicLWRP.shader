﻿Shader "Poseidon/LWRP/WaterBasicLWRP"
{
    Properties
    {
		[HideInInspector] _MeshNoise ("Mesh Noise", Range(0.0, 1.0)) = 0

		[HideInInspector] _Color ("Color", Color) = (0.0, 0.8, 1.0, 0.5)
		[HideInInspector] _Specular ("Specular Color", Color) = (0.1, 0.1, 0.1, 1)
		[HideInInspector] _Smoothness ("Smoothness", Range(0.0, 1.0)) = 1
		
		[HideInInspector] _DepthColor ("Depth Color", Color) = (0.0, 0.45, 0.65, 0.85)
		[HideInInspector] _MaxDepth ("Max Depth", Float) = 5
		
		[HideInInspector] _FoamColor ("Foam Color", Color) = (1,1,1,1)
		[HideInInspector] _FoamDistance ("Foam Distance", Float) = 1.2
		[HideInInspector] _FoamNoiseScaleHQ ("Foam Noise Scale HQ", Float) = 3
		[HideInInspector] _FoamNoiseSpeedHQ ("Foam Noise Speed HQ", Float) = 1
		   
		[HideInInspector] _RippleHeight ("Ripple Height", Range(0 , 1)) = 0.1
		[HideInInspector] _RippleSpeed ("Ripple Speed", Float) = 5
		[HideInInspector] _RippleNoiseScale ("Ripple Noise Scale", Float) = 1

		[HideInInspector] _FresnelStrength ("Fresnel Strength", Range(0.0, 5.0)) = 1
		[HideInInspector] _FresnelBias ("Fresnel Bias", Range(0.0, 1.0)) = 0
    }

    SubShader
    {
        Tags { "RenderPipeline"="LightweightPipeline" "RenderType"="Transparent" "Queue"="Transparent" }

		Cull Off
		HLSLINCLUDE
		#pragma target 3.0
		ENDHLSL
		
        Pass
        {
        	Tags { "LightMode"="LightweightForward" }

        	Name "Base"
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			ZTest LEqual
            
        	HLSLPROGRAM
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            
            #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            #pragma multi_compile_fog

            #pragma vertex vertSRP
        	#pragma fragment fragBasicSRP

            #define _SPECULAR_SETUP
			#if LIGHT_ABSORPTION || FOAM
				#define REQUIRE_DEPTH_TEXTURE
			#endif

			#pragma shader_feature_local MESH_NOISE
			#pragma shader_feature_local LIGHT_ABSORPTION
			#pragma shader_feature_local FOAM
			#pragma shader_feature_local FOAM_HQ
			#pragma shader_feature_local LIGHTING_PHYSICAL_BASED
			#pragma shader_feature_local LIGHTING_BLINN_PHONG
			#pragma shader_feature_local LIGHTING_LAMBERT
			#pragma shader_feature_local BACK_FACE

        	#include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/Core.hlsl"
        	#include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/Lighting.hlsl"
        	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
        	#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
        	#include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/ShaderGraphFunctions.hlsl"

			#undef POSEIDON_WATER_ADVANCED
			#define POSEIDON_SRP
			#include "./CGIncludes/PUniforms.cginc"
			#include "./CGIncludes/PMeshNoise.cginc"
			#include "./CGIncludes/PVertexAnimation.cginc"
			#include "./CGIncludes/PDepth.cginc"
			#include "./CGIncludes/PLightAbsorption.cginc"
			#include "./CGIncludes/PFresnel.cginc"
			#include "./CGIncludes/PFoam.cginc"
			#include "./CGIncludes/PCommon.cginc"
			#include "./CGIncludes/PLightingSRP.cginc"
			#include "./CGIncludes/PCoreSRP.cginc"
            
        	ENDHLSL
        }
    }
    Fallback "Hidden/InternalErrorShader"
}
