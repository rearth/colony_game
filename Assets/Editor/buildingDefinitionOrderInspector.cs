﻿using Game.Structures.Placement;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Editor {
    [CustomEditor(typeof(BuildingManager))]
    public class buildingDefinitionOrderInspector : UnityEditor.Editor {
        private ReorderableList reorderableList;

        private BuildingManager listExample {
            get { return target as BuildingManager; }
        }

        private void OnEnable() {
            reorderableList = new ReorderableList(listExample.autoPrefabs, typeof(BuildingManager.placementData), true, true, true, true);

            // Add listeners to draw events
            reorderableList.drawHeaderCallback += DrawHeader;
            reorderableList.drawElementCallback += DrawElement;

            reorderableList.onAddCallback += AddItem;
            reorderableList.onRemoveCallback += RemoveItem;
        }

        private void OnDisable() {
            // Make sure we don't get memory leaks etc.
            reorderableList.drawHeaderCallback -= DrawHeader;
            reorderableList.drawElementCallback -= DrawElement;

            reorderableList.onAddCallback -= AddItem;
            reorderableList.onRemoveCallback -= RemoveItem;
        }
        private void DrawHeader(Rect rect) {
            GUI.Label(rect, "Reorder menu items");
        }

        private void DrawElement(Rect rect, int index, bool active, bool focused) {
            BuildingManager.placementData item = listExample.autoPrefabs[index];

            EditorGUI.BeginChangeCheck();
            item.dispName = EditorGUI.TextField(new Rect(rect.x, rect.y, rect.width, rect.height), item.dispName);
            if (EditorGUI.EndChangeCheck()) {
                EditorUtility.SetDirty(target);
            }
        }

        private void AddItem(ReorderableList list) {
            listExample.autoPrefabs.Add(new BuildingManager.placementData());

            EditorUtility.SetDirty(target);
        }

        private void RemoveItem(ReorderableList list) {
            listExample.autoPrefabs.RemoveAt(list.index);

            EditorUtility.SetDirty(target);
        }

        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            // Actually draw the list in the inspector
            reorderableList.DoLayoutList();
        }
    }
}